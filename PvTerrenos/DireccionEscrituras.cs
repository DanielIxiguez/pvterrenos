﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvTerrenos
{
   public class DireccionEscrituras : CRUD
    {
        public string id_lote { get; set; }
        public string nombre { get; set; }
        public string nombre_predio { get; set; }
        public string n_manzana { get; set; }
        public string n_lote { get; set; }
        public string status_venta { get; set; }

        private string table = "";

        public List<DireccionEscrituras> _list = new List<DireccionEscrituras>();
        public DireccionEscrituras()
        {
            table = COMPRAVENTA;
        }
        public void queryFree(string values_string, string query)
        {
            execute(table, $"{values_string}", action.free.ToString(), query);
        }

        public string values()
        {
            return
            $"{id_lote.ToLower().Trim()}{cc}" +
            $"{nombre}{cc}" +
            $"{nombre_predio}{cc}" +
            $"{n_manzana}{cc}" +
            $"{n_lote}{cc}" +
            $"{status_venta}{cc}";

        }
    }
}
