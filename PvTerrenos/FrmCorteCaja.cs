﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Collections;

namespace PvTerrenos
{
    public partial class FrmCorteCaja : Form
    {
        PdfCreate recibo = new PdfCreate();
        WSpvt.PVT ws = new WSpvt.PVT();
        ConceptoPago CP = new ConceptoPago();
        static List<ResultQueryCPCorteCaja> _ListresultQueryCPCorteCaja = new List<ResultQueryCPCorteCaja>();
        static  List<orderdgvlist> _Listorderdgvlist = new List<orderdgvlist>();
        static List<orderdgvlist> _Listorderdgvlist2 = new List<orderdgvlist>();
        orderdgvlist _orderdgvlist = new orderdgvlist();
        

        string _ListresultQueryCPCorteCajaComprador = "";
               string _ListresultQueryCPCorteCajaCantidad_pagar = "";
               string _ListresultQueryCPCorteCajaLote = "";
               string _ListresultQueryCPCorteCajaManzana = "";
               string _ListresultQueryCPCorteCajaConcepto = "";
               string _ListresultQueryCPCorteCajaFecha = "";
               string _ListresultQueryCPCorteCajaPredio = "";

        string usuario = "";
        string nivelUsuario = "";
        int idUsuario = 0;
        string administracion = "";

        public FrmCorteCaja(string usuario, string nivelUsuario, int idUsuario, string administracion)
        {
            InitializeComponent();

            this.usuario = usuario;
            this.nivelUsuario = nivelUsuario ;
            this.idUsuario = idUsuario;
            this.administracion = administracion;

            inicializarPermisos();
        }

        ////pagos
        private void cmdRealizarConsultas_Click_1(object sender, EventArgs e)
        {
           
            dgvConsultas.Rows.Clear();
            cargarTabla(dgvConsultas, "1", txtTotalPagado, "Administracion General");
            cargarTabla(dgvConsultasMM, "1", txtTotalPagodoMM, "M & M");
        }

        private void cmdConsultaFecha_Click_1(object sender, EventArgs e)
        {


            _Listorderdgvlist.Clear();
            string fecha = dtpFechaConsulta.Value.ToShortDateString();

            var date = DateTime.Parse(fecha);
            var yyyy_mm_dd = date.ToString("yyyy-MM-dd");

            string consulta = "";
            string consultaMM = "";
            txtTotalPagado.Text = "0";
            txtTotalPagodoMM.Text = "0";

            //string mostrarSoloAsociados = "";
            if (nivelUsuario != NivelAcceso.ADMINISTRADORMM.ToString())
            {
                //mostrarSoloAsociados = "AND `administracion` = '" + administracion + "'";
                _Listorderdgvlist.Clear();
                CP.queryFree("", $"SELECT comprador,cantidad_pagar,lote,manzana,concepto,fecha_concepto_pago,predio FROM `ConceptoPago` WHERE STR_TO_DATE( `fecha_concepto_pago` , '%Y-%m-%d' ) = STR_TO_DATE( '{yyyy_mm_dd}', '%Y-%m-%d') and fk_id_user = '{cbEmpleado.Text}' and `administracion` = 'GENERAL'");
              // cargarTablaCP(dgvConsultas);
                consulta = "r.`nombreUsuario` = '" + cbEmpleado.Text + "' AND STR_TO_DATE( `fechaPago` , '%d/%m/%Y' ) = STR_TO_DATE( '" + fecha + "', '%d/%m/%Y')" + "AND `administracion` = 'GENERAL'";
                cargarTabla(dgvConsultas, consulta, txtTotalPagado, "Administracion General");



            



                //if usuario sitemas/adminmm    admin suoervizor
            }
            if(nivelUsuario == NivelAcceso.CAJERO.ToString() || nivelUsuario == NivelAcceso.ADMINISTRADOR.ToString() || nivelUsuario == NivelAcceso.SUPERVISOR.ToString() || nivelUsuario == NivelAcceso.ADMINISTRADORMM.ToString())
            {
                _Listorderdgvlist.Clear();
                CP.queryFree("", $"SELECT comprador,cantidad_pagar,lote,manzana,concepto,fecha_concepto_pago,predio FROM `ConceptoPago` WHERE STR_TO_DATE( `fecha_concepto_pago` , '%Y-%m-%d' ) = STR_TO_DATE( '{yyyy_mm_dd}', '%Y-%m-%d') and fk_id_user = '{cbEmpleado.Text}' and `administracion` = 'MM'");
               // cargarTablaCP(dgvConsultasMM,);
                consultaMM = "r.`nombreUsuario` = '" + cbEmpleado.Text + "' AND STR_TO_DATE( `fechaPago` , '%d/%m/%Y' ) = STR_TO_DATE( '" + fecha + "', '%d/%m/%Y')" + "AND `administracion` = 'MM'";
                cargarTabla(dgvConsultasMM, consultaMM, txtTotalPagodoMM, "M & M");
            }
            if (nivelUsuario != NivelAcceso.ADMINISTRADORMM.ToString())
            {

            }
            //string fecha = dtpFechaConsulta.Value.ToString("dd/MM/yyyy 12:00:00 "/*18/11/2014 12:00:00 a.m.*/);
            //string fechaConcatenada = fecha + "a.m.";
            //`idDetallePago` IN (SELECT `idActividad` FROM `RegistroActividad` WHERE `nombreUsuario` = 'yeni')
            //string consulta = "STR_TO_DATE( `fechaPago` , '%d/%m/%Y' ) = STR_TO_DATE( '" + fecha + "', '%d/%m/%Y')";
            
            //string consulta = "WHERE r.`nombreUsuario` = '" + cbEmpleado.Text + "' AND STR_TO_DATE( a.`fechaPago` , '%d/%m/%Y' ) = STR_TO_DATE( '" + fecha + "', '%d/%m/%Y')";
            //string consulta = "WHERE r.`nombreUsuario` = '" + cbEmpleado.Text + "') AND STR_TO_DATE( a.`fechaPago` , '%d/%m/%Y' ) = STR_TO_DATE( '" + fecha + "', '%d/%m/%Y')";

            //dgvConsultas.Rows.Clear();
            
            double sumaAdministraciones = Convert.ToDouble(txtTotalPagado.Text) + Convert.ToDouble(txtTotalPagodoMM.Text);
            txtTotal.Text = string.Format("{0:N2}",sumaAdministraciones);
        }
        private bool cargarTablaCP(DataGridView tabla)
        {
            //tabla.Rows.Clear();
            // || || !CP.json.Equals("")
            if (!CP.json.Equals("false"))
            {
                _ListresultQueryCPCorteCaja = CP.list<ResultQueryCPCorteCaja>();

                foreach (ResultQueryCPCorteCaja _ListresultQueryCPCorteCajafeach in _ListresultQueryCPCorteCaja)
                {
                    int _ListresultQueryCPCorteCajaCantidad_pagarafter = _ListresultQueryCPCorteCajafeach.cantidad_pagar;
                    DateTime dateTime1 = DateTime.Parse(_ListresultQueryCPCorteCajafeach.fecha_concepto_pago);


                    _Listorderdgvlist.Add(new orderdgvlist()
                    {
                   comprador = _ListresultQueryCPCorteCajafeach.comprador,
                    cantidad_pagar  =  string.Format("{0:N2}", _ListresultQueryCPCorteCajaCantidad_pagarafter),
                    lote = _ListresultQueryCPCorteCajafeach.lote, 
                    manzana = _ListresultQueryCPCorteCajafeach.manzana,
                    concepto = _ListresultQueryCPCorteCajafeach.concepto,
                    fecha_concepto_pago= dateTime1,
                    predio = _ListresultQueryCPCorteCajafeach.predio,
                    });



                   

                   // tabla.Rows.Insert(0, _ListresultQueryCPCorteCajaComprador, _ListresultQueryCPCorteCajaCantidad_pagar, _ListresultQueryCPCorteCajaLote, _ListresultQueryCPCorteCajaManzana, _ListresultQueryCPCorteCajaConcepto, _ListresultQueryCPCorteCajaFecha, _ListresultQueryCPCorteCajaPredio);
             
                   

                }

                return true;

            }
            else
            {
               
                    return false;
            //        MessageBox.Show("La consulta no genero ningun resultado para " + TipoAdministracion);
               
            }
        }
        private bool cargarTablaCPaddless(DataGridView tabla)
        {
           // tabla.Rows.Clear();
            // || || !CP.json.Equals("")
            if (!CP.json.Equals("false"))
            {

                _ListresultQueryCPCorteCaja = CP.list<ResultQueryCPCorteCaja>();
                //_ListresultQueryCPCorteCajaComprador = _ListresultQueryCPCorteCaja[0].comprador;
                //_ListresultQueryCPCorteCajaCantidad_pagar = _ListresultQueryCPCorteCaja[0].cantidad_pagar;
                //_ListresultQueryCPCorteCajaLote = _ListresultQueryCPCorteCaja[0].lote;
                //_ListresultQueryCPCorteCajaManzana = _ListresultQueryCPCorteCaja[0].manzana;
                //_ListresultQueryCPCorteCajaConcepto = _ListresultQueryCPCorteCaja[0].concepto;
                //_ListresultQueryCPCorteCajaFecha = _ListresultQueryCPCorteCaja[0].fecha_concepto_pago;
                //_ListresultQueryCPCorteCajaPredio = _ListresultQueryCPCorteCaja[0].predio;
                foreach (ResultQueryCPCorteCaja _ListresultQueryCPCorteCajafeach in _ListresultQueryCPCorteCaja)
                {
                    int _ListresultQueryCPCorteCajaCantidad_pagarafter = _ListresultQueryCPCorteCajafeach.cantidad_pagar;
                    
                     DateTime dateTime1 = DateTime.Parse(_ListresultQueryCPCorteCajafeach.fecha_concepto_pago);

                    _Listorderdgvlist.Add(new orderdgvlist()
                    {
                        comprador = _ListresultQueryCPCorteCajafeach.comprador,
                        cantidad_pagar = string.Format("{0:N2}", _ListresultQueryCPCorteCajaCantidad_pagarafter),
                        lote = _ListresultQueryCPCorteCajafeach.lote,
                        manzana = _ListresultQueryCPCorteCajafeach.manzana,
                        concepto = _ListresultQueryCPCorteCajafeach.concepto,
                        fecha_concepto_pago = dateTime1,
                        predio = _ListresultQueryCPCorteCajafeach.predio,
                    });

                    //_ListresultQueryCPCorteCajaComprador = _ListresultQueryCPCorteCajafeach.comprador;


                    //_ListresultQueryCPCorteCajaCantidad_pagar = string.Format("{0:N2}", _ListresultQueryCPCorteCajaCantidad_pagarafter);
                    // _ListresultQueryCPCorteCajaCantidad_pagar = _ListresultQueryCPCorteCajafeach.cantidad_pagar;
                    //_ListresultQueryCPCorteCajaLote = _ListresultQueryCPCorteCajafeach.lote;
                    //_ListresultQueryCPCorteCajaManzana = _ListresultQueryCPCorteCajafeach.manzana;
                    //_ListresultQueryCPCorteCajaConcepto = _ListresultQueryCPCorteCajafeach.concepto;
                    //_ListresultQueryCPCorteCajaFecha = _ListresultQueryCPCorteCajafeach.fecha_concepto_pago;
                    //_ListresultQueryCPCorteCajaPredio = _ListresultQueryCPCorteCajafeach.predio;

                    //tabla.Rows.Insert(0, _ListresultQueryCPCorteCajaComprador, _ListresultQueryCPCorteCajaCantidad_pagar, _ListresultQueryCPCorteCajaLote, _ListresultQueryCPCorteCajaManzana, _ListresultQueryCPCorteCajaConcepto, _ListresultQueryCPCorteCajaFecha, _ListresultQueryCPCorteCajaPredio);


                    //orderdgvlist.comprador = _ListresultQueryCPCorteCajaComprador;
                    //orderdgvlist.cantidad_pagar = _ListresultQueryCPCorteCajaCantidad_pagar;
                    //orderdgvlist.lote = _ListresultQueryCPCorteCajaLote;
                    //orderdgvlist.manzana = _ListresultQueryCPCorteCajaManzana;
                    //orderdgvlist.concepto = _ListresultQueryCPCorteCajaConcepto;
                    //orderdgvlist.fecha_concepto_pago = _ListresultQueryCPCorteCajaFecha;
                    //orderdgvlist.predio = _ListresultQueryCPCorteCajaPredio;

                    //_orderdgvlist.cantidad_pagar = string.Format("{0:N2}", _ListresultQueryCPCorteCajaCantidad_pagarafter);
                    //_orderdgvlist.lote = _ListresultQueryCPCorteCajafeach.lote;
                    //_orderdgvlist.manzana = _ListresultQueryCPCorteCajafeach.manzana;
                    //_orderdgvlist.concepto = _ListresultQueryCPCorteCajafeach.concepto;
                    //_orderdgvlist.fecha_concepto_pago = _ListresultQueryCPCorteCajafeach.fecha_concepto_pago;
                    //_orderdgvlist.predio = _ListresultQueryCPCorteCajafeach.predio;


                    //_Listorderdgvlist.Add(_orderdgvlist);

                }



                return true;
            }
           else
            {
                //       MessageBox.Show("La consulta no genero ningun resultado para " + TipoAdministracion);
                return false;    
            }                
        }

        private void cmdReporte_Click_1(object sender, EventArgs e)
        {
            if (dgvConsultas.Rows.Count > 1) 
            {
                reporte(dgvConsultas, txtTotalPagado);
            }

            if (dgvConsultasMM.Rows.Count > 1) 
            {
                reporte(dgvConsultasMM, txtTotalPagodoMM);
            }
        }

        //METODOS**
        /*public void deserialize()
        {
            if (!CP.json.Contains("null"))
            {
                _ListresultQueryCPCorteCaja = CP.list<ResultQueryCPCorteCaja>();

                _ListresultQueryCPCorteCaja
                _ListresultQueryCPCorteCaja
                _ListresultQueryCPCorteCaja
                _ListresultQueryCPCorteCaja
                _ListresultQueryCPCorteCaja
                _ListresultQueryCPCorteCaja
                _ListresultQueryCPCorteCaja


                Listcantidad_total = _ListresultQueryCP[0].cantidad_total;
                Listsumcantidad_pagar = _ListresultQueryCP[0].sumcantidad_pagar;
                restanteCP = Listcantidad_total - Listsumcantidad_pagar;

                if (Listcantidad_total == Listsumcantidad_pagar)
                {
                    MessageBox.Show("ya esta pagado este concepto");
                }
                else if (Listcantidad_total > Listsumcantidad_pagar)
                {
                    txttotal.Text = Listcantidad_total.ToString();
                    tbactual.Text = Listsumcantidad_pagar.ToString();
                    txtMonto.Text = restanteCP.ToString();
                }
            }
        }*/
        public void cargarTabla(DataGridView tabla, string consulta, TextBox sumarTotal, string TipoAdministracion)
        {
            tabla.Rows.Clear();

            string respuestaConsulta = "";
            int bandera = 0;
            try
            {
                ws.Timeout = 600000;
                respuestaConsulta = ws.getDetallePago(consulta);
            }
            catch (WebException e) 
            { 
                MessageBox.Show(e.Message); 
            }

            string[] splitDatos = respuestaConsulta.Split(new char[] { '|' });

           

            if (splitDatos[0] != "" && splitDatos[0] != "0")
            {

                foreach (string datosInsertar in splitDatos)
                {
                    string[] splitDatosPago = datosInsertar.Split(new char[] { ',' });

                    string comprador = splitDatosPago[0];

                    //string[] sinSimbolo = splitDatosPago[1].Split(new char[] { '$' });
                    //double montoPagoMora = Convert.ToDouble(sinSimbolo[1].Trim());
                    double montoPagoMora = Convert.ToDouble(splitDatosPago[1].Trim());
                    double montoAbonoMora = Convert.ToDouble(splitDatosPago[2].Trim());
                    double montoPagoMes = Convert.ToDouble(splitDatosPago[3].Trim());
                    double montoAbonoMes = Convert.ToDouble(splitDatosPago[4].Trim());

                    double total = montoAbonoMora + montoAbonoMes + montoPagoMes + montoPagoMora;

                    string totalPagado = string.Format("{0:N2}", total);

                    string numeroLote = splitDatosPago[5];
                    string numeroManzana = splitDatosPago[6];
                    string fechaProximoPago = Convert.ToDateTime(splitDatosPago[7]).ToString("MMMM - yyyy");
                    //formato de salida
                    //string fechaPago = Convert.ToDateTime(splitDatosPago[8]).ToString("dd/MM/yy hh:mm:ss");
                    DateTime dateTime1 = DateTime.Parse(splitDatosPago[8]);
                    string nombrePredio = splitDatosPago[9];

                    _Listorderdgvlist.Add(new orderdgvlist()
                    {
                    comprador = comprador,
                    cantidad_pagar = totalPagado,
                    lote = numeroLote,
                    manzana = numeroManzana,
                    concepto = fechaProximoPago,
                    fecha_concepto_pago = dateTime1,
                    predio = nombrePredio,
                       
                    });
                   // tabla.Rows.Insert(0, comprador, totalPagado, numeroLote, numeroManzana, fechaProximoPago, fechaPago, nombrePredio);

                    bandera++;

                    
                }
                cargarTablaCPaddless(tabla);
                cargarlistaordenada(tabla);
                sumarTotalPagado(tabla, sumarTotal);
            }
            else 
            {

                if (!cargarTablaCP(tabla))
                {
                    MessageBox.Show("La consulta no genero ningun resultado para " + TipoAdministracion + "");
                }
                else
                {
                    cargarlistaordenada(tabla);
                    sumarTotalPagado(tabla, sumarTotal);
                }
               
            }




        }

        private void cargarlistaordenada(DataGridView tabla)
        {


            //_Listorderdgvlist.OrderBy(x => x.fecha_concepto_pago.TimeOfDay).ToList();

            //_Listorderdgvlist.Sort((x, y) => x.fecha_concepto_pago.CompareTo(y.fecha_concepto_pago));

            _Listorderdgvlist2 = _Listorderdgvlist.OrderByDescending(x => x.fecha_concepto_pago.TimeOfDay).ToList();
            foreach (orderdgvlist orderdgvlistfeac in _Listorderdgvlist2)
            {
                tabla.Rows.Insert(0, orderdgvlistfeac.comprador, orderdgvlistfeac.cantidad_pagar, orderdgvlistfeac.lote, orderdgvlistfeac.manzana, orderdgvlistfeac.concepto, orderdgvlistfeac.fecha_concepto_pago, orderdgvlistfeac.predio);
            }
        }

        private void sumarTotalPagado(DataGridView tabla, TextBox totalPagado)
        {
            double suma = 0;

            foreach (DataGridViewRow row in tabla.Rows)
            {
                if (row.Cells[2].Value == DBNull.Value)
                    continue;

                double valorcell = 0;
                double.TryParse(Convert.ToString(row.Cells[1].Value), out valorcell);

                suma += Convert.ToDouble(valorcell);
            }

            totalPagado.Text = string.Format("{0:N2}", suma);
        }

        private void inicializarPermisos() 
        {
            if (nivelUsuario == NivelAcceso.CAJERO.ToString()) 
            {
                cmdMostrarTodo.Enabled = false;
                cmdReporte.Enabled = false;

                int index = cbEmpleado.FindString(usuario);

                if (index >= 0) 
                {
                    cbEmpleado.SelectedIndex = index;

                    cbEmpleado.Enabled = false;
                    dtpFechaConsulta.Enabled = false;
                }
            }
        }

        private void reporte(DataGridView tabla, TextBox totalPagado) 
        {
            DataTable datosTabla = new DataTable();
            DataRow filaNueva;
            string fechaConsulta = dtpFechaConsulta.Value.ToString("dd-MMMM-yyyy");
            string totalConsulta = totalPagado.Text;

            if (tabla[0, 0].Value.ToString() == string.Empty)
            {
                MessageBox.Show("Favor de llenar la tabla con los datos de la consulta");
            }
            else
            {
                //foreach (DataColumn columna in dgvConsultas.Columns) 
                //{
                //    DataColumn columnaNueva = new DataColumn(columna.ColumnName);

                //    datosTabla.Columns.Add(columnaNueva);
                //}

                datosTabla.Columns.Add("Comprador");
                datosTabla.Columns.Add("Paga");
                datosTabla.Columns.Add("Lote");
                datosTabla.Columns.Add("Manzana");
                datosTabla.Columns.Add("Mes pagado");
                datosTabla.Columns.Add("Fecha de pago");
                datosTabla.Columns.Add("Predio");

                foreach (DataGridViewRow filaDatos in tabla.Rows)
                {
                    filaNueva = datosTabla.NewRow();

                    //for (int i = 0; dgvConsultas.Rows.ToString() != string.Empty; i++) 
                    //{
                    filaNueva["Comprador"] = filaDatos.Cells[0].Value;
                    filaNueva["Paga"] = filaDatos.Cells[1].Value;
                    filaNueva["Lote"] = filaDatos.Cells[2].Value;
                    filaNueva["Manzana"] = filaDatos.Cells[3].Value;
                    filaNueva["Mes pagado"] = filaDatos.Cells[4].Value;
                    filaNueva["Fecha de pago"] = filaDatos.Cells[5].Value;
                    filaNueva["predio"] = filaDatos.Cells[6].Value;

                    datosTabla.Rows.Add(filaNueva);
                    //}
                }

                //datosTabla.TableName = "consulta";

                //recibo.reporteDiario(datosTabla, fechaConsulta, totalConsulta);
                recibo.reporteDiarioDos(datosTabla, fechaConsulta, totalConsulta);
            }
        }
    }
}
