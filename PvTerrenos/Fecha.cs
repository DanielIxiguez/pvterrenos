﻿ using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvTerrenos
{ 
    public class Fecha
    {
         WSpvt.PVT ws = new WSpvt.PVT();
        //variable tipo datetime


        //constructor

        /*public Fecha(DateTime fecha, string pago_actual) {

            this.fecha = fecha;
            this.pago_actual = pago_actual;
        }*/

        public DateTime setProximoPago(DateTime fechaCompra, string pago_actual)
        {

            DateTime fechaProximoPago;
            int auxiliar = 0;

            for (int i = 1; i <= Convert.ToInt32(pago_actual); i++)
            {
                auxiliar = i;
            }
            return (fechaProximoPago = fechaCompra.AddMonths(auxiliar));
        }

        public DateTime setProximoPagoDos(DateTime ProximoPago)
        {
            ProximoPago = ProximoPago.AddMonths(1);
            return ProximoPago;
        }

        public int distanciaMes(DateTime proximoPago, DateTime hoy)
        {
            //DateTime today = DateTime.Today;
            int distanciaMes = hoy.Year * 12 + hoy.Month - (proximoPago.Year * 12 + proximoPago.Month);
           
            return distanciaMes;
        }

        //si el mes no esta en mora a la diferencia de meses se le resta 1
        public int distanciaMesVerificandoMes(DateTime proximoPago, DateTime hoy) 
        {
            int distanciaMes = this.distanciaMes(proximoPago.AddDays(5), hoy);

            if (mesActualEstaEnMora(proximoPago) != "mora")
            {
                distanciaMes -= 1;
            }

            return distanciaMes;
           }

        public string estaEnMora(DateTime fechaProximoPago)
        {
            if (fechaProximoPago.AddDays(5) <= DateTime.Today)
            {
                return "mora";
            }
            else if (this.distanciaMes(fechaProximoPago, DateTime.Today) < 0)
            {
                return "adelantado";
            }
            else
            {
                return "corriente";
            }
        }

        //public Boolean estaEnMora(DateTime fechaProximoPago)
        //{
        //    if (fechaProximoPago.AddDays(5) <= DateTime.Today)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public Boolean statusMora(string statusMora)
        {
            if (statusMora == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DateTime mesActual(DateTime proximoPago) 
        {
            int distanciaMes = 0;
            //int auxiliarDistanciaMes = 0;
            //DateTime auxiliarProximoPago;

            //auxiliarProximoPago = proximoPago.AddDays(5);

            distanciaMes = this.distanciaMes(proximoPago.AddDays(5), DateTime.Today);

            proximoPago = proximoPago.AddMonths(distanciaMes);

            return proximoPago;
        //    int distanciaMes = this.distanciaMes(proximoPago);
        //    int auxiliarDistanciaMes = 0;

        //    //DateTime hoy = DateTime.Today;
        //    DateTime auxiliarProximoPago = proximoPago.AddMonths(distanciaMes).AddDays(5);

        //    auxiliarDistanciaMes = this.distanciaMes(auxiliarProximoPago);

        //    if (auxiliarDistanciaMes < 0)
        //    {
        //        proximoPago = auxiliarProximoPago.AddDays(-5).AddMonths(auxiliarDistanciaMes);
        //    }
        //    else
        //    {
        //        proximoPago = auxiliarProximoPago.AddDays(-5);
        //    }
        //    return proximoPago;
        }

        //verifico que el mes en curso ya entro en mora
        public string mesActualEstaEnMora(DateTime proximoPago) 
        {
            int distanciaMes = 0;

            distanciaMes = this.distanciaMes(proximoPago.AddDays(5), DateTime.Today);

            if (distanciaMes < 0) 
            {
                distanciaMes = 0;
            }

            proximoPago = proximoPago.AddMonths(distanciaMes);

            return estaEnMora(proximoPago);
        }

        public double calcularInteres(DateTime proximoPago, double mensualidadDeProximoPago)
        {
            if (estaEnMora(proximoPago) == "mora")
            {
                int distancia = this.distanciaMesVerificandoMes(proximoPago, DateTime.Today);
                double interes = mensualidadDeProximoPago * 0.06;

                if (distancia >= 0)
                {
                    //int distancia = this.distanciaMes(proximoPago,DateTime.Today);
                    //double interes = mensualidadDeProximoPago * 0.06;
                    if (distancia != 0)
                    {
                        interes += mensualidadDeProximoPago  * 0.06 * distancia;
                    }
                    return interes;
                }
                else 
                {
                    return 0.00;
                }
            }
            return 0.00;
        }

        public bool seRegistroMoraMesActual(DateTime proximoPago, string idVenta)
        {
            int bandera = 0;
            string respuestaFechaMora = "";
            DateTime mesActual = this.mesActual(proximoPago);

            try
            {
                respuestaFechaMora = ws.getFechaMora(idVenta);
            }
            catch(Exception){};

            string[] splitFechaMora = respuestaFechaMora.Split(new char[] { ',' });

            if (splitFechaMora[0] != "")
            {
                foreach (string fechaMora in splitFechaMora)
                {
                    DateTime fechaMoraCompara = Convert.ToDateTime(fechaMora);

                    //for (int i = 0; )
                    if (fechaMoraCompara.Year == mesActual.Year && fechaMoraCompara.Month == mesActual.Month)
                    {
                        bandera++;
                    }
                }
            }

            if (bandera >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string ultimoMesEnMora(string idVenta)
        {
            string respuestaUltimoMesMora = ws.getUltimoMes(idVenta);
            string[] splitUltimoMesMora = respuestaUltimoMesMora.Split(new char[] { ',' });

            //int tamañoArreglo = splitUltimoMesMora.Length;

            return splitUltimoMesMora[0];
        }

        public string calculaMesesMorosos(string esMoroso, Boolean statusMora, DateTime proximoPago, string idVenta, string monto, int actualizo, string montoDeCompraVenta/*, System.Windows.Forms.Label lCargar*/)
        {
            double montoMora = 0;
            int distanciaMes = 0;
            int auxiliar2 = 0;
            //int maxMes = 0;
            int aMaxMes = 0;
            int bMaxMes = 0;
            string respuestaRegistraMora = "";
            DateTime hoyAuxiliar = DateTime.Today;
            DateTime hoy = DateTime.Today;

            if (mesActualEstaEnMora(proximoPago) != "mora")
            {
                hoy = mesActual(proximoPago).AddMonths(-1);
                
                distanciaMes = distanciaMesVerificandoMes(proximoPago, DateTime.Today);
            }
            else 
            {
                hoy = mesActual(proximoPago);

                distanciaMes = this.distanciaMes(proximoPago, hoy);
            }

            //maxMes = hoy.Month;

            if (esMoroso == "mora" && !statusMora)//esta sentencia determinamos si la fecha de "proximoPago" ya entro en mora ademas se checa
            {                           //en el status para saber si ya habia entrado a este ciclo de ser asi no se hace todo el calculo de mora desde su ultima fecha no pagada              
                for (int i = 0; i <= distanciaMes; i++)
                {
                    aMaxMes = i; //lleba el registro del mes en en que se esta efectuando la mora o moras ejemplo "mora de febrero en Marzo <-- esta variable seria Marzo"
                    bMaxMes = distanciaMes - i; //varible que servira para recorrer los meses en el siguiente ciclo

                    //if (i == distanciaMes && monto != montoDeCompraVenta)//cuado hay un abono en el mes actual el resto de los meses se calculen con el monto de la compraventa
                    //{
                    //    monto =  montoDeCompraVenta;
                    //}

                    for (int j = 0; j <= bMaxMes; j++)
                    { //este recorrera los meses hasta llegar al mes en fecha de proximo pago 

                        //montoMora = Convert.ToDouble(monto) * 0.06;

                        DateTime mesPrincipal = proximoPago.AddMonths(distanciaMes - aMaxMes);
                        DateTime mesRecorrido = mesPrincipal.AddMonths(-j);

                        if (monto != montoDeCompraVenta && mesRecorrido.Month == proximoPago.Month)
                        {
                            montoMora = Convert.ToDouble(monto) * 0.06;
                        }
                        else 
                        {
                            montoMora = Convert.ToDouble(montoDeCompraVenta) * 0.06;
                        }

                        respuestaRegistraMora = ws.registraMora(idVenta, Convert.ToString(montoMora), "", mesRecorrido.ToString(), mesPrincipal.ToString(),"0");
                    }
                } ws.updateStatusMora(idVenta);
                return respuestaRegistraMora;
            }

            if (esMoroso == "mora" && statusMora && !seRegistroMoraMesActual(proximoPago, idVenta) && mesActualEstaEnMora(proximoPago) == "mora")
            {
                string ultimoMes = ultimoMesEnMora(idVenta);

                for (int i = 1; Convert.ToDateTime(ultimoMes).Month <= hoy.AddMonths(-i).Month; i++)
                {
                    distanciaMes = i;
                }
                for (int i = 1; proximoPago.Month <= hoy.AddMonths(-i).Month; i++)
                {
                    auxiliar2 = i;
                }

                for (int i = 0; i <= distanciaMes; i++)
                {
                    aMaxMes = i; //lleba el registro del mes en en que se esta efectuando la mora o moras ejemplo "mora de febrero en Marzo <-- esta variable seria Marzo"
                    bMaxMes = auxiliar2 - i; //varible que servira para recorrer los meses en el siguiente ciclo

                    if (i > 1 && monto != montoDeCompraVenta)//cuado hay un abono en el mes actual el resto de los meses se calculen con el monto de la compraventa
                    {
                        monto = montoDeCompraVenta;
                    }

                    for (int j = 0; j <= bMaxMes; j++)
                    {//este recorrera los meses hasta llegar al mes en fecha de proximo pago 

                        montoMora = Convert.ToDouble(monto) * 0.06;

                        DateTime mesPrincipal = Convert.ToDateTime(ultimoMes).AddMonths(distanciaMes - aMaxMes);
                        DateTime mesRecorrido = mesPrincipal.AddMonths(-j);

                        respuestaRegistraMora = ws.registraMora(idVenta, Convert.ToString(montoMora), "", mesRecorrido.ToString(), mesPrincipal.ToString(),"0");
                    }
                    //lCargar.ForeColor = System.Drawing.Color.Blue;
                    //lCargar.Text = "ESPERANDO RESPUESTA";
                } return (respuestaRegistraMora + " se registro nueva mora del mes de "+DateTime.Today.ToString("MMMM").ToUpper()+" para este usuario");
                    //lCargar.ForeColor = System.Drawing.Color.Red;
            }
            else if (actualizo != 1)
            {
                if (statusMora)
                {
                    //lCargar.ForeColor = System.Drawing.Color.Blue;
                    //lCargar.Text = "ESPERANDO RESPUESTA";
                    return "Usuario con mora \n\nPero no genera nueva mora del mes de " + DateTime.Today.ToString("MMMM").ToUpper();
                    //lCargar.ForeColor = System.Drawing.Color.Red;
                }
                //lCargar.ForeColor = System.Drawing.Color.Blue;
                //lCargar.Text = "ESPERANDO RESPUESTA";
                return "Usuario con pago al corriente";
                //lCargar.ForeColor = System.Drawing.Color.Red;
            }
            return "";
        }
    }
}
