﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace PvTerrenos
{
    class Predio : CRUD
    {
        public string id_predio { get; set; }
        public string id_predio_literal { get; set; }
        public string nombre_predio { get; set; }
        public string colonia { get; set; }
        public string municipio { get; set; }
        public string administracion { get; set; }
        //public string fecha_creacion { get; set; }

        private string table = "";

        public List<Predio> _list = new List<Predio>();

        public Predio()
        {
            table = PPREDIO;
        }

        #region METODOS
        public void insert()
        {
            execute(table, values(), action.insert.ToString(), "");
        }

        public void query(string valuesString, string where)
        {
            execute(table, $"{valuesString}", action.query.ToString(), where);
        }

        public void update(string where)
        {
            execute(table, values(), action.update.ToString(), where);
        }

        public string idPredio(string nombre_predio)
        {
            return _list.Find(x => x.nombre_predio == nombre_predio).id_predio;
        }

        public string nombrePredio(string id_predio) 
        {
            return _list.Find(x => x.id_predio == id_predio).nombre_predio;
        }

        public string nombreAdministracion(string id_predio)
        {
            return _list.Find(x => x.id_predio == id_predio).administracion;
        }

        public void loadList()
        {
            _list = list<Predio>();
        }
        #endregion

        #region METODOS INTERFACE
        public void loadComboBox(ComboBox cb)
        {
            //query("1", $"{WHERE} ?");

            foreach (var predio in _list)
            {
                cb.Items.Add(predio.nombre_predio);
            }
        }
        #endregion

        #region VALUES
        public string values()
        {
            return
            $"{id_predio_literal.ToUpper().Trim()}{cc}" +
            $"{nombre_predio.ToUpper().Trim()}{cc}" +
            $"{colonia.ToUpper().Trim()}{cc}" +
            $"{municipio.ToUpper().Trim()}{cc}" +
            $"{administracion.ToUpper().Trim()}";
           //$"{fecha_creacion.ToUpper().Trim()}";
        }
        #endregion
    }
}
