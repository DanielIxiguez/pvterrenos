﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;

namespace PvTerrenos
{
    class CrearPdf
    {
        string filePath;
        public float[] widths { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public PdfPCell _cell { get; set; }
        public Document pdf { get; set; }
        public PdfPTable table { get; set; }
        public PdfWriter write { get; set; }
        public PdfContentByte cb { get; set; }
        public BaseFont baseFont { get; set; }
        public Image image { get; set; }
        public Element _alignment { get; set; }
        public Paragraph _parrafo { get; set; }
        public Font font { get; set; }
        //public FontFactory fontProperties;
        //public FontFactory fontType { get; set; }
        //public Font font { get; set; }
        //public BaseColor fontColor { get; set; }

        /// <summary>
        /// crea y abre un documeto pdf en una ruta temporal
        /// </summary>
        public void createPdf()
        {
            filePath = Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";

            //Creamos documento con el tamaña de pagina tradicional
            pdf = new Document(PageSize.A4, 60, 60, 100, 10);

            //Indicamos la ruta donde se buscara el documento
            write = PdfWriter.GetInstance(pdf, new FileStream(filePath, FileMode.Create));

            //abrimos archivo
            pdf.Open();
        }

        /// <summary>
        /// Recibe un parametro style que es 0 fuente normal 1 fuente negrita
        /// </summary>
        /// <param name="style"></param>
        /// <returns></returns>
        public BaseFont fontFormat(bool style)
        {
            if (style)
            {
                return baseFont = FontFactory.GetFont(FontFactory.HELVETICA, Font.DEFAULTSIZE, Font.BOLD, BaseColor.WHITE).BaseFont;
            }
            else
            {
                return baseFont = FontFactory.GetFont(FontFactory.HELVETICA, Font.DEFAULTSIZE, Font.NORMAL, BaseColor.WHITE).BaseFont;
            }
        }

        public void openWrite()
        {
            cb = new PdfContentByte(write);
            cb = write.DirectContent;
            cb.BeginText();
        }

        public void addLine(string text, int x, int y, int rotacion, BaseFont fontFormat, int fontSize)
        {
            cb.SetFontAndSize(fontFormat, fontSize);
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, text, x, y, rotacion);
        }

        /// <summary>
        /// Carga una imagen en el documento
        /// </summary>
        /// <param name="rutaImagen"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="rotacion"></param>
        /// <returns></returns>
        public void addImage(string rutaImagen, int width, int height, int x, int y, int rotacion)
        {
            // Creamos la imagen y le ajustamos el tamaño
            image = null;
            try
            {
                image = Image.GetInstance(rutaImagen);
                image.Alignment = Element.ALIGN_CENTER;
                image.ScaleAbsoluteWidth(width);
                image.ScaleAbsoluteHeight(height);
                image.SetAbsolutePosition(x, y);
                image.RotationDegrees = rotacion;

                pdf.Add(image);
            }
            catch (System.Net.WebException)
            {
                MessageBox.Show("No se encontro el archivo especificado en la ruta " + rutaImagen + "\n realice una nueva captura de la foto");
            }
        }

        public void parrafo()
        {
            _parrafo = new Paragraph();
        }

        public void parrafoAlignment(int _alignment)
        {
            _parrafo.Alignment = _alignment;
        }

        public void parrafoFont()
        {
            _parrafo.Font = new Font(Font.FontFamily.TIMES_ROMAN, 12f, Font.NORMAL);
        }

        public void parrafoAdd(string parrafo)
        {
            _parrafo.Add(parrafo);
        }

        public Chunk nL()
        {
            return Chunk.NEWLINE;
        }

        public void addParrafo()
        {
            pdf.Add(_parrafo);
        }

        public void pdfTable(int columnas)
        {
            table = new PdfPTable(columnas);
        }

        public void tableConfiguracion()
        {
            table.TotalWidth = 480f;
            table.SpacingBefore = 7f;
            table.SpacingAfter = 10f;
            table.HorizontalAlignment = 1;
            float[] widths = new float[] { 5f, 1f, 1f, 1f };
            table.SetWidths(widths);
            table.LockedWidth = true;
        }

        public void pdfCell(string _texto, int fontSize)
        {
            _cell = new PdfPCell(new Phrase(_texto.ToUpper(), new Font(Font.FontFamily.TIMES_ROMAN, fontSize, Font.NORMAL, BaseColor.BLACK)));
            _cell.HorizontalAlignment = 1;
        }

        public void cellPaddingBottom(float size)
        {
            _cell.PaddingBottom = size;
        }

        public void addCell()
        {
            table.AddCell(_cell);
        }

        public void addTable()
        {
            pdf.Add(table);
        }

        public void closeWrite()
        {
            cb.EndText();
        }

        public void closePdf()
        {
            pdf.Close();
        }

        public void openPdf()
        {
            Process prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = filePath;
            prc.Start();
        }
    }
}
