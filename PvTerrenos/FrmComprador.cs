﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using MySql.Data.MySqlClient;


namespace PvTerrenos
{
    public partial class FrmComprador : Form
    {
        #region VARIABLES
        int index;

        Comprador comprador;
        ComboBox cbComprador;
        #endregion

        #region CONSTRUCTOR
        public FrmComprador(ref Comprador comprador, ref ComboBox cbComprador)
        {
            InitializeComponent();
            this.comprador = comprador;
            this.cbComprador = cbComprador;
            comprador.inicializarlistaId();
            comprador.loadList();
            comprador.loadComboBox(cbNombre);
        }
        #endregion

        #region EVENTOS
        #region CLIC
        //INSERTAR COMPRADOR
        private void btnInsertar_Click(object sender, EventArgs e)
        {
            if (!ValidateForm.empty(this))
            {
                if (
                        Mensaje.responseMessage
                            (
                                Mensaje.list_MessageBox[0], "comprador", false
                            ) == DialogResult.OK
                   )
                {
                    insert();
                }
            }
            else
            {
                MessageBox.Show(Mensaje.result);
            }
        }

        //LIMPIAR FORMULARIO
        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Clear.clearForm(this);
            btnInsertar.Text = " ";//INSERTAR
            btnInsertar.Image = Properties.Resources.insertar;
        }
        #endregion

        #region SELECTED INDEX CHANGE
        private void cbNombre_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadForm();
            btnInsertar.Text = "  ";//ACTULIZAR
            btnInsertar.Image = Properties.Resources.actulizar;
        }
        #endregion

        #region FORMCLOSING
        private void FrmComprador_FormClosed(object sender, FormClosedEventArgs e)
        {
            //comprador.loadComboBox(cbComprador);
        }
        #endregion
        #endregion

        #region METODOS
        //INSERTAR ACTULIZAR COMPRADOR
        public void insert()
        {
            if (btnInsertar.Text == " ")//INSERTAR
            {
                loadInsert();

                comprador._list.Last().insert();
                comprador.insertLastId();
                reloadComboBoxComprador();
                cbComprador.Items.Add(comprador._list.Last().nombre);
                //comprador.loadComboBox(cbComprador);
            }
            else
            {
                
                loadUpdate();
                comprador._list[index].update
                    (
                        $"WHERE {comprador.id_comprador_a} = {comprador.idComprador(index)}"
                    );
                //reloadComboBoxComprador();
                comprador.loadComboBox(cbComprador);
            }

            MessageBox.Show(Mensaje.result);

            /*if (Mensaje.result_bool)
            {
                Clear.clearForm(this);
                btnInsertar.Text = " ";//INSERTAR
                btnInsertar.Image = Properties.Resources.insertar;
            }*/
        }

        //CARGAR FORMULARIO
        public void loadForm()
        {
            index = cbNombre.SelectedIndex;
           
            cbNombre.Text = comprador._list[index].nombre;
            txtDireccion.Text = comprador._list[index].direccion;
            txtColonia.Text = comprador._list[index].colonia;
            txtMunicipio.Text = comprador._list[index].municipio;
            txtBeneficiario.Text = comprador._list[index].beneficiario;
            cbSexo.Text = comprador._list[index].sexo;
            txtOriginario.Text = comprador._list[index].originario;
            txtResidencia.Text = comprador._list[index].residencia;
            txtOcupacion.Text = comprador._list[index].ocupacion;
            cbEstadoCivil.Text = comprador._list[index].estado_civil;
            txtTelefono.Text = comprador._list[index].tel1;
            txtTelefono2.Text = comprador._list[index].tel2;
        }

        //INICIALIZA LA INSTANCIA DEL COMPRADOR
        public void loadInsert()
        {
            comprador._list.Add
                (
                    new Comprador()
                    {
                        nombre = cbNombre.Text.ToUpper().Trim(),
                        direccion = txtDireccion.Text.Trim(),
                        colonia = txtColonia.Text,
                        municipio = txtMunicipio.Text,
                        beneficiario = txtBeneficiario.Text.Trim(),
                        sexo = cbSexo.Text.ToUpper(),
                        originario = txtOriginario.Text.Trim(),
                        residencia = txtResidencia.Text.Trim(),
                        ocupacion = txtOcupacion.Text.Trim(),
                        estado_civil = cbEstadoCivil.Text.Trim(),
                        tel1 = txtTelefono.Text.Trim(),
                        tel2 = txtTelefono2.Text.Trim()
                    }
                );
        }

        public void loadUpdate()
        {
            comprador._list[index].nombre = cbNombre.Text.ToUpper().Trim();
            comprador._list[index].direccion = txtDireccion.Text.Trim();
            comprador._list[index].colonia = txtColonia.Text.Trim();
            comprador._list[index].municipio = txtMunicipio.Text.Trim();
            comprador._list[index].beneficiario = txtBeneficiario.Text.Trim();
            comprador._list[index].sexo = cbSexo.Text.Trim();
            comprador._list[index].originario = txtOriginario.Text.Trim();
            comprador._list[index].residencia = txtResidencia.Text.Trim();
            comprador._list[index].ocupacion = txtOcupacion.Text.Trim();
            comprador._list[index].estado_civil = cbEstadoCivil.Text.Trim();
            comprador._list[index].tel1 = txtTelefono.Text.Trim();
            comprador._list[index].tel2 = txtTelefono2.Text.Trim();
        }

        //RELOAD DATOS COMBOBOX COMPRADOR
        public void reloadComboBoxComprador()
        {
            string nombre = cbNombre.Text.ToUpper().Trim();
            comprador.loadComboBox(cbNombre);
            cbNombre.SelectedIndex = cbNombre.FindString(nombre);
        }

        
        #endregion

        /*void llenarComboComprador() {

            cbNombre.Items.Clear();
            string respuestaCargaComprador = ws.cargaComprador();
            string[] splitComprador = respuestaCargaComprador.Split(new char[] { '|' });
            string[] nombreComprador = splitComprador[0].Split(new char[] { ',' });  
            
            foreach (string comprador in nombreComprador)
            {
                cbNombre.Items.Add(comprador);
            }
        }*/

        /*void agregarCliente()
        {
            //guardo la variable que seran enviadas en la consulta insertar

            string domicilio = txtDireccion.Text;
            string beneficiario = txtBeneficiario.Text;
            string residencia = txtResidencia.Text;
            string ocupacion = txtOcupacion.Text;
            string ecivil = txtEc.Text;
            string telefono = txtTelefono.Text;
            string telefono2 = txtTelefono2.Text;
         
            if (cbNombre.Text == "")
            {
                MessageBox.Show("Debes proporcionar por lo menos los siguientes datos " + ".:: Nombre ::.");
            }
                try
                {
                    string idComprador = generaId(cbNombre.Text);
                    string nombre = cbNombre.Text.ToUpper();

                    string respuetaAgregarComprador = ws.registraComprador(idComprador, nombre, domicilio, beneficiario, residencia, ocupacion, ecivil, telefono, telefono2);
                    MessageBox.Show(respuetaAgregarComprador+"\n\nEl id de usario es "+idComprador);
                    //ventaLote.llenaComboComprador();
                    llenarComboComprador();
                    cbNombre.Text = "";
                    txtDireccion.Text = "";
                    txtBeneficiario.Text = "";
                    txtResidencia.Text = "";
                    txtOcupacion.Text = "";
                    txtEc.Text = "";
                    txtTelefono.Text = "";
                    txtTelefono2.Text = "";
                    
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }  
        }*/

        /*public void initializeValues()
        {
            string name = cbNombre.Text;
            string last_name = "";
            string address = txtDireccion.Text;
            string beneficiary = txtBeneficiario.Text;
            string originally_from = "";
            string residence = txtResidencia.Text;
            string ocupation = txtOcupacion.Text;
            string civil_status = txtEc.Text;
            string number_phone = txtTelefono.Text;
            string alternative_number_phone = txtTelefono2.Text;
            string created_at = DateTime.Now.ToString("dd-MM-yyyy hh:mm tt");

            /*customer = new Comprador()
            {
                nombre = name,
                //last_name = last_name,
                direccion = address,
                beneficiario = beneficiary,
                originario = originally_from,
                recidencia = residence,
                ocupacion = ocupation,
                estado_civil = civil_status,
                tel1 = number_phone,
                tel2 = alternative_number_phone,
                created_at = created_at
            };
        }*/

        /*void modificarCliente(string idcomprador)
          {
              string nombreNuevo = cbNombre.Text.ToUpper();
              string respuestaActulizaCliente = ws.updateComprador(idcomprador, nombreNuevo, txtDireccion.Text, txtBeneficiario.Text, txtResidencia.Text, txtOcupacion.Text, txtEc.Text, txtTelefono.Text, txtTelefono2.Text);
              //ventaLote.llenaComboComprador();
              llenarComboComprador();
              MessageBox.Show(respuestaActulizaCliente);
          }*/

        /*private string generaId(string nombreComprador)
          {

              string idComprador = "";
              string iniciales = "";

              string[] splitComprador = nombreComprador.Split(new char[] { ' ' });

              for (int i = 0; i < splitComprador.Length; i++)
              {

                  iniciales += splitComprador[i].Substring(0, 2);
              }
              idComprador = iniciales;
              return idComprador;
          }*/

        /*private void cbNombre_KeyDown(object sender, KeyEventArgs e)
          { 
              if (e.KeyCode == Keys.Enter) {
          
                  if (cbNombre.FindString(cbNombre.Text) != -1)
                  {
                      btnActualizar.Visible = true;
                      btnInsertar.Enabled = false;
                      cargaCliente();
                  }
                  else
                  {
                      btnActualizar.Visible = false;
                      btnInsertar.Enabled = true;
                  }
                  
              }
          }*/

        /*private void btnActualizar_Click(object sender, EventArgs e)
          {    
                  string mensaje = "¿Estas seguro de que quieres actualizar a este cliente?";
                  string caption = "Actualizar Cliente";
                  MessageBoxButtons botones = MessageBoxButtons.OKCancel;
                  DialogResult resultado;

                  resultado = MessageBox.Show(mensaje, caption, botones);

                  if (resultado == System.Windows.Forms.DialogResult.OK)
                  {
                      modificarCliente(idComprador);
                  }
              
          }*/


        /*public void cargaCliente() {
              string nombre = cbNombre.Text;
              string respuestaCliente = "";

              try
              {
                  respuestaCliente = ws.getComprador(nombre);
                  idComprador = ws.getIdComprador(nombre);
              }
              catch (Exception) { }

              
              string[] splitDatosComprador = respuestaCliente.Split(new char[] { ',' });

              txtDireccion.Text = splitDatosComprador[0];
              txtBeneficiario.Text = splitDatosComprador[1];
              txtResidencia.Text = splitDatosComprador[2];
              txtOcupacion.Text = splitDatosComprador[3];
              txtEc.Text = splitDatosComprador[4];
              txtTelefono2.Text = splitDatosComprador[5];
              txtTelefono.Text = splitDatosComprador[6];
          }*/
    }
}
