﻿namespace PvTerrenos
{
    partial class FrmHistorial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbHasta = new System.Windows.Forms.ComboBox();
            this.cbDesde = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbManzana = new System.Windows.Forms.ComboBox();
            this.cbPredio = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvHistorial = new System.Windows.Forms.DataGridView();
            this.cmdRalizarConsulta = new System.Windows.Forms.Button();
            this.cmdExportarEcxel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistorial)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbHasta);
            this.groupBox1.Controls.Add(this.cbDesde);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(301, 67);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seleccionar año";
            // 
            // cbHasta
            // 
            this.cbHasta.FormattingEnabled = true;
            this.cbHasta.Location = new System.Drawing.Point(194, 28);
            this.cbHasta.Name = "cbHasta";
            this.cbHasta.Size = new System.Drawing.Size(99, 21);
            this.cbHasta.TabIndex = 3;
            this.cbHasta.SelectedIndexChanged += new System.EventHandler(this.cbHasta_SelectedIndexChanged);
            // 
            // cbDesde
            // 
            this.cbDesde.FormattingEnabled = true;
            this.cbDesde.Location = new System.Drawing.Point(50, 28);
            this.cbDesde.Name = "cbDesde";
            this.cbDesde.Size = new System.Drawing.Size(99, 21);
            this.cbDesde.TabIndex = 2;
            this.cbDesde.SelectedIndexChanged += new System.EventHandler(this.cbDesde_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(155, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "hasta";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Desde";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbManzana);
            this.groupBox2.Controls.Add(this.cbPredio);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(319, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(343, 67);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Seleccionar predio y manzana";
            // 
            // cbManzana
            // 
            this.cbManzana.FormattingEnabled = true;
            this.cbManzana.Location = new System.Drawing.Point(271, 28);
            this.cbManzana.Name = "cbManzana";
            this.cbManzana.Size = new System.Drawing.Size(63, 21);
            this.cbManzana.TabIndex = 7;
            // 
            // cbPredio
            // 
            this.cbPredio.FormattingEnabled = true;
            this.cbPredio.Location = new System.Drawing.Point(53, 28);
            this.cbPredio.Name = "cbPredio";
            this.cbPredio.Size = new System.Drawing.Size(156, 21);
            this.cbPredio.TabIndex = 6;
            this.cbPredio.SelectedIndexChanged += new System.EventHandler(this.cbPredio_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Predio";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(215, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "manzana";
            // 
            // dgvHistorial
            // 
            this.dgvHistorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistorial.Location = new System.Drawing.Point(12, 85);
            this.dgvHistorial.Name = "dgvHistorial";
            this.dgvHistorial.Size = new System.Drawing.Size(1227, 460);
            this.dgvHistorial.TabIndex = 2;
            // 
            // cmdRalizarConsulta
            // 
            this.cmdRalizarConsulta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRalizarConsulta.Location = new System.Drawing.Point(668, 22);
            this.cmdRalizarConsulta.Name = "cmdRalizarConsulta";
            this.cmdRalizarConsulta.Size = new System.Drawing.Size(112, 54);
            this.cmdRalizarConsulta.TabIndex = 14;
            this.cmdRalizarConsulta.Text = "Realizar consulta";
            this.cmdRalizarConsulta.UseVisualStyleBackColor = true;
            this.cmdRalizarConsulta.Click += new System.EventHandler(this.cmdRalizarConsulta_Click);
            // 
            // cmdExportarEcxel
            // 
            this.cmdExportarEcxel.Enabled = false;
            this.cmdExportarEcxel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExportarEcxel.Location = new System.Drawing.Point(1127, 22);
            this.cmdExportarEcxel.Name = "cmdExportarEcxel";
            this.cmdExportarEcxel.Size = new System.Drawing.Size(112, 54);
            this.cmdExportarEcxel.TabIndex = 15;
            this.cmdExportarEcxel.Text = "Exportar Excel";
            this.cmdExportarEcxel.UseVisualStyleBackColor = true;
            this.cmdExportarEcxel.Click += new System.EventHandler(this.cmdExportarEcxel_Click);
            // 
            // frmHistorial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 557);
            this.Controls.Add(this.cmdExportarEcxel);
            this.Controls.Add(this.cmdRalizarConsulta);
            this.Controls.Add(this.dgvHistorial);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmHistorial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Historial";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistorial)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbHasta;
        private System.Windows.Forms.ComboBox cbDesde;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbManzana;
        private System.Windows.Forms.ComboBox cbPredio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvHistorial;
        private System.Windows.Forms.Button cmdRalizarConsulta;
        private System.Windows.Forms.Button cmdExportarEcxel;
    }
}