﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvTerrenos
{
   public class Actualizaciones : CRUD
    {
        public int id_actualizacion { get; set; }
        public String numero_actualizacion { get; set; }
        public String fecha_actualizacion { get; set; }
        public String comentario_actualizacion { get; set; }

        private string table = "";
        public List<Actualizaciones> _list = new List<Actualizaciones>();
        #region METODOS
        public Actualizaciones()
        {
            table = ACTUALIZACIONES;
            User user = new User();
            FrmVentaLote vl = new FrmVentaLote(user);
           
            
        }
        public void insert()
        {
            execute(table, values(), action.insert.ToString(), "");
        }

        public void update(string where)
        {
            execute(table, values(), action.update.ToString(), where);
        }

        public void query(string valuesString, string where)
        {
            execute(table, $"{valuesString}", action.query.ToString(), where);
        }
        #endregion
        public string values()
        {
            return
                //$"{id_actualizacion}{cc}" +
                $"{numero_actualizacion.ToUpper().Trim()}{cc}" +
                $"{fecha_actualizacion.ToUpper().Trim()}{cc}" +
                $"{comentario_actualizacion.ToUpper()}";

        }

    }





}
