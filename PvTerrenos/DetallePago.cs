﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvTerrenos
{
    public class DetallePago : CRUD
    {
        public int idDetallePago { get; set; }
        public int idVenta { get; set; }
        public string pagoActual { get; set; }
        public string mensualidad { get; set; }
        public string deudaTotal { get; set; } //este dato dejara de ser utilizado
        public string fechaProximoPago { get; set; }
        public string fechaPago { get; set; }
        public string montoPagoMora { get; set; }
        public string montoAbonoMora { get; set; }
        public string montoPagoMes { get; set; }
        public string montoAbonoMes { get; set; }
        public string administracion { get; set; }
        public string monto { get; set; }
        public string tipo_pago { get; set; }

        public string comprador { get; set; }
        public string idLote { get; set; }

        public string table { get; set; }

        public int _ultimoPago { get; set; }
        public int _mesesVencidos { get; set; }
        public int _numeroPagoMesEnCurso { get; set; }
        public int _numeroProximoPago { get; set; }
        public bool _statusPagado { get; set; }
        public bool _statusPagoVencido { get; set; }

        public decimal _mensualidadMenosAbono { get; set; }
        public decimal _totalAbonoMensualidad { get; set; }
        public decimal _totalAbonoInteres { get; set; }

        public decimal Mensualidad { get; set; }

        //public decimal _montoPagoMora { get; set; } = 0.0M;
        //public decimal _montoAbonoMora { get; set; } = 0.0M;
        //public decimal _montoAbonoMes { get; set; } = 0.0M;
        //public decimal _montoPagoMes { get; set; } = 0.0M;

        public bool _cambiarNumeroPago { get; set; } = false;

        public DateTime _fechaPagoMesEnCurso { get; set; }

        //public int _numeroPagoMesEnCurso { get; set; }

        public List<DateTime> _listAmortizacion { get; set; }
        public List<DetallePago> _listDetallePago { get; set; }

        #region CONSTRUCTORES
        public DetallePago()
        {
            table = DETALLEPAGO;
        }

        public DetallePago(DateTime _fechaCompra, decimal _mensualidad, int _idCompraVenta)
        {
            table = DETALLEPAGO;
            idVenta = _idCompraVenta;
            this.Mensualidad = _mensualidad;

            query(_idCompraVenta.ToString(), $"WHERE `idVenta` = ? ORDER BY `idDetallePago` DESC");
            loadList();
            _listAmortizacion = amortizacion(_fechaCompra);
            _numeroPagoMesEnCurso = numeroPagoMesEnCurso(_listAmortizacion);
            _fechaPagoMesEnCurso = fechaPagoMesEnCurso(_listAmortizacion, _numeroPagoMesEnCurso);
            ///fechaProximoPago = fechaProximoPagoMetodo(_listAmortizacion).ToString();
            _ultimoPago = numeroUltimoPago(_listDetallePago);
            _totalAbonoMensualidad = totalAbonoMensualidad(_listDetallePago);
            _totalAbonoInteres = totalAbonoInteres(_listDetallePago);
            _statusPagoVencido = pagoVencido(_ultimoPago, _numeroPagoMesEnCurso, _fechaPagoMesEnCurso);
            _statusPagado = statusPagado(_mensualidad, _totalAbonoMensualidad);

            _numeroProximoPago = numeroProximoPago(_listDetallePago, _statusPagado);

            _mesesVencidos = mesesVencidos(_ultimoPago, _numeroPagoMesEnCurso, _statusPagoVencido);

            fechaProximoPago = fechaProximoPagoMetodo(_listAmortizacion, _numeroProximoPago).ToString();
            _mensualidadMenosAbono = calcularMensualidad(_mensualidad);

            if (_ultimoPago != _numeroProximoPago && _mensualidad == _totalAbonoMensualidad)
            {
                _totalAbonoMensualidad = 0.0M;
                _totalAbonoInteres = 0.0M;
            }
        }
        #endregion

        #region METODOS
        #region CRUD
        public void insert()
        {
            execute(table, values(), action.insert.ToString(), "");
        }

        public void update(string where)
        {
            execute(table, values(), action.update.ToString(), where);
        }

        public void query(string valuesString, string where)
        {
            execute(table, $"{valuesString}", action.query.ToString(), where);
        }
        //add

        #endregion


        //public int idDetallePagoMetodo(string idVenta)
        //{
        //    return _listDetallePago.Find(x => x.idVenta == this.idVenta).idDetallePago;
        //}

        /// <summary>
        /// obtiene el numero de pago correspodiente al mes en curso
        /// </summary>
        /// <returns></returns>
        public int numeroPagoMesEnCurso(List<DateTime> _listAmortizacion)
        {
            return _listAmortizacion.IndexOf
                 (
                     _listAmortizacion.Find
                     (
                         x => x.Month == DateTime.Today.Month && x.Year == DateTime.Today.Year
                     )
                 ) + 1;
        }

        /// <summary>
        /// obtiene la fecha del pago del mes en curso
        /// </summary>
        /// <returns></returns>
        public DateTime fechaPagoMesEnCurso(List<DateTime> _listAmortizacion, int _numeroPagoMesEnCurso)
        {
            if (_numeroPagoMesEnCurso > 0)
            {
                return _listAmortizacion[_numeroPagoMesEnCurso - 1];
            }
            else
            {
                return _listAmortizacion[_numeroPagoMesEnCurso];
            } 
        }

        /// <summary>
        /// obtiene el numero del ultimo pago realizado
        /// </summary>
        /// <returns></returns>
        public int numeroUltimoPago(List<DetallePago> _listDetallePago)
        {
            if (_listDetallePago != null)
            {
                return Convert.ToInt32(_listDetallePago.First().pagoActual);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// obtiene el numero de pago inmediato al ultimo pago realizado
        /// </summary>
        /// <returns></returns>
        public int numeroProximoPago(List<DetallePago> _listDetallePagodo, bool _statusPagado)
        {

            if (_listDetallePago != null)
            {
                if (_statusPagado)
                {
                    return Convert.ToInt32(_listDetallePago.First().pagoActual) + 1;
                }
                else
                {
                    return Convert.ToInt32(_listDetallePago.First().pagoActual);
                }
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// obtiene la fecha de pago inmediato al ultimo pago realizado
        /// </summary>
        /// <returns></returns>
        public DateTime fechaProximoPagoMetodo(List<DateTime> _listAmortizacion, int _numeroProximoPago)
        {
            return _listAmortizacion[_numeroProximoPago - 1];// numeroUltimoPago(_listDetallePago)];
        }

        /// <summary>
        /// Devuelve la fecha del proximo pago desde la lista de amortizacion inicializada desde el construcctor
        /// </summary>
        /// <param name="_numeroProximoPago"></param>
        /// <returns></returns>
        public DateTime fechaProximoPagoMetodo(int _numeroProximoPago)
        {
            return this._listAmortizacion[_numeroProximoPago - 1];// numeroUltimoPago(_listDetallePago)];
        }

        /// <summary>
        /// Devuelve si el ultimo pago realizado esta totalmente pagado
        /// </summary>
        public bool statusPagado(decimal _mensualidad, decimal _abonoMensualidad)
        {
            //string _ultimoPago = _listDetallePago.First().pagoActual;
            //decimal _sumaAbonoMensualidad = 0.0M;
            //decimal _sumaMensualidad = 0.0M;
            //decimal _totalMensualidad = 0.0M;

            //foreach (var _detallePago in _listDetallePago)
            //{
            //    if (_detallePago.pagoActual == _ultimoPago)
            //    {
            //        _sumaAbonoMensualidad += Convert.ToDecimal(_detallePago.montoPagoMes);
            //        _sumaMensualidad += Convert.ToDecimal(_detallePago.montoAbonoMes);

            //        _totalMensualidad += _sumaMensualidad + _sumaAbonoMensualidad;

            //        _sumaAbonoMensualidad = 0.0M;
            //        _sumaMensualidad = 0.0M;
            //    }
            //    else if(_detallePago.pagoActual != _ultimoPago)
            //    {
            //        break;
            //    }
            //}
            //if (_ultimoPago == _numeroProximoPago)
            //{
            if (_mensualidad == _abonoMensualidad | _ultimoPago == 0)
            {
                return true;
            }
            //}

            return false;
        }

        /// <summary>
        ///  Devuelve el total pagado correspondiente a la mensualidad del ultimo numero de pago
        /// </summary>
        /// <param name="_listDetallePago"></param>
        /// <returns></returns>
        public decimal totalAbonoMensualidad(List<DetallePago> _listDetallePago)
        {

            decimal _sumaAbonoMensualidad = 0.0M;
            decimal _sumaMensualidad = 0.0M;
            decimal _totalMensualidad = 0.0M;

            if (_listDetallePago != null)
            {
                string _ultimoPago = _listDetallePago.First().pagoActual;

                foreach (var _detallePago in _listDetallePago)
                {
                    //Estas validando que se haya realizado el pago total del ultimo pago
                    if (_detallePago.pagoActual == _ultimoPago)
                    {
                        //try
                        //{
                        _sumaAbonoMensualidad += Convert.ToDecimal(_detallePago.montoPagoMes);
                        _sumaMensualidad += Convert.ToDecimal(_detallePago.montoAbonoMes);

                        _totalMensualidad += _sumaMensualidad + _sumaAbonoMensualidad;

                        _sumaAbonoMensualidad = 0.0M;
                        _sumaMensualidad = 0.0M;
                        //}
                        //catch (Exception) { }
                    }
                    else if (_detallePago.pagoActual != _ultimoPago)
                    {
                        break;
                    }
                }
            }

            //if (_mensualidad == _totalMensualidad)
            //{
            //    return true;
            //}

            return _totalMensualidad;
        }

        /// <summary>
        /// Devuelve el total pagado correspondiente al interes del ultimo numero de pago
        /// </summary>
        /// <param name="_listDetallePago"></param>
        /// <returns></returns>
        public decimal totalAbonoInteres(List<DetallePago> _listDetallePago)
         {
            decimal _sumaAbonoInteres = 0.0M;
            decimal _sumaInteres = 0.0M;
            decimal _totalInteres = 0.0M;

            if (_listDetallePago != null)
            {
                string _ultimoPago = _listDetallePago.First().pagoActual;

                foreach (var _detallePago in _listDetallePago)
                {
                    if (_detallePago.pagoActual == _ultimoPago)
                    {
                        //try
                        //{
                        _sumaAbonoInteres += Convert.ToDecimal(_detallePago.montoPagoMora);
                        _sumaInteres += Convert.ToDecimal(_detallePago.montoAbonoMora);

                        _totalInteres += _sumaInteres + _sumaAbonoInteres;

                        _sumaAbonoInteres = 0.0M;
                        _sumaInteres = 0.0M;
                        //}
                        //catch (Exception) { }
                    }
                    else if (_detallePago.pagoActual != _ultimoPago)
                    {
                        break;
                    }
                }
            }
            //if (_mensualidad == _totalMensualidad)
            //{
            //    return true;
            //}

            return _totalInteres;
        }

        public int calcularInteresAbonado(decimal MensualidadActual)//decimal PagoParcialMensualidad, decimal PagoParcialInteres)
        {
            int NumeroMensuliadadesVencidas = _mesesVencidos;
            int NumeroMensualidadesVencidasPagadas = 0;
            int residuo = 0;

            decimal Interes = 0;
            decimal mensualidadDeCompraventa = Mensualidad;
            //decimal InteresActual = 0;

            decimal PagoParcialMensualidad = 0;
            decimal pagoParcialInteres = 0;
            decimal ultimoPagoAbonoInteres = 0;
            List<DetallePago> detalleProximoPago = null;

            if (_listDetallePago != null)
            {
                if (_listDetallePago.Count > 0)
                {
                    detalleProximoPago = _listDetallePago.FindAll(x => x.pagoActual == _numeroProximoPago.ToString());

                    if (detalleProximoPago.Count > 0)
                    {
                        if (Convert.ToDecimal(detalleProximoPago.First().montoAbonoMora) > 0)
                        {
                            foreach (DetallePago dpInteres in detalleProximoPago)
                            {
                                if (Convert.ToDecimal(dpInteres.montoAbonoMes) != 0 && Convert.ToDecimal(dpInteres.montoPagoMora) != 0)
                                {
                                    _totalAbonoInteres += Convert.ToDecimal(dpInteres.montoAbonoMora);// tabla[i, j + 1];
                                }
                            }
                        }
                    }
                }
            }

            if (Mensualidad != MensualidadActual)
            {
                PagoParcialMensualidad += MensualidadActual;
            }

            if (_ultimoPago != 0)
            {
                foreach (DetallePago detallePago in detalleProximoPago.OrderBy(x => x.idDetallePago))//_listDetallePago.FindAll(x => x.pagoActual == _numeroProximoPago.ToString()).OrderBy(x => x.idDetallePago))
                {

                    //for (int j = 0; j < 1; j++)
                    //{
                    //PagoParcialMensualidad += Convert.ToDecimal(detallePago.montoAbonoMes);// tabla[i, j];
                    //mensualidadDeCompraventa -= Convert.ToDecimal(detallePago.montoAbonoMes);

                    if (Convert.ToDecimal(detallePago.montoAbonoMes) > 0)
                    {
                        MensualidadActual += Convert.ToDecimal(detallePago.montoAbonoMes);
                        //pagoParcialInteres += Convert.ToDecimal(detallePago.montoAbonoMora);// tabla[i, j + 1];

                        if (Convert.ToDecimal(detallePago.montoPagoMora) > 0)
                        {
                            pagoParcialInteres = Convert.ToDecimal(detallePago.montoPagoMora);
                        }
                    }
                    else
                    {
                        pagoParcialInteres += Convert.ToDecimal(detallePago.montoAbonoMora);// tabla[i, j + 1];
                        pagoParcialInteres += Convert.ToDecimal(detallePago.montoPagoMora);
                        //_totalAbonoInteres = pagoParcialInteres;
                    }

                    if (MensualidadActual != mensualidadDeCompraventa)//Mensualidad != mensualidadDeCompraventa)
                    {
                        Interes = PagoParcialMensualidad * 0.06M; //PagoParcialMensualidad * 0.06M;
                    }
                    else
                    {
                        Interes = Mensualidad * 0.06M;
                    }

                    //if (Convert.ToDecimal(detallePago.montoAbonoMes) > 0 && pagoParcialInteres > 0)
                    //{
                   // Interes = 10;
                        NumeroMensualidadesVencidasPagadas =  Convert.ToInt32(pagoParcialInteres / Interes);
                    //}
                    Math.DivRem(Convert.ToInt32(_totalAbonoInteres), Convert.ToInt32(Interes), out residuo);

                    if(residuo == 0)
                    {
                        _totalAbonoInteres = 0;
                    }

                    if(_mesesVencidos == NumeroMensualidadesVencidasPagadas)
                    {
                        return 0;
                    }
                    //}
                }

                return  NumeroMensuliadadesVencidas -= NumeroMensualidadesVencidasPagadas;

                //InteresActual = (MensualidadActual * 0.06M) * NumeroMensuliadadesVencidad;

                //return InteresActual;
            }
            return _mesesVencidos;
         }

        /// <summary>
        /// Devuelve la diferencia de la mensualidad menos los abonos de la mensualidad
        /// </summary>
        /// <param name="mensualidad"></param>
        /// <returns></returns>
        public decimal calcularMensualidad(decimal mensualidad)
        {
            if (_statusPagado)
            {
                return mensualidad;
            }
            
            return mensualidad - _totalAbonoMensualidad;
            //decimal resultado = Math.Abs(mensualidad - _totalAbonoMensualidad);

            //if (resultado >= 0.0M )
            //{
            //    return mensualidad;
            //}

            //return resultado;
        }

        /// <summary>
        /// Devuelve la diferencia del interes menos los abonos de interes
        /// </summary>
        /// <param name="interes"></param>
        /// <returns></returns>
        public decimal cacularInteres(decimal interes)
        {
            return Math.Abs(interes - _totalAbonoInteres);
        }

        /// <summary>
        /// Devuelve si el ultimo pago fue abonado al interes o a la mensualidad
        /// </summary>
        public int tipoPagoUltimoPago(bool _statusPago)
        {
            string _tipoPago = _listDetallePago.First().tipo_pago;

            if (_statusPago)
            {
                return 0;
            }
            else if (_tipoPago == "ABONO MENSUALIDAD")
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }

        ///,  <summary>
        /// Determina si el pago inmediato al ultimo pago ya esta vencido
        /// </summary>
        /// <returns></returns>
        public bool pagoVencido(int _ultimoPago, int _numeroPagoMesEnCurso, DateTime _fechaPagoActual)
        {
            if (_ultimoPago > _numeroPagoMesEnCurso)//numeroUltimoPago() > numeroPagoMesEnCurso(this._listAmortizacion))
            {
                return false;
            }
            else if(_ultimoPago == _numeroPagoMesEnCurso)//numeroUltimoPago() == numeroPagoMesEnCurso(this._listAmortizacion))
            {
                //DateTime _fechaUltimoPago = _listAmortizacion[numeroUltimoPago() - 1];

                //if (DateTime.Compare(_fechaUltimoPago.AddDays(+5), DateTime.Today) >= 0)
                //{
                //    return false;
                //}

                //DateTime _fechaPagoActual = fechaPagoMesEnCurso();

                //if (DateTime.Compare(_fechaPagoActual.AddDays(+5), DateTime.Today) >= 0)
                //{
                //    mesActualVencido = false;
                //    return false;
                //}
                return mesActualVencido(_fechaPagoActual);
            }
            else
            {
                //DateTime _fechaPagoActual = fechaPagoMesEnCurso();

                //if (DateTime.Compare(_fechaPagoActual.AddDays(+5), DateTime.Today) >= 0)
                //{
                //    mesActualVencido = false;
                //    //return false;
                //}
                return mesActualVencido(_fechaPagoActual);
            }

            //_mesActualVencido = true;
            //return true;
        }

        /// <summary>
        /// Verifica si el mes en curso esta vencido
        /// </summary>
        /// <returns></returns>
        public bool mesActualVencido(DateTime _fechaPago)
         {
            //DateTime _fechaPagoActual = fechaPagoMesEnCurso();

            if (DateTime.Compare(_fechaPago.AddDays(+5), DateTime.Today) >= 0)
            {
                //_mesActualVencido = false;
                return false; 
            }
            return true;
        }
         
        /// <summary>
        /// Calcula el numero de meses vencidos desde el ultimo pago realizado
        /// </summary>
        /// <param name="_ultimoPago"></param>
        /// <param name="_pagoActual"></param>
        /// <param name="_mesActualVencido"></param>
        /// <returns></returns>
        public int mesesVencidos(int _ultimoPago, int _pagoActual, bool _mesActualVencido)
        {
            int inicio = _ultimoPago; //numeroProximoPago();
            int final = _pagoActual; //numeroPagoMesEnCurso();
            int _mesesVencidos = 0;

            //for (int i = 1; inicio <= final; i++, inicio++)
            //{
            //    _mesesVencidos = i;
            //}

            _mesesVencidos = final - inicio;

            if (_mesesVencidos > 0)
            {
                if (!_mesActualVencido)
                {
                    _mesesVencidos--;

                    try
                    {
                        if (!mesActualVencido(_listAmortizacion[_numeroPagoMesEnCurso - 2]) & _mesesVencidos > 0)
                        {
                            _mesesVencidos--;
                        }
                    }
                    catch (Exception) { }
                }

                if (!_statusPagado)
                { 
                    _mesesVencidos++;
                }

                return _mesesVencidos;
            }

            return _mesesVencidos = 0;//numeroPagoMesEnCurso() - numeroUltimoPago();  
        }

        public decimal pagoInteligente(decimal monto, decimal mensualidad, decimal interes)
        {
            if (_statusPagoVencido)
            {
                if (monto >= interes && interes != 0.0M)
                {
                    montoPagoMora = interes.ToString();

                    monto -= interes;
                }
                else
                {
                    montoAbonoMora = Convert.ToString(interes - monto);

                    monto -= monto;
                }
            }

            if (monto >= mensualidad)
            {
                montoPagoMes = mensualidad.ToString();

                monto -= mensualidad;

                if (_cambiarNumeroPago)
                { 
                    ++_numeroProximoPago;
                    _cambiarNumeroPago = true;
                }
            }
            else
            {
                montoAbonoMes = Convert.ToString(mensualidad - monto);

                monto -= monto;
            }

            return monto;
        }

        public List<DateTime> amortizacion(DateTime fechaInicio)
        {
            List<DateTime> _listAmortizacion = new List<DateTime>();

            for (int i = 0; i <= 99; i++)
            {
                _listAmortizacion.Add(fechaInicio.AddMonths(i));
            }

            return _listAmortizacion;
        }

        public void tipoPago(string interes, string monto, string mensualidad)
        {
            decimal _interes = Convert.ToDecimal(interes);
            decimal _monto = Convert.ToDecimal(monto);
            decimal _mensualidad = Convert.ToDecimal(mensualidad);
            string tipo = "";
            string abono_pago = "";

            if (_mensualidad + _interes >= _monto)
            {
                tipo_pago = "PAGO MENSUALIDAD";
            }
            else
            {
                if (_interes != 0)
                {
                    if (_monto >= _interes)
                    {
                        abono_pago = "PAGO";
                    }
                    else
                    {
                        abono_pago = "ABONO";
                    }

                    tipo = "INTERES";
                }
                else
                {
                    if (_monto >= _mensualidad)
                    {
                        abono_pago = "PAGO";
                    }
                    else
                    {
                        abono_pago = "ABONO";
                    }

                    tipo = "MENSUALIDAD";
                }
                tipo_pago = $"{abono_pago} {tipo}";
            }
        }

        public void loadList()
        {
            _listDetallePago = list<DetallePago>();
        }
        #endregion

        #region METODOS INTERFACE
        public void loadComboBox(ComboBox cb)
        {
            query("1", $"{WHERE} ?");

            //deserializeJson();

            /*cb.DataSource = user_list.Select(user => user.nombre).ToList();
 
            cb.SelectedIndex = -1;*/

            //foreach (var user in list<User>())
            //{
            //    cb.Items.Add(user.nombre.ToUpper());
            //}
        }
        #endregion

        #region VALUES
        public string values()
        {
            return
            $"{idVenta}{cc}" +
            $"{pagoActual}{cc}" +
            $"{100}{cc}" +/*pagoFinal*/
            $"{idLote}{cc}" +/*idLote*/
            $"{mensualidad}{cc}" +
            $"{deudaTotal}{cc}" + 
            $"{""}{cc}" +/*restoAudeudo*/
            $"{comprador}{cc}" +/*comprador*/
            $"{fechaProximoPago}{cc}" +
            $"{fechaPago}{cc}" +
            $"{0}{cc}" +/*cuantasPagoMora*/
            $"{0}{cc}" +/*cuantasAbonoMora*/
            $"{0}{cc}" +/*cuantasPagoMes*/
            $"{0}{cc}" +/*CuantasAbonoMes*/
            $"{montoPagoMora}{cc}" +/*montoPagoMora*/
            $"{montoAbonoMora}{cc}" +/*montoAbonoMora*/
            $"{montoPagoMes}{cc}" +/*montoPagoMes*/
            $"{montoAbonoMes}{cc}" +/*montoAbonoMes*/
            $"{administracion.ToUpper().Trim()}{cc}" +
            $"{monto.ToLower().Trim()}{cc}" +
            $"{tipo_pago.ToUpper().Trim()}"; 
        }
        #endregion
    }
}
 