﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Data;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace PvTerrenos
{
    class PdfCreate
    {
        public void crearPdfRecibo (string pagoActual, 
                                    string pagoFinal,
                                    string numeroLote, 
                                    string numeroManzana,
                                    string predio,
                                    string nombreComprador, 
                                    string mensualidad, 
                                    double deudaTotal,  
                                    double restoAdeudo,
                                    string fechaProximoPago,
                                    string fechaProximoPagoActual,
                                    int[] quePagueYcuantasVeces,
                                    decimal [] montosPagados,
                                    int reimpresion,
                                    string permisos)
        { 
            //string mensualidadLetra = NumeroLetra.NumeroALetras(mensualidad);
            Fecha fecha = new Fecha();
            string filename = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
            string cadenaPrincipal = "";
            string cadenaMonto = "";
            string cadenaMes = "";
            string cadenaMesInteres = "";
            string totalPagado = Convert.ToString(montosPagados[0] + montosPagados[1] + montosPagados[2] + montosPagados[3]);
            string cadenaPagoActual = "";
            int baderaInteres = 0;
            int banderaMensualidad = 0;
            int incrementaPagoActual = Convert.ToInt32(pagoActual);
            string hoy = "";
            
            if(reimpresion == 1)
            {
                hoy = Convert.ToDateTime(fechaProximoPagoActual).ToString("dd-MMMM-yyyy");
            }
            else if (reimpresion == 0)
            {
                hoy = DateTime.Today.ToString("dd-MMMM-yyyy");
            }

            if (quePagueYcuantasVeces[0] > 0)
            {
                int contadorPagoMora = quePagueYcuantasVeces[0];
                string montoPagoMora = "";
                
                //INTERESES implementacion para mejor estetica del recibo cuando son varios pagos******
                if (quePagueYcuantasVeces[0] > 4)
                {
                    string ultimoMes = Convert.ToDateTime(fechaProximoPago).AddMonths(quePagueYcuantasVeces[0]-1).ToString("MMMM - yyyy").ToUpper();

                    cadenaMesInteres += $"\n{Convert.ToDateTime(fechaProximoPago).ToString("MMMM - yyyy").ToUpper()} HASTA {ultimoMes}";
                }
                else
                {
                    for (int i = 0; i < contadorPagoMora; i++)
                    {
                        cadenaMesInteres += Convert.ToDateTime(fechaProximoPago).AddMonths(i).ToString("MMMM").ToUpper();

                        if (i != contadorPagoMora - 1)
                        {
                            cadenaMesInteres += ",";
                        }
                    }
                }

                montoPagoMora = Convert.ToString(montosPagados[0]);

                cadenaMonto = "$ " + string.Format("{0:N2}",Convert.ToDouble(montoPagoMora));
                cadenaPrincipal = "PAGO DE INTERES DE " + cadenaMesInteres;
                baderaInteres = 1;
            }

            if (quePagueYcuantasVeces[2] > 0)
            {
                int contadorPagoMensualidad = quePagueYcuantasVeces[2];
                string montoMensualidad = "";

                //MENSUALIDAD implementacion para mejor estetica del recibo cuando son varios pagos******
                if (quePagueYcuantasVeces[2] > 4)
                {
                    string ultimoMes = Convert.ToDateTime(fechaProximoPago).AddMonths(quePagueYcuantasVeces[2] - 1).ToString("MMMM - yyyy").ToUpper();

                    cadenaMes += $"\n{Convert.ToDateTime(fechaProximoPago).ToString("MMMM - yyyy").ToUpper()} HASTA {ultimoMes}";
                }
                else
                {
                    for (int j = 0; j < contadorPagoMensualidad; j++)
                    {
                        cadenaMes += Convert.ToDateTime(fechaProximoPago).AddMonths(j).ToString("MMMM").ToUpper();

                        if (j != contadorPagoMensualidad - 1)
                        {
                            cadenaMes += ",";
                        }
                    }
                }

                montoMensualidad = Convert.ToString(montosPagados[2]);

                if (baderaInteres == 1)
                {
                    cadenaMonto += "\n\n\n";
                    cadenaMonto = cadenaMonto + "$ " + string.Format("{0:N2}", Convert.ToDouble(montoMensualidad));
                    cadenaPrincipal += "\n\n\n";
                    cadenaPrincipal = cadenaPrincipal + "PAGO MENSUALIDAD DE " + cadenaMes;
                }
                else
                {
                    cadenaMonto = "$ " + string.Format("{0:N2}", Convert.ToDouble(montoMensualidad));
                    cadenaPrincipal = "PAGO MENSUALIDAD DE " + cadenaMes;
                }
                banderaMensualidad = 1;

                //NUMERO PAGO implementacion para mejor estetica del recibo cuando son varios pagos ******
                if (quePagueYcuantasVeces[2] > 4)
                {
                    int ultimoPago = incrementaPagoActual + (quePagueYcuantasVeces[2] - 1);

                    cadenaPagoActual += $"{Convert.ToString(incrementaPagoActual)} HASTA {ultimoPago}";
                }
                else
                {
                    for (int i = 0; i < quePagueYcuantasVeces[2]; i++)
                    {
                        cadenaPagoActual += Convert.ToString(incrementaPagoActual);

                        if (i != quePagueYcuantasVeces[2] - 1)
                        {
                            cadenaPagoActual += ",";
                            incrementaPagoActual++;
                        }
                    }
                }
            }

            if (quePagueYcuantasVeces[1] == 1)
            {
                int adelantaMes = 0;
                string cadenaMesAbonoInteres = "";
                string cadenaAbonoInteres = "";

                if (baderaInteres == 1)
                {
                    adelantaMes = quePagueYcuantasVeces[0];
                }
                
                cadenaMesAbonoInteres = Convert.ToDateTime(fechaProximoPago).AddMonths(adelantaMes).ToString("MMMM").ToUpper();

                if (baderaInteres == 1)
                {
                    cadenaAbonoInteres = "\n\nABONO DE INTERES DE " + cadenaMesAbonoInteres;

                    cadenaPrincipal += cadenaAbonoInteres; 
                    cadenaMonto += "\n\n\n";
                    cadenaMonto = cadenaMonto + "$ " + string.Format("{0:N2}",montosPagados[1]);
                }
                else
                {
                    cadenaAbonoInteres = "ABONO DE INTERES DE " + cadenaMesAbonoInteres;
                    cadenaPrincipal += cadenaAbonoInteres;
                    cadenaMonto = "$ " + string.Format("{0:N2}", montosPagados[1]);
                }
            }

            if (quePagueYcuantasVeces[3] == 1)
            {
                int adelantaMesMensualidad = 0;
                string cedenaMesAbonoMensualidad = "";
                string cadenaAbonoMensualidad = "";

                if (banderaMensualidad == 1)
                {
                    adelantaMesMensualidad = quePagueYcuantasVeces[2];

                    cedenaMesAbonoMensualidad = Convert.ToDateTime(fechaProximoPago).AddMonths(adelantaMesMensualidad).ToString("MMMM").ToUpper();

                    cadenaAbonoMensualidad = "\n\nABONO DE MENSUALIDAD DE " + cedenaMesAbonoMensualidad;

                    cadenaPrincipal += cadenaAbonoMensualidad;

                    cadenaMonto += "\n\n\n";
                    cadenaMonto = cadenaMonto + "$ " + string.Format("{0:N2}", montosPagados[3]);
                    
                    //cadenaPagoActual = pagoActual;
                }

                if (banderaMensualidad != 1)
                {
                    if (baderaInteres != 1)
                    {
                        cedenaMesAbonoMensualidad = Convert.ToDateTime(fechaProximoPago).AddMonths(adelantaMesMensualidad).ToString("MMMM").ToUpper();

                        cadenaAbonoMensualidad = "ABONO DE MENSUALIDAD DE " + cedenaMesAbonoMensualidad;

                        cadenaPrincipal += cadenaAbonoMensualidad;

                        cadenaMonto = cadenaMonto + "$ " + string.Format("{0:N2}", montosPagados[3]);
                        
                        //cadenaPagoActual = pagoActual;
                    }
                    else 
                    {
                        //adelantaMesMensualidad = quePagueYcuantasVeces[2];

                        cedenaMesAbonoMensualidad = Convert.ToDateTime(fechaProximoPago).AddMonths(adelantaMesMensualidad).ToString("MMMM").ToUpper();

                        cadenaAbonoMensualidad = "\n\nABONO DE MENSUALIDAD DE " + cedenaMesAbonoMensualidad;

                        cadenaPrincipal += cadenaAbonoMensualidad;

                        cadenaMonto += "\n\n\n";
                        cadenaMonto = cadenaMonto + "$ " + string.Format("{0:N2}", montosPagados[3]);
                        //cadenaPagoActual = pagoActual;
                    }
                }
                //else
                //{
                //    cadenaMonto += "\n\n";
                //    cadenaMonto = cadenaMonto + "$ " + string.Format("{0:N2}", montosPagados[3]);
                //    cadenaPagoActual = pagoActual;
                //}
            }


            //Creamos documento con el tamaña de pagina tradicional
            Document recibo = new Document(PageSize.A4, 60 , 60, 100, 10);
            //Indicamos la ruta donde se guardara el documento
            PdfWriter escribir = PdfWriter.GetInstance(recibo, new FileStream(filename, FileMode.Create));

            //abrimos archivo
            recibo.Open();

            //creamos el tipo de font que vamos a utilizar
            iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN
                                                                          ,12
                                                                          ,iTextSharp.text.Font.BOLD
                                                                          ,BaseColor.BLACK);

            //Instanciamos un nuevo parrafo
            Paragraph parrafo = new Paragraph();

            parrafo.Alignment = Element.ALIGN_JUSTIFIED;
            parrafo.Font = new Font(Font.FontFamily.TIMES_ROMAN, 12f, Font.NORMAL);

            //agregamos texto al parrafo
            parrafo.Add("                                                                        "
                        +"                                     "+predio
                        +Chunk.NEWLINE
                        +"PAGO No "+cadenaPagoActual+" de "+pagoFinal
                        +"                                                             "
                        +"LOTE No "+numeroLote+" MANZANA "+numeroManzana
		                +Chunk.NEWLINE
		                +Chunk.NEWLINE
		                +"CLIENTE: "+nombreComprador
		                +Chunk.NEWLINE
		                +Chunk.NEWLINE
		                +"FECHA DE PAGO: "+hoy);
           
            //Añadimos el parrafo al documento
            recibo.Add(parrafo);
             
            //creamos la tabla 
            PdfPTable table = new PdfPTable(4);

            table.TotalWidth = 480f;
            table.SpacingBefore = 7f;
            table.SpacingAfter = 10f;
            table.HorizontalAlignment = 1;
            float[] widths = new float[] { 5f, 1f , 1f, 1f };
            table.SetWidths(widths);
            table.LockedWidth = true;

            //PdfPTable table = new PdfPTable(3);
            //table.WidthPercentage = 100;

            PdfPCell concepto = new PdfPCell(new Phrase("CONCEPTO",new Font(Font.FontFamily.TIMES_ROMAN,12,Font.NORMAL,BaseColor.BLACK)));
            concepto.HorizontalAlignment = 1;
            //clNombre.BorderWidth = 0;
            //clNombre.BorderWidthBottom = 0.75f;

            PdfPCell pago = new PdfPCell(new Phrase("PAGO",new Font(Font.FontFamily.TIMES_ROMAN,12,Font.NORMAL,BaseColor.BLACK)));
            pago.HorizontalAlignment = 1;
            //clApellido.BorderWidth = 0;
            //clApellido.BorderWidthBottom = 0.75f;

            PdfPCell total = new PdfPCell(new Phrase("TOTAL",new Font(Font.FontFamily.TIMES_ROMAN,12,Font.NORMAL,BaseColor.BLACK)));
            total.HorizontalAlignment = 1;
            //clPais.BorderWidth = 0;
            //clPais.BorderWidthBottom = 0.75f;

            PdfPCell restan = new PdfPCell(new Phrase(""/*"RESTAN"*/,new Font(Font.FontFamily.TIMES_ROMAN,12,Font.NORMAL,BaseColor.BLACK)));
            restan.HorizontalAlignment = 1;
            //restan.HorizontalAlignment = 1;

            // Añadimos las celdas a la tabla
            table.AddCell(concepto);
            table.AddCell(pago);
            table.AddCell(total);
            table.AddCell(restan);

            // Llenamos la tabla con información
            concepto = new PdfPCell(new Phrase(cadenaPrincipal, new Font(Font.FontFamily.TIMES_ROMAN,11,Font.NORMAL,BaseColor.BLACK)));
            concepto.HorizontalAlignment = 1;
            concepto.PaddingBottom = 35f;
            //clNombre.BorderWidth = 0;

            pago = new PdfPCell(new Phrase(cadenaMonto, new Font(Font.FontFamily.TIMES_ROMAN,11,Font.NORMAL,BaseColor.BLACK)));
            pago.HorizontalAlignment = 1; 
            //clApellido.BorderWidth = 0;

            total = new PdfPCell(new Phrase("\n\n\n\n\n\n\n$ " + string.Format("{0:N2}",Convert.ToDouble(totalPagado)),new Font(Font.FontFamily.TIMES_ROMAN,11,Font.NORMAL,BaseColor.BLACK)));
            total.HorizontalAlignment = 1;
            //clPais.BorderWidth = 0;

            restan = new PdfPCell(new Phrase("\n\n\n\n\n\n\n "/*"\n\n\n\n\n\n\n$ " + string.Format("{0:N2}",Convert.ToDouble(restoAdeudo))*/, new Font(Font.FontFamily.TIMES_ROMAN,11, Font.NORMAL, BaseColor.BLACK)));
            restan.HorizontalAlignment = 1;

            // Añadimos las celdas a la tabla
            table.AddCell(concepto);
            table.AddCell(pago);
            table.AddCell(total);
            table.AddCell(restan);

            recibo.Add(table);

            Paragraph parrafo2 = new Paragraph();
            parrafo2.Alignment = Element.ALIGN_JUSTIFIED;
            parrafo2.Font = new Font(Font.FontFamily.TIMES_ROMAN, 10f, Font.NORMAL);

            int distanciaMes = fecha.distanciaMes(Convert.ToDateTime(fechaProximoPagoActual), DateTime.Today);

            if (distanciaMes < 0) 
            {
                distanciaMes = 0;
            }

            string enMoraFechaProximoPagoActual = fecha.estaEnMora(Convert.ToDateTime(fechaProximoPagoActual));
            string enMoraMesActual = fecha.mesActualEstaEnMora(Convert.ToDateTime(fechaProximoPagoActual).AddMonths(distanciaMes));
            string fechaInteres = "";
            string formatoFecha = "dd";
            string mensajeFinal = "";

            if (enMoraFechaProximoPagoActual == "mora")
            {
                if (enMoraMesActual == "mora")
                {
                    //if (quePagueYcuantasVeces[3] == 0)
                    //{
                    fechaInteres = Convert.ToDateTime(fechaProximoPagoActual).AddMonths(distanciaMes + 1).ToString(formatoFecha).ToUpper();
                    mensajeFinal = "***Recuerde realizar sus pagos puntualmente para no generar intereses";
                    //}
                    //else 
                    //{
                    //fechaInteres = Convert.ToDateTime(fechaProximoPago).AddMonths(distanciaMes).ToString("dd-MMMM-yyyy").ToUpper();
                    //}
                }
                else 
                {
                    fechaInteres = Convert.ToDateTime(fechaProximoPagoActual).AddMonths(distanciaMes).ToString(formatoFecha).ToUpper();
                    mensajeFinal = "***Recuerde realizar sus pagos puntualmente para no generar intereses";
                }
            }
            
            else if (enMoraMesActual == "adelantado") 
            {
                //if (quePagueYcuantasVeces[3] == 0)
                //{
                fechaInteres = Convert.ToDateTime(fechaProximoPagoActual).ToString(formatoFecha).ToUpper();
                mensajeFinal = "***Gracias por ser un excelente cliente";
                //}
                //else 
                //{
                //fechaInteres = Convert.ToDateTime(fechaProximoPagoActual).ToString("dd-MMMM-yyyy").ToUpper();
                //mensajeFinal = "Gracias por ser un excelente cliente";
                //}
            }

            else if (enMoraMesActual == "corriente") 
            {
                if (quePagueYcuantasVeces[3] == 0)
                {
                    fechaInteres = Convert.ToDateTime(fechaProximoPagoActual).AddMonths(distanciaMes + 1).ToString(formatoFecha).ToUpper();
                    mensajeFinal = "***Gracias por ser puntual con sus pagos";
                }
                
                else if (quePagueYcuantasVeces[1] == 0) 
                {
                    fechaInteres = Convert.ToDateTime(fechaProximoPagoActual).AddMonths(distanciaMes + 1).ToString(formatoFecha).ToUpper();
                    mensajeFinal = "***Gracias por ser puntual con sus pagos";
                }

                else
                {
                    fechaInteres = Convert.ToDateTime(fechaProximoPagoActual).AddMonths(distanciaMes).ToString(formatoFecha).ToUpper();
                    mensajeFinal = "***Gracias por ser puntual con sus pagos";
                }
            }
           
            //else if (distanciaMes > 0) 
            //{
            //    fechaInteres = Convert.ToDateTime(fechaProximoPago).AddMonths(distanciaMes).ToString("dd-MMMM-yyyy").ToUpper();
            //}
            if (reimpresion == 0)
            {
                parrafo2.Add(/*"****TIENE UN ADEUDO PENDIENTE DE $" + string.Format("{0:N2}", Convert.ToDouble(restoAdeudo))
                            +*/ Chunk.NEWLINE
                            + "****SU FECHA DE CORTE SON LOS DIAS "/*"****SU PROXIMA FECHA DE PAGO PARA NO GENERAR INTERESES "*/
                            + fechaInteres
                            + Chunk.NEWLINE
                            + mensajeFinal);
            }
            else if (reimpresion == 1) 
            {
                parrafo2.Add(/*"****TIENE UN ADEUDO PENDIENTE DE $" + string.Format("{0:N2}", Convert.ToDouble(restoAdeudo))
                           +*/ Chunk.NEWLINE
                           + "****ESTE DOCUMENTO ES UNA REIMPRESION, INVALIDO AL PRESENTAR OTRO DOCUMENTO CON EL MISMO NUMERO DE PAGO");
            }

            recibo.Add(parrafo2);

            //cerramos la escritura del documento
            recibo.Close();

            /* if (permisos == "3")
             {

                 ProcessStartInfo info = new ProcessStartInfo();
                 info.Verb = "print";
                 info.FileName = filename;
                 info.CreateNoWindow = true;
                 info.WindowStyle = ProcessWindowStyle.Hidden;


                 Process p = new Process();
                 p.StartInfo = info;
                 p.Start();

                 p.WaitForInputIdle();
                 System.Threading.Thread.Sleep(3000);
                 if (false == p.CloseMainWindow())
                 p.Kill();
             }
             else
             {
                 Process prc = new System.Diagnostics.Process();
                 prc.StartInfo.FileName = filename;
                 prc.Start();
             }*/

            Process prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = filename;
            prc.Start();

        }

        public void reporteDiario(DataTable datosTabla, string fechaConsulta, string totalConsulta /*string comprador, double[] montosPagados, string lote, string manzana, string mes, string predio*/)
        {
            string filename = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
            string comprador = "";
            string lote = "";
            string manzana = "";
            string fechaProximoPago = "";
            string fechaPago = "";
            string predio = "";
            double montoTotal = 0;
            //DataSet datosTabla = new DataSet();    
            //datosTabla.Tables.Add(tabla);

            Document recibo = new Document(PageSize.LEGAL, 60, 60, 30, 10);
            //Indicamos la ruta donde se guardara el documento
            PdfWriter escribir = PdfWriter.GetInstance(recibo, new FileStream(filename, FileMode.Create));

            //abrimos archivo
            recibo.Open();

            //creamos el tipo de font que vamos a utilizar
            iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN
                                                                          , 9
                                                                          , iTextSharp.text.Font.BOLD
                                                                          , BaseColor.BLACK);
           //Instanciamos un nuevo parrafo
            Paragraph parrafo = new Paragraph();

            parrafo.Alignment = Element.ALIGN_JUSTIFIED;

            //string[] contenidoParrafo = {"hola","adios"};
 
            parrafo.Add("                                    CONTROL DE INGRESOS DEL "+fechaConsulta);

            for (int i = 0; i < datosTabla.Rows.Count-1; i++)
            {
                comprador = datosTabla.Rows[i][0].ToString();
                montoTotal = Convert.ToDouble(datosTabla.Rows[i][1].ToString());
                lote = datosTabla.Rows[i][2].ToString();
                manzana = datosTabla.Rows[i][3].ToString();
                fechaProximoPago = datosTabla.Rows[i][4].ToString();
                fechaPago = datosTabla.Rows[i][5].ToString();
                predio = datosTabla.Rows[i][6].ToString();

                parrafo.Add(  "\n\nNOMBRE: " + comprador + "                        PAGA: " + string.Format("{0:N2}",montoTotal)/*25 espacios*/
                              + Chunk.NEWLINE
                              + "LOTE: " + lote + "         MANZANA: " + manzana + "         MES: " + Convert.ToDateTime(fechaProximoPago).ToString("MMMM - yyyy") + "               PREDIO: " + predio/*10 espacios*/
                              + Chunk.NEWLINE);
            }

            parrafo.Add("\n\n TOTAL "+totalConsulta);
                          
            recibo.Add(parrafo);
            recibo.Close();

            Process prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = filename;
            prc.Start();
        }

        public void reporteDiarioDos(DataTable datosTabla, string fechaConsulta, string totalConsulta /*string comprador, double[] montosPagados, string lote, string manzana, string mes, string predio*/)
        {
            string filename = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
            string comprador = "";
            string lote = "";
            string manzana = "";
            string fechaProximoPago = "";
            string fechaPago = "";
            string predio = "";
            double montoTotal = 0;
            //DataSet datosTabla = new DataSet();    
            //datosTabla.Tables.Add(tabla);

            Document recibo = new Document(PageSize.LEGAL, 60, 60, 30, 10);
            //Indicamos la ruta donde se guardara el documento
            PdfWriter escribir = PdfWriter.GetInstance(recibo, new FileStream(filename, FileMode.Create));

            //abrimos archivo
            recibo.Open();

            //creamos el tipo de font que vamos a utilizar
            iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN
                                                                          , 9
                                                                          , iTextSharp.text.Font.BOLD
                                                                          , BaseColor.BLACK);
           //Instanciamos un nuevo parrafo
            Paragraph parrafo = new Paragraph();
            parrafo.Alignment = Element.ALIGN_JUSTIFIED;

            Paragraph parrafo2 = new Paragraph();
            parrafo2.Alignment = Element.ALIGN_RIGHT;
            //parrafo2.Font = new Font(Font.FontFamily.TIMES_ROMAN, 10f, Font.NORMAL);

            //string[] contenidoParrafo = {"hola","adios"};
 
            parrafo.Add("                                    CONTROL DE INGRESOS DEL "+fechaConsulta+"\n\n");

            PdfPTable table = new PdfPTable(7);
            //tamaño de la tabla en puntos
            table.TotalWidth = 500f;
            //truncar el tamaño de la tabla
            table.LockedWidth = true;

            Font font = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL, BaseColor.BLACK);
            Font font2 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD, BaseColor.BLACK);

            //relative col widths in proportions - 1/3 and 2/3
            float[] widths = new float[] { 7f, 2f, 2f, 2f, 1f, 2f, 2f};
            table.SetWidths(widths);
            table.HorizontalAlignment = 0;
            //espaicion antes y despues de la tabla
            //table.SpacingBefore = 20f;
            //table.SpacingAfter = 30f;

            PdfPCell cellComprador = new PdfPCell(new Phrase("Comprador", font2));
            //cellComprador.Colspan = 2;
            //cell.Border = 0;
            cellComprador.HorizontalAlignment = 1;
            table.AddCell(cellComprador);

            PdfPCell cellPagado = new PdfPCell(new Phrase("Pagado", font2));
            //cellPagado.Colspan = 2;
            //cell.Border = 0;
            cellPagado.HorizontalAlignment = 1;
            table.AddCell(cellPagado);

            PdfPCell cellPredio = new PdfPCell(new Phrase("Predio", font2));
            //cellPredio.Colspan = 2;
            //cell.Border = 0;
            cellPredio.HorizontalAlignment = 1;
            table.AddCell(cellPredio);

            PdfPCell cellManzana = new PdfPCell(new Phrase("Manzana", font2));
            //cellManzana.Colspan = 2;
            //cell.Border = 0;
            cellManzana.HorizontalAlignment = 1;
            table.AddCell(cellManzana);

            PdfPCell cellLote = new PdfPCell(new Phrase("Lote", font2));
            //cellLote.Colspan = 2;
            //cell.Border = 0;
            cellLote.HorizontalAlignment = 1;
            table.AddCell(cellLote);

            PdfPCell cellMes = new PdfPCell(new Phrase("Mes", font2));
            //cellMes.Colspan = 2;
            //cell.Border = 0;
            cellMes.HorizontalAlignment = 1;
            table.AddCell(cellMes);

            PdfPCell cellfechaPago = new PdfPCell(new Phrase("Fecha pago", font2));
            //cellComprador.Colspan = 2;
            //cell.Border = 0;
            cellfechaPago.HorizontalAlignment = 1;
            table.AddCell(cellfechaPago);

 

            for (int i = 0; i < datosTabla.Rows.Count-1; i++)
            {
                comprador = datosTabla.Rows[i][0].ToString();
                montoTotal = Convert.ToDouble(datosTabla.Rows[i][1].ToString());
                lote = datosTabla.Rows[i][2].ToString();
                manzana = datosTabla.Rows[i][3].ToString();
                fechaProximoPago = datosTabla.Rows[i][4].ToString();
                fechaPago = datosTabla.Rows[i][5].ToString();
                predio = datosTabla.Rows[i][6].ToString();

                cellComprador = new PdfPCell(new Phrase(comprador, font));
                
                cellPagado = new PdfPCell(new Phrase(string.Format("{0:N2}", montoTotal), font));
                cellPagado.HorizontalAlignment = 1;
                cellPredio = new PdfPCell(new Phrase(predio, new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD, BaseColor.BLACK)));
                cellManzana = new PdfPCell(new Phrase(manzana, font));
                cellManzana.HorizontalAlignment = 1;
                cellLote = new PdfPCell(new Phrase(lote, font));
                cellLote.HorizontalAlignment = 1;
                cellMes = new PdfPCell(new Phrase(fechaProximoPago, font));
                cellfechaPago = new PdfPCell(new Phrase(fechaPago, font));

                table.AddCell(cellComprador);
                table.AddCell(cellPagado);
                table.AddCell(cellPredio);
                table.AddCell(cellManzana);
                table.AddCell(cellLote);
                table.AddCell(cellMes);
                table.AddCell(cellfechaPago);

                //parrafo.Add(  "\n\nNOMBRE: " + comprador + "                        PAGA: " + string.Format("{0:N2}",montoTotal)/*25 espacios*/
                //              + Chunk.NEWLINE
                //              + "LOTE: " + lote + "         MANZANA: " + manzana + "         MES: " + Convert.ToDateTime(fechaProximoPago).ToString("MMMM - yyyy") + "               PREDIO: " + predio/*10 espacios*/
                //              + Chunk.NEWLINE);
            }
            parrafo2.Add("\n\n TOTAL " + totalConsulta);

            recibo.Add(parrafo);
            recibo.Add(table);
            recibo.Add(parrafo2);
       
            recibo.Close();

            Process prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = filename;
            prc.Start();
        }
    }
}
