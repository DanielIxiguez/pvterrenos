﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvTerrenos
{
    public partial class FrmTablaDeReestructuracion : Form
    {
        Fecha f = new Fecha();
        string fechaCompra = "";
        string fechaProximoPago = "";
        string mensualidad = "";
        string mensualidadProximoPago = "";
        double totalPagado = 0;
        double totalInteres = 0;
        //double subDivision = 0;
        //double aumentoMensual = 0;

        public FrmTablaDeReestructuracion(string fechaCompra, string fechaProximoPago, string mensualidad, string mensualidadProximoPago)
        {
            InitializeComponent();

            this.fechaCompra = fechaCompra;
            this.fechaProximoPago = fechaProximoPago;
            this.mensualidad = mensualidad;
            this.mensualidadProximoPago = mensualidadProximoPago;

            llenarDgvPagado();
            llenarDgvDeuda();
        }

        private void llenarDgvPagado() 
        {
            int distanciaMes = f.distanciaMes(Convert.ToDateTime(fechaCompra), DateTime.Today);
            
            for(int i =0; i<=distanciaMes; i++)
            {
                if (Convert.ToDateTime(fechaCompra).AddMonths(i).ToString("MM - yyyy") == Convert.ToDateTime(fechaProximoPago).ToString("MM - yyyy"))
                {
                    break;
                }   
                dgvPagado.Rows.Add(Convert.ToDateTime(fechaCompra).AddMonths(i).ToString("dd - MMMM - yyyy"), string.Format("{0:N2}", mensualidad), i + 1);
            }

            sumarTotalPagado(dgvPagado);
        }

        private void llenarDgvDeuda()
        {
            int distanciaMes = f.distanciaMesVerificandoMes(Convert.ToDateTime(fechaProximoPago), DateTime.Today);
            string usarMensualidad = mensualidadProximoPago;
;
            double interes = 0;

            for (int i = 0; i <= distanciaMes; i++)
            {
                if (Convert.ToDateTime(fechaProximoPago).AddMonths(i).ToString("MM - yyyy") == DateTime.Today.ToString("MM - yyyy"))
                {
                    break;
                }
                
                int auxiliar = i;

                interes = Convert.ToDouble(usarMensualidad)* 0.06;

                dgvDeuda.Rows.Add(Convert.ToDateTime(fechaProximoPago).AddMonths(i).ToString("dd - MMMM - yyyy"), string.Format("{0:N2}", interes), string.Format("{0:N2}", usarMensualidad));

                for (int j = 0; j < distanciaMes - auxiliar; j++) 
                {
                    dgvDeuda.Rows.Add("", string.Format("{0:N2}", interes), "");
                }

                if (mensualidad != mensualidadProximoPago) 
                {
                    usarMensualidad = mensualidad;
                }

                //dgvPagado.Rows.Add(Convert.ToDateTime(fechaProximoPago).AddMonths(i).ToString("dd - MMMM - yyyy"), string.Format("{0:N2}", mensualidad), i + 1);
            }

            sumarDeudaTotal(dgvDeuda);
        }

        private void llenarDgvReestructuracion() 
        {
            int distanciaMes = f.distanciaMes(Convert.ToDateTime(fechaCompra), DateTime.Today);
            
            double subDivision = Convert.ToDouble(txtSubDivision.Text);
            double aumentoMensual = Convert.ToDouble(txtAumentoMensual.Text);
            double total = totalPagado - totalInteres - subDivision;
            double nuevasMensualidades = total - (Convert.ToDouble(mensualidad) + aumentoMensual);
            double nuevoTotal = total * nuevasMensualidades;

            lRestructuracion.Text = "Total deuda - Interes - subDivision = " + total; 

            //for (int i = 0; i <= distanciaMes; i++)
            //{
            //    if (Convert.ToDateTime(fechaCompra).AddMonths(i).ToString("MM - yyyy") == Convert.ToDateTime(fechaProximoPago).ToString("MM - yyyy"))
            //    {
            //        break;
            //    }
            //    dgvPagado.Rows.Add(Convert.ToDateTime(fechaCompra).AddMonths(i).ToString("dd - MMMM - yyyy"), string.Format("{0:N2}", mensualidad), i + 1);
            //}

            //sumarTotalPagado(dgvPagado);
        }

        private void sumarTotalPagado(DataGridView dgvTabla)
        {
            double suma = 0;
            int numeroFilas = 0;

            foreach (DataGridViewRow row in dgvTabla.Rows)
            {
                if (row.Cells[2].Value == DBNull.Value)
                    continue;

                double valorcell = 0;
                double.TryParse(Convert.ToString(row.Cells[1].Value), out valorcell);

                suma += Convert.ToDouble(valorcell);
            }

            dgvPagado.Rows.Add("TOTAL", string.Format("{0:N2}", suma),"");

            numeroFilas =  dgvPagado.Rows.Count-2;

            dgvPagado["rowMes", numeroFilas].Style.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);
            dgvPagado["rowMensualidad", numeroFilas].Style.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);

            lPagado.Text = "Total Pagado = " + suma;
            totalPagado = suma;
            //txtTotalPagado.Text = string.Format("{0:N2}", suma);
        }
        
        private void sumarDeudaTotal(DataGridView dgvTabla)
        {
            double suma = 0;
            double sumaDos = 0;
            double total = 0;
            int numeroFilas = 0;

            foreach (DataGridViewRow row in dgvTabla.Rows)
            {
                if (row.Cells["rowInteres"].Value == DBNull.Value)
                    continue;

                double valorcell = 0;
                double.TryParse(Convert.ToString(row.Cells["rowInteres"].Value), out valorcell);

                suma += Convert.ToDouble(valorcell);
            }

            foreach (DataGridViewRow row in dgvTabla.Rows)
            {
                if (row.Cells["rowMensualidadProximoPago"].Value == DBNull.Value)
                    continue;

                double valorcell = 0;
                double.TryParse(Convert.ToString(row.Cells["rowMensualidadProximoPago"].Value), out valorcell);

                sumaDos += Convert.ToDouble(valorcell);
            }

            dgvDeuda.Rows.Add("SUB-TOTAL", string.Format("{0:N2}", suma), string.Format("{0:N2}", sumaDos));

            total = suma + sumaDos;

            dgvDeuda.Rows.Add("TOTAL", "", string.Format("{0:N2}", total));

            numeroFilas = dgvDeuda.Rows.Count - 3;

            dgvDeuda["rowMesEnDeuda", numeroFilas].Style.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);
            dgvDeuda["rowInteres", numeroFilas].Style.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);
            dgvDeuda["rowMensualidadProximoPago", numeroFilas].Style.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);

            dgvDeuda["rowMesEnDeuda", numeroFilas + 1].Style.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);
            dgvDeuda["rowMensualidadProximoPago", numeroFilas+1].Style.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);

            lDeudaTotal.Text = "Total Deuda = " + total;

            lSubDivision.Visible = true;
            txtSubDivision.Visible = true;
            cmdAceptar.Visible = true;

            totalInteres = suma;
            //txtTotalPagado.Text = string.Format("{0:N2}", suma);
        }

        private void cmdAceptar_Click(object sender, EventArgs e)
        {
            if (lSubDivision.Visible == true)
            {
                lSubDivision.Visible = false;
                txtSubDivision.Visible = false;

                lAumentoMensual.Visible = true;
                txtAumentoMensual.Visible = true;
            }
            else 
            {
                lAumentoMensual.Visible = false;
                txtAumentoMensual.Visible = false;

                cmdAceptar.Visible = false;

                lPaso3.Visible = true;
                lTres.Visible = true;
                lRestructuracion.Visible = true;

                llenarDgvReestructuracion();
            }
        }
    }
}
