﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvTerrenos
{
  public  class ConceptoPago : CRUD
    {
            public int id_pago_concepto { get; set; }
            public int fk_id_compraventa { get; set; }
            public string comprador { get; set; }
            public string administracion { get; set; }
            public string fk_id_user { get; set; }
            public string fecha_concepto_pago { get; set; }
            public int cantidad_total { get; set; }
            public int cantidad_pagar { get; set; }
            public string concepto { get; set; }
            public string lote { get; set; }
            public string manzana { get; set; }
            public string predio { get; set; }
            public string status { get; set; }
        private string table = "";
        public List<ConceptoPago> _list = new List<ConceptoPago>();
        public ConceptoPago()
        {
            table = CONCEPTOPAGO;
        }
        #region METODOS
        public void insert()
        {
            execute(table, values(), action.insert.ToString(), "");
        }

        public void update(string where)
        {
            execute(table, values(), action.update.ToString(), where);
        }
        //add

        public void query(string valuesString, string where)
        {
            execute(table, $"{valuesString}", action.query.ToString(), where);
        }
        public void queryFree(string values_string, string query)
        {
            execute(table, $"{values_string}", action.free.ToString(), query);
        }
        #endregion
        public string values()
        {
            return
          //  $"{fk_id_lote.ToLower().Trim()}{cc}" +
            $"{fk_id_compraventa}{cc}" +
            $"{comprador}{cc}" +
            $"{administracion.ToUpper().Trim()}{cc}" +
            $"{fk_id_user}{cc}" +
            $"{fecha_concepto_pago.ToUpper().Trim()}{cc}" +
            $"{cantidad_total}{cc}" +
            $"{cantidad_pagar}{cc}" + 
            $"{concepto.ToUpper().Trim()}{cc}" +
            $"{lote}{cc}" +
            $"{manzana}{cc}" +
            $"{predio}{cc}" +
            $"{status.ToUpper().Trim()}";
        }
    }
}
