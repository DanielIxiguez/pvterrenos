﻿   namespace PvTerrenos
{
    partial class FrmVentaLote
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVentaLote));
            this.label3 = new System.Windows.Forms.Label();
            this.cbVendedores = new System.Windows.Forms.ComboBox();
            this.cbNombre = new System.Windows.Forms.ComboBox();
            this.dtpProximoPago = new System.Windows.Forms.DateTimePicker();
            this.lProximoPago = new System.Windows.Forms.Label();
            this.cbEstatus = new System.Windows.Forms.ComboBox();
            this.lEstatusVenta = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMensualidad = new System.Windows.Forms.TextBox();
            this.dtpFechaVenta = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.cbLotes = new System.Windows.Forms.ComboBox();
            this.cbManzana = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPagoActual = new System.Windows.Forms.TextBox();
            this.cbPredio = new System.Windows.Forms.ComboBox();
            this.lNoPago = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.dgvHistorial = new System.Windows.Forms.DataGridView();
            this.dgvNombre = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgvId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCostoTerreno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMensualidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvgPredio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cMesPagado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvgManzana = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.predioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.usuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsBtnPago = new System.Windows.Forms.ToolStripMenuItem();
            this.consultasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteDiarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atrasadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comisionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarcomoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.imprimirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vistapreviadeimpresiónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deshacerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rehacerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.cortarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pegarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.seleccionartodoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.herramientasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.personalizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contenidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.índiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.acercadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsBtnCliente = new System.Windows.Forms.ToolStripButton();
            this.tsBtnPredio = new System.Windows.Forms.ToolStripButton();
            this.tsBtnUsuario = new System.Windows.Forms.ToolStripButton();
            this.tsBtnConsulta = new System.Windows.Forms.ToolStripSplitButton();
            this.tsMenuReporteDiario = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuAtrasados = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuHistorial = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuComisiones = new System.Windows.Forms.ToolStripMenuItem();
            this.tsBtnPagar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsBtnGenerarVenta = new System.Windows.Forms.ToolStripButton();
            this.tsBtnLimpiar = new System.Windows.Forms.ToolStripButton();
            this.tsBtnReestructurar = new System.Windows.Forms.ToolStripButton();
            this.tsBtnResumen = new System.Windows.Forms.ToolStripButton();
            this.txtInteres = new System.Windows.Forms.TextBox();
            this.lInteres = new System.Windows.Forms.Label();
            this.txtMonto = new System.Windows.Forms.TextBox();
            this.lMonto = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.lTotal = new System.Windows.Forms.Label();
            this.direccionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistorial)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(549, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 20);
            this.label3.TabIndex = 40;
            this.label3.Text = "Vendedor";
            // 
            // cbVendedores
            // 
            this.cbVendedores.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbVendedores.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbVendedores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbVendedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVendedores.FormattingEnabled = true;
            this.cbVendedores.Location = new System.Drawing.Point(552, 141);
            this.cbVendedores.Name = "cbVendedores";
            this.cbVendedores.Size = new System.Drawing.Size(199, 37);
            this.cbVendedores.TabIndex = 2;
            // 
            // cbNombre
            // 
            this.cbNombre.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbNombre.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbNombre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNombre.FormattingEnabled = true;
            this.cbNombre.Location = new System.Drawing.Point(26, 141);
            this.cbNombre.Name = "cbNombre";
            this.cbNombre.Size = new System.Drawing.Size(512, 37);
            this.cbNombre.TabIndex = 1;
            this.cbNombre.SelectedIndexChanged += new System.EventHandler(this.cbNombre_SelectedIndexChanged);
            // 
            // dtpProximoPago
            // 
            this.dtpProximoPago.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpProximoPago.CustomFormat = "mmmm-yyyy";
            this.dtpProximoPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpProximoPago.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpProximoPago.Location = new System.Drawing.Point(355, 293);
            this.dtpProximoPago.Name = "dtpProximoPago";
            this.dtpProximoPago.Size = new System.Drawing.Size(195, 34);
            this.dtpProximoPago.TabIndex = 10;
            // 
            // lProximoPago
            // 
            this.lProximoPago.AutoSize = true;
            this.lProximoPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lProximoPago.Location = new System.Drawing.Point(352, 274);
            this.lProximoPago.Name = "lProximoPago";
            this.lProximoPago.Size = new System.Drawing.Size(160, 20);
            this.lProximoPago.TabIndex = 53;
            this.lProximoPago.Text = "Fecha proximo pago";
            // 
            // cbEstatus
            // 
            this.cbEstatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEstatus.FormattingEnabled = true;
            this.cbEstatus.ItemHeight = 25;
            this.cbEstatus.Items.AddRange(new object[] {
            "ACTIVO",
            "OFICINA",
            "TRASPASO",
            "LIQUIDADO",
            "CANCELADO",
            "ESCRITURADO",
            "PARA ESCRITURAR",
            "REESTRUCTURADO"});
            this.cbEstatus.Location = new System.Drawing.Point(99, 294);
            this.cbEstatus.Name = "cbEstatus";
            this.cbEstatus.Size = new System.Drawing.Size(246, 33);
            this.cbEstatus.TabIndex = 9;
            this.cbEstatus.Text = "0";
            // 
            // lEstatusVenta
            // 
            this.lEstatusVenta.AutoSize = true;
            this.lEstatusVenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lEstatusVenta.Location = new System.Drawing.Point(96, 274);
            this.lEstatusVenta.Name = "lEstatusVenta";
            this.lEstatusVenta.Size = new System.Drawing.Size(111, 20);
            this.lEstatusVenta.TabIndex = 51;
            this.lEstatusVenta.Text = "Estatus venta";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 119);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Comprador";
            // 
            // txtMensualidad
            // 
            this.txtMensualidad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMensualidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMensualidad.Location = new System.Drawing.Point(421, 223);
            this.txtMensualidad.Name = "txtMensualidad";
            this.txtMensualidad.Size = new System.Drawing.Size(116, 30);
            this.txtMensualidad.TabIndex = 3;
            // 
            // dtpFechaVenta
            // 
            this.dtpFechaVenta.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFechaVenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFechaVenta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaVenta.Location = new System.Drawing.Point(552, 213);
            this.dtpFechaVenta.Name = "dtpFechaVenta";
            this.dtpFechaVenta.Size = new System.Drawing.Size(199, 53);
            this.dtpFechaVenta.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(549, 194);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 20);
            this.label2.TabIndex = 49;
            this.label2.Text = "Fecha de  compra";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(418, 199);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(103, 20);
            this.label18.TabIndex = 43;
            this.label18.Text = "Mensualidad";
            // 
            // cbLotes
            // 
            this.cbLotes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbLotes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbLotes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbLotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLotes.FormattingEnabled = true;
            this.cbLotes.ItemHeight = 29;
            this.cbLotes.Location = new System.Drawing.Point(355, 220);
            this.cbLotes.Name = "cbLotes";
            this.cbLotes.Size = new System.Drawing.Size(60, 37);
            this.cbLotes.TabIndex = 6;
            this.cbLotes.SelectedIndexChanged += new System.EventHandler(this.cbLotes_SelectedIndexChanged);
            // 
            // cbManzana
            // 
            this.cbManzana.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbManzana.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbManzana.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbManzana.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbManzana.FormattingEnabled = true;
            this.cbManzana.ItemHeight = 29;
            this.cbManzana.Location = new System.Drawing.Point(285, 220);
            this.cbManzana.Name = "cbManzana";
            this.cbManzana.Size = new System.Drawing.Size(60, 37);
            this.cbManzana.TabIndex = 5;
            this.cbManzana.SelectedIndexChanged += new System.EventHandler(this.cbManzana_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(352, 199);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "Lote";
            // 
            // txtPagoActual
            // 
            this.txtPagoActual.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPagoActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPagoActual.Location = new System.Drawing.Point(27, 296);
            this.txtPagoActual.Name = "txtPagoActual";
            this.txtPagoActual.Size = new System.Drawing.Size(59, 30);
            this.txtPagoActual.TabIndex = 8;
            this.txtPagoActual.Text = " 0";
            this.txtPagoActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbPredio
            // 
            this.cbPredio.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbPredio.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPredio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbPredio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPredio.FormattingEnabled = true;
            this.cbPredio.ItemHeight = 29;
            this.cbPredio.Location = new System.Drawing.Point(27, 220);
            this.cbPredio.Name = "cbPredio";
            this.cbPredio.Size = new System.Drawing.Size(252, 37);
            this.cbPredio.TabIndex = 4;
            this.cbPredio.SelectedIndexChanged += new System.EventHandler(this.cbPredio_SelectedIndexChanged);
            // 
            // lNoPago
            // 
            this.lNoPago.AutoSize = true;
            this.lNoPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lNoPago.Location = new System.Drawing.Point(24, 274);
            this.lNoPago.Name = "lNoPago";
            this.lNoPago.Size = new System.Drawing.Size(73, 20);
            this.lNoPago.TabIndex = 45;
            this.lNoPago.Text = "No Pago";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 198);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 20);
            this.label8.TabIndex = 29;
            this.label8.Text = "Predio";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(282, 199);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 20);
            this.label13.TabIndex = 32;
            this.label13.Text = "Manzana";
            // 
            // dgvHistorial
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            this.dgvHistorial.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvHistorial.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHistorial.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvHistorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistorial.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvNombre,
            this.dgvId,
            this.dgvCostoTerreno,
            this.dgvMensualidad,
            this.dvgPredio,
            this.cMesPagado,
            this.dvgManzana});
            this.dgvHistorial.Location = new System.Drawing.Point(17, 366);
            this.dgvHistorial.Name = "dgvHistorial";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvHistorial.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvHistorial.Size = new System.Drawing.Size(739, 205);
            this.dgvHistorial.TabIndex = 25;
            // 
            // dgvNombre
            // 
            this.dgvNombre.HeaderText = "Eliminar pago";
            this.dgvNombre.Name = "dgvNombre";
            this.dgvNombre.ReadOnly = true;
            this.dgvNombre.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNombre.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvNombre.Text = "Eliminar";
            // 
            // dgvId
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvId.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvId.HeaderText = "id";
            this.dgvId.Name = "dgvId";
            this.dgvId.ReadOnly = true;
            // 
            // dgvCostoTerreno
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvCostoTerreno.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvCostoTerreno.HeaderText = "No Pago";
            this.dgvCostoTerreno.Name = "dgvCostoTerreno";
            this.dgvCostoTerreno.ReadOnly = true;
            // 
            // dgvMensualidad
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvMensualidad.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvMensualidad.HeaderText = "Monto";
            this.dgvMensualidad.Name = "dgvMensualidad";
            this.dgvMensualidad.ReadOnly = true;
            // 
            // dvgPredio
            // 
            this.dvgPredio.HeaderText = "Concepto";
            this.dvgPredio.Name = "dvgPredio";
            this.dvgPredio.ReadOnly = true;
            // 
            // cMesPagado
            // 
            this.cMesPagado.HeaderText = "Mes pagado";
            this.cMesPagado.Name = "cMesPagado";
            // 
            // dvgManzana
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dvgManzana.DefaultCellStyle = dataGridViewCellStyle13;
            this.dvgManzana.HeaderText = "Fecha pago";
            this.dvgManzana.Name = "dvgManzana";
            this.dvgManzana.ReadOnly = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clientesToolStripMenuItem,
            this.tsBtnPago,
            this.consultasToolStripMenuItem,
            this.archivoToolStripMenuItem,
            this.editarToolStripMenuItem,
            this.herramientasToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(775, 24);
            this.menuStrip1.TabIndex = 27;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clienteToolStripMenuItem,
            this.predioToolStripMenuItem1,
            this.usuarioToolStripMenuItem});
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.clientesToolStripMenuItem.Text = "Nuevo";
            // 
            // clienteToolStripMenuItem
            // 
            this.clienteToolStripMenuItem.Name = "clienteToolStripMenuItem";
            this.clienteToolStripMenuItem.Size = new System.Drawing.Size(134, 26);
            this.clienteToolStripMenuItem.Text = "Cliente";
            // 
            // predioToolStripMenuItem1
            // 
            this.predioToolStripMenuItem1.Name = "predioToolStripMenuItem1";
            this.predioToolStripMenuItem1.Size = new System.Drawing.Size(134, 26);
            this.predioToolStripMenuItem1.Text = "Predio";
            // 
            // usuarioToolStripMenuItem
            // 
            this.usuarioToolStripMenuItem.Name = "usuarioToolStripMenuItem";
            this.usuarioToolStripMenuItem.Size = new System.Drawing.Size(134, 26);
            this.usuarioToolStripMenuItem.Text = "Usuario";
            // 
            // tsBtnPago
            // 
            this.tsBtnPago.Name = "tsBtnPago";
            this.tsBtnPago.Size = new System.Drawing.Size(54, 20);
            this.tsBtnPago.Text = "Pago";
            this.tsBtnPago.Click += new System.EventHandler(this.tsBtnPago_Click_1);
            // 
            // consultasToolStripMenuItem
            // 
            this.consultasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pagoToolStripMenuItem,
            this.reporteDiarioToolStripMenuItem,
            this.atrasadosToolStripMenuItem,
            this.historialToolStripMenuItem,
            this.comisionesToolStripMenuItem});
            this.consultasToolStripMenuItem.Name = "consultasToolStripMenuItem";
            this.consultasToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.consultasToolStripMenuItem.Text = "Consulta";
            // 
            // pagoToolStripMenuItem
            // 
            this.pagoToolStripMenuItem.Name = "pagoToolStripMenuItem";
            this.pagoToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.pagoToolStripMenuItem.Text = "pago";
            this.pagoToolStripMenuItem.Click += new System.EventHandler(this.pagoToolStripMenuItem_Click);
            // 
            // reporteDiarioToolStripMenuItem
            // 
            this.reporteDiarioToolStripMenuItem.Name = "reporteDiarioToolStripMenuItem";
            this.reporteDiarioToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.reporteDiarioToolStripMenuItem.Text = "reporte diario";
            this.reporteDiarioToolStripMenuItem.Click += new System.EventHandler(this.reporteDiarioToolStripMenuItem_Click);
            // 
            // atrasadosToolStripMenuItem
            // 
            this.atrasadosToolStripMenuItem.Name = "atrasadosToolStripMenuItem";
            this.atrasadosToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.atrasadosToolStripMenuItem.Text = "atrasados";
            this.atrasadosToolStripMenuItem.Click += new System.EventHandler(this.atrasadosToolStripMenuItem_Click);
            // 
            // historialToolStripMenuItem
            // 
            this.historialToolStripMenuItem.Name = "historialToolStripMenuItem";
            this.historialToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.historialToolStripMenuItem.Text = "historial";
            this.historialToolStripMenuItem.Click += new System.EventHandler(this.historialToolStripMenuItem_Click);
            // 
            // comisionesToolStripMenuItem
            // 
            this.comisionesToolStripMenuItem.Name = "comisionesToolStripMenuItem";
            this.comisionesToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.comisionesToolStripMenuItem.Text = "comisiones";
            this.comisionesToolStripMenuItem.Click += new System.EventHandler(this.comisionesToolStripMenuItem_Click);
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoToolStripMenuItem,
            this.abrirToolStripMenuItem,
            this.toolStripSeparator,
            this.guardarToolStripMenuItem,
            this.guardarcomoToolStripMenuItem,
            this.toolStripSeparator2,
            this.imprimirToolStripMenuItem,
            this.vistapreviadeimpresiónToolStripMenuItem,
            this.toolStripSeparator3,
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.archivoToolStripMenuItem.Text = "&Archivo";
            // 
            // nuevoToolStripMenuItem
            // 
            this.nuevoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("nuevoToolStripMenuItem.Image")));
            this.nuevoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.nuevoToolStripMenuItem.Name = "nuevoToolStripMenuItem";
            this.nuevoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.nuevoToolStripMenuItem.Size = new System.Drawing.Size(252, 26);
            this.nuevoToolStripMenuItem.Text = "&Nuevo";
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("abrirToolStripMenuItem.Image")));
            this.abrirToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(252, 26);
            this.abrirToolStripMenuItem.Text = "&Abrir";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(249, 6);
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("guardarToolStripMenuItem.Image")));
            this.guardarToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(252, 26);
            this.guardarToolStripMenuItem.Text = "&Guardar";
            // 
            // guardarcomoToolStripMenuItem
            // 
            this.guardarcomoToolStripMenuItem.Name = "guardarcomoToolStripMenuItem";
            this.guardarcomoToolStripMenuItem.Size = new System.Drawing.Size(252, 26);
            this.guardarcomoToolStripMenuItem.Text = "G&uardar como";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(249, 6);
            // 
            // imprimirToolStripMenuItem
            // 
            this.imprimirToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("imprimirToolStripMenuItem.Image")));
            this.imprimirToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.imprimirToolStripMenuItem.Name = "imprimirToolStripMenuItem";
            this.imprimirToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.imprimirToolStripMenuItem.Size = new System.Drawing.Size(252, 26);
            this.imprimirToolStripMenuItem.Text = "&Imprimir";
            // 
            // vistapreviadeimpresiónToolStripMenuItem
            // 
            this.vistapreviadeimpresiónToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("vistapreviadeimpresiónToolStripMenuItem.Image")));
            this.vistapreviadeimpresiónToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.vistapreviadeimpresiónToolStripMenuItem.Name = "vistapreviadeimpresiónToolStripMenuItem";
            this.vistapreviadeimpresiónToolStripMenuItem.Size = new System.Drawing.Size(252, 26);
            this.vistapreviadeimpresiónToolStripMenuItem.Text = "&Vista previa de impresión";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(249, 6);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(252, 26);
            this.salirToolStripMenuItem.Text = "&Salir";
            // 
            // editarToolStripMenuItem
            // 
            this.editarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deshacerToolStripMenuItem,
            this.rehacerToolStripMenuItem,
            this.toolStripSeparator4,
            this.cortarToolStripMenuItem,
            this.copiarToolStripMenuItem,
            this.pegarToolStripMenuItem,
            this.toolStripSeparator5,
            this.seleccionartodoToolStripMenuItem});
            this.editarToolStripMenuItem.Name = "editarToolStripMenuItem";
            this.editarToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.editarToolStripMenuItem.Text = "&Editar";
            // 
            // deshacerToolStripMenuItem
            // 
            this.deshacerToolStripMenuItem.Name = "deshacerToolStripMenuItem";
            this.deshacerToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.deshacerToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.deshacerToolStripMenuItem.Text = "&Deshacer";
            // 
            // rehacerToolStripMenuItem
            // 
            this.rehacerToolStripMenuItem.Name = "rehacerToolStripMenuItem";
            this.rehacerToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.rehacerToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.rehacerToolStripMenuItem.Text = "&Rehacer";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(193, 6);
            // 
            // cortarToolStripMenuItem
            // 
            this.cortarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cortarToolStripMenuItem.Image")));
            this.cortarToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cortarToolStripMenuItem.Name = "cortarToolStripMenuItem";
            this.cortarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cortarToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.cortarToolStripMenuItem.Text = "Cor&tar";
            // 
            // copiarToolStripMenuItem
            // 
            this.copiarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copiarToolStripMenuItem.Image")));
            this.copiarToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copiarToolStripMenuItem.Name = "copiarToolStripMenuItem";
            this.copiarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copiarToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.copiarToolStripMenuItem.Text = "&Copiar";
            // 
            // pegarToolStripMenuItem
            // 
            this.pegarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pegarToolStripMenuItem.Image")));
            this.pegarToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pegarToolStripMenuItem.Name = "pegarToolStripMenuItem";
            this.pegarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pegarToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.pegarToolStripMenuItem.Text = "&Pegar";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(193, 6);
            // 
            // seleccionartodoToolStripMenuItem
            // 
            this.seleccionartodoToolStripMenuItem.Name = "seleccionartodoToolStripMenuItem";
            this.seleccionartodoToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.seleccionartodoToolStripMenuItem.Text = "&Seleccionar todo";
            // 
            // herramientasToolStripMenuItem
            // 
            this.herramientasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.personalizarToolStripMenuItem,
            this.opcionesToolStripMenuItem});
            this.herramientasToolStripMenuItem.Name = "herramientasToolStripMenuItem";
            this.herramientasToolStripMenuItem.Size = new System.Drawing.Size(110, 20);
            this.herramientasToolStripMenuItem.Text = "&Herramientas";
            // 
            // personalizarToolStripMenuItem
            // 
            this.personalizarToolStripMenuItem.Name = "personalizarToolStripMenuItem";
            this.personalizarToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.personalizarToolStripMenuItem.Text = "&Personalizar";
            // 
            // opcionesToolStripMenuItem
            // 
            this.opcionesToolStripMenuItem.Name = "opcionesToolStripMenuItem";
            this.opcionesToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.opcionesToolStripMenuItem.Text = "&Opciones";
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contenidoToolStripMenuItem,
            this.índiceToolStripMenuItem,
            this.buscarToolStripMenuItem,
            this.toolStripSeparator6,
            this.acercadeToolStripMenuItem});
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.ayudaToolStripMenuItem.Text = "Ay&uda";
            // 
            // contenidoToolStripMenuItem
            // 
            this.contenidoToolStripMenuItem.Name = "contenidoToolStripMenuItem";
            this.contenidoToolStripMenuItem.Size = new System.Drawing.Size(159, 26);
            this.contenidoToolStripMenuItem.Text = "&Contenido";
            // 
            // índiceToolStripMenuItem
            // 
            this.índiceToolStripMenuItem.Name = "índiceToolStripMenuItem";
            this.índiceToolStripMenuItem.Size = new System.Drawing.Size(159, 26);
            this.índiceToolStripMenuItem.Text = "Índic&e";
            // 
            // buscarToolStripMenuItem
            // 
            this.buscarToolStripMenuItem.Name = "buscarToolStripMenuItem";
            this.buscarToolStripMenuItem.Size = new System.Drawing.Size(159, 26);
            this.buscarToolStripMenuItem.Text = "&Buscar";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(156, 6);
            // 
            // acercadeToolStripMenuItem
            // 
            this.acercadeToolStripMenuItem.Name = "acercadeToolStripMenuItem";
            this.acercadeToolStripMenuItem.Size = new System.Drawing.Size(159, 26);
            this.acercadeToolStripMenuItem.Text = "&Acerca de...";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnCliente,
            this.tsBtnPredio,
            this.tsBtnUsuario,
            this.tsBtnConsulta,
            this.tsBtnPagar,
            this.toolStripSeparator1,
            this.tsBtnGenerarVenta,
            this.tsBtnLimpiar,
            this.tsBtnReestructurar,
            this.tsBtnResumen});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(775, 91);
            this.toolStrip1.TabIndex = 101;
            this.toolStrip1.Text = " ";
            // 
            // tsBtnCliente
            // 
            this.tsBtnCliente.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnCliente.Image")));
            this.tsBtnCliente.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnCliente.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnCliente.Name = "tsBtnCliente";
            this.tsBtnCliente.Size = new System.Drawing.Size(89, 88);
            this.tsBtnCliente.Text = "Comprador";
            this.tsBtnCliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnCliente.Visible = false;
            this.tsBtnCliente.Click += new System.EventHandler(this.tsBtnCliente_Click);
            // 
            // tsBtnPredio
            // 
            this.tsBtnPredio.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnPredio.Image")));
            this.tsBtnPredio.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnPredio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnPredio.Name = "tsBtnPredio";
            this.tsBtnPredio.Size = new System.Drawing.Size(68, 88);
            this.tsBtnPredio.Text = "Predio";
            this.tsBtnPredio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnPredio.Visible = false;
            this.tsBtnPredio.Click += new System.EventHandler(this.tsBtnPredio_Click);
            // 
            // tsBtnUsuario
            // 
            this.tsBtnUsuario.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnUsuario.Image")));
            this.tsBtnUsuario.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnUsuario.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnUsuario.Name = "tsBtnUsuario";
            this.tsBtnUsuario.Size = new System.Drawing.Size(68, 88);
            this.tsBtnUsuario.Text = "Usuario";
            this.tsBtnUsuario.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnUsuario.Visible = false;
            this.tsBtnUsuario.Click += new System.EventHandler(this.tsBtnUsuario_Click);
            // 
            // tsBtnConsulta
            // 
            this.tsBtnConsulta.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMenuReporteDiario,
            this.tsMenuAtrasados,
            this.tsMenuHistorial,
            this.tsMenuComisiones,
            this.direccionToolStripMenuItem});
            this.tsBtnConsulta.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnConsulta.Image")));
            this.tsBtnConsulta.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnConsulta.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnConsulta.Name = "tsBtnConsulta";
            this.tsBtnConsulta.Size = new System.Drawing.Size(91, 88);
            this.tsBtnConsulta.Text = "Consultas";
            this.tsBtnConsulta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnConsulta.Visible = false;
            this.tsBtnConsulta.MouseHover += new System.EventHandler(this.tsBtnConsulta_MouseHover);
            // 
            // tsMenuReporteDiario
            // 
            this.tsMenuReporteDiario.Name = "tsMenuReporteDiario";
            this.tsMenuReporteDiario.Size = new System.Drawing.Size(182, 26);
            this.tsMenuReporteDiario.Text = "Reporte Diario";
            this.tsMenuReporteDiario.Click += new System.EventHandler(this.tsMenuReporteDiario_Click);
            // 
            // tsMenuAtrasados
            // 
            this.tsMenuAtrasados.Name = "tsMenuAtrasados";
            this.tsMenuAtrasados.Size = new System.Drawing.Size(182, 26);
            this.tsMenuAtrasados.Text = "Atrasados";
            this.tsMenuAtrasados.Click += new System.EventHandler(this.tsMenuAtrasados_Click);
            // 
            // tsMenuHistorial
            // 
            this.tsMenuHistorial.Name = "tsMenuHistorial";
            this.tsMenuHistorial.Size = new System.Drawing.Size(182, 26);
            this.tsMenuHistorial.Text = "Historial";
            this.tsMenuHistorial.Click += new System.EventHandler(this.tsMenuHistorial_Click);
            // 
            // tsMenuComisiones
            // 
            this.tsMenuComisiones.Name = "tsMenuComisiones";
            this.tsMenuComisiones.Size = new System.Drawing.Size(182, 26);
            this.tsMenuComisiones.Text = "Comisiones";
            this.tsMenuComisiones.Click += new System.EventHandler(this.tsMenuComisiones_Click);
            // 
            // tsBtnPagar
            // 
            this.tsBtnPagar.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnPagar.Image")));
            this.tsBtnPagar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnPagar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnPagar.Name = "tsBtnPagar";
            this.tsBtnPagar.Size = new System.Drawing.Size(112, 88);
            this.tsBtnPagar.Text = "Punto de venta";
            this.tsBtnPagar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnPagar.Click += new System.EventHandler(this.tsBtnPago_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 91);
            this.toolStripSeparator1.Visible = false;
            // 
            // tsBtnGenerarVenta
            // 
            this.tsBtnGenerarVenta.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnGenerarVenta.Image")));
            this.tsBtnGenerarVenta.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnGenerarVenta.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnGenerarVenta.Name = "tsBtnGenerarVenta";
            this.tsBtnGenerarVenta.Size = new System.Drawing.Size(106, 88);
            this.tsBtnGenerarVenta.Text = "Generar Venta";
            this.tsBtnGenerarVenta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnGenerarVenta.Visible = false;
            this.tsBtnGenerarVenta.Click += new System.EventHandler(this.tsBtnAgregarVenta_Click);
            // 
            // tsBtnLimpiar
            // 
            this.tsBtnLimpiar.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnLimpiar.Image")));
            this.tsBtnLimpiar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnLimpiar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnLimpiar.Name = "tsBtnLimpiar";
            this.tsBtnLimpiar.Size = new System.Drawing.Size(68, 88);
            this.tsBtnLimpiar.Text = "Limpiar";
            this.tsBtnLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnLimpiar.Click += new System.EventHandler(this.tsBtnLimpiar_Click);
            // 
            // tsBtnReestructurar
            // 
            this.tsBtnReestructurar.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnReestructurar.Image")));
            this.tsBtnReestructurar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnReestructurar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnReestructurar.Name = "tsBtnReestructurar";
            this.tsBtnReestructurar.Size = new System.Drawing.Size(100, 88);
            this.tsBtnReestructurar.Text = "Reestructurar";
            this.tsBtnReestructurar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnReestructurar.Visible = false;
            // 
            // tsBtnResumen
            // 
            this.tsBtnResumen.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnResumen.Image")));
            this.tsBtnResumen.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnResumen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnResumen.Name = "tsBtnResumen";
            this.tsBtnResumen.Size = new System.Drawing.Size(73, 88);
            this.tsBtnResumen.Text = "Resumen";
            this.tsBtnResumen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnResumen.Visible = false;
            // 
            // txtInteres
            // 
            this.txtInteres.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInteres.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInteres.Location = new System.Drawing.Point(452, 392);
            this.txtInteres.Name = "txtInteres";
            this.txtInteres.Size = new System.Drawing.Size(109, 27);
            this.txtInteres.TabIndex = 102;
            this.txtInteres.Text = "0";
            this.txtInteres.Visible = false;
            // 
            // lInteres
            // 
            this.lInteres.AutoSize = true;
            this.lInteres.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lInteres.Location = new System.Drawing.Point(398, 395);
            this.lInteres.Name = "lInteres";
            this.lInteres.Size = new System.Drawing.Size(60, 20);
            this.lInteres.TabIndex = 103;
            this.lInteres.Text = "Interes";
            this.lInteres.Visible = false;
            // 
            // txtMonto
            // 
            this.txtMonto.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonto.Location = new System.Drawing.Point(452, 456);
            this.txtMonto.Name = "txtMonto";
            this.txtMonto.Size = new System.Drawing.Size(109, 27);
            this.txtMonto.TabIndex = 104;
            this.txtMonto.Text = "0";
            this.txtMonto.Visible = false;
            // 
            // lMonto
            // 
            this.lMonto.AutoSize = true;
            this.lMonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lMonto.Location = new System.Drawing.Point(401, 459);
            this.lMonto.Name = "lMonto";
            this.lMonto.Size = new System.Drawing.Size(55, 20);
            this.lMonto.TabIndex = 105;
            this.lMonto.Text = "Monto";
            this.lMonto.Visible = false;
            this.lMonto.Click += new System.EventHandler(this.lMonto_Click);
            // 
            // txtTotal
            // 
            this.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(452, 424);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(109, 27);
            this.txtTotal.TabIndex = 106;
            this.txtTotal.Text = "0";
            this.txtTotal.Visible = false;
            // 
            // lTotal
            // 
            this.lTotal.AutoSize = true;
            this.lTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTotal.Location = new System.Drawing.Point(407, 427);
            this.lTotal.Name = "lTotal";
            this.lTotal.Size = new System.Drawing.Size(46, 20);
            this.lTotal.TabIndex = 107;
            this.lTotal.Text = "Total";
            this.lTotal.Visible = false;
            // 
            // direccionToolStripMenuItem
            // 
            this.direccionToolStripMenuItem.Name = "direccionToolStripMenuItem";
            this.direccionToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.direccionToolStripMenuItem.Text = "Direccion";
            this.direccionToolStripMenuItem.Click += new System.EventHandler(this.direccionToolStripMenuItem_Click);
            // 
            // FrmVentaLote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 361);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.lTotal);
            this.Controls.Add(this.txtMonto);
            this.Controls.Add(this.lMonto);
            this.Controls.Add(this.txtInteres);
            this.Controls.Add(this.lInteres);
            this.Controls.Add(this.dtpProximoPago);
            this.Controls.Add(this.lProximoPago);
            this.Controls.Add(this.cbEstatus);
            this.Controls.Add(this.lEstatusVenta);
            this.Controls.Add(this.cbNombre);
            this.Controls.Add(this.cbPredio);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cbVendedores);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lNoPago);
            this.Controls.Add(this.txtMensualidad);
            this.Controls.Add(this.txtPagoActual);
            this.Controls.Add(this.dtpFechaVenta);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbManzana);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.cbLotes);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dgvHistorial);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmVentaLote";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Compra venta";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmVentaLote_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistorial)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbLotes;
        private System.Windows.Forms.ComboBox cbManzana;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbPredio;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgvHistorial;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsBtnPago;
        private System.Windows.Forms.ToolStripMenuItem clienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem predioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultasToolStripMenuItem;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtMensualidad;
        private System.Windows.Forms.TextBox txtPagoActual;
        private System.Windows.Forms.Label lNoPago;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbNombre;
        private System.Windows.Forms.DateTimePicker dtpFechaVenta;
        private System.Windows.Forms.ToolStripMenuItem pagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteDiarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atrasadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuarioToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbVendedores;
        private System.Windows.Forms.ToolStripMenuItem comisionesToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsBtnCliente;
        private System.Windows.Forms.ToolStripButton tsBtnPredio;
        private System.Windows.Forms.ToolStripButton tsBtnUsuario;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsBtnPagar;
        private System.Windows.Forms.ComboBox cbEstatus;
        private System.Windows.Forms.Label lEstatusVenta;
        private System.Windows.Forms.DateTimePicker dtpProximoPago;
        private System.Windows.Forms.Label lProximoPago;
        private System.Windows.Forms.ToolStripButton tsBtnGenerarVenta;
        private System.Windows.Forms.ToolStripButton tsBtnReestructurar;
        private System.Windows.Forms.ToolStripButton tsBtnLimpiar;
        private System.Windows.Forms.TextBox txtInteres;
        private System.Windows.Forms.Label lInteres;
        private System.Windows.Forms.TextBox txtMonto;
        private System.Windows.Forms.Label lMonto;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label lTotal;
        private System.Windows.Forms.ToolStripButton tsBtnResumen;
        private System.Windows.Forms.DataGridViewButtonColumn dgvNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvCostoTerreno;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMensualidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvgPredio;
        private System.Windows.Forms.DataGridViewTextBoxColumn cMesPagado;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvgManzana;
        private System.Windows.Forms.ToolStripSplitButton tsBtnConsulta;
        private System.Windows.Forms.ToolStripMenuItem tsMenuReporteDiario;
        private System.Windows.Forms.ToolStripMenuItem tsMenuAtrasados;
        private System.Windows.Forms.ToolStripMenuItem tsMenuHistorial;
        private System.Windows.Forms.ToolStripMenuItem tsMenuComisiones;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarcomoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem imprimirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vistapreviadeimpresiónToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deshacerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rehacerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem cortarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copiarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pegarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem seleccionartodoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem herramientasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem personalizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contenidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem índiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem acercadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem direccionToolStripMenuItem;
    }
}

