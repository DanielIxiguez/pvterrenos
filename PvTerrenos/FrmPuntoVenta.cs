﻿using System;
using System.Collections.Generic; 
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvTerrenos
{
    public partial class FrmPuntoVenta : Form
    {
        WSpvt.PVT ws = new WSpvt.PVT();
        Fecha f = new Fecha();
        DetallePago _detallePago;
        Interes interes = new Interes();
        PdfCreate recibo = new PdfCreate();
        ConceptoPago CP = new ConceptoPago();

        static List<ResultQueryCP> _ListresultQueryCP = new List<ResultQueryCP>();
        static List<resultconceptospagos> _ListresultQueryCPconceptos = new List<resultconceptospagos>();

        string idVenta = "";
        string pagoActual = "";
        string pagoActualRecibo = "";//algo feo que hay que borrar, se usa como un auxiliar 
        string proximoPago = "";
        string mensualidadDeVentas = "";
        string mensualidadProximoPago = "";
        string fechaCompra = "";
        string statusMora = "";
        string usuario = "";
        bool bandera = true;
        string privilegios = "";
        int idUsuario = 0;
        string administracion = "";
        string administracionUsuario = "";
        //string idMesActualizar;
        string[] splitIdMora;
        string[] splitIdLote;
        string[] splitFechaMora;
        string[] splitMontoMora;
        string[] splitDatosDetallePago;

        int indexComboLote = 0;
        int[] arrayMeses;
        //double totalInteres = 0;
        //double sumaMesMora;
        //double diferencia;
        int actualizo = 0;
        bool vengoDeComboCliente = false;
        
       
        

        public FrmPuntoVenta(User user)
        {
            InitializeComponent();
            this.usuario = user.nombre;
            this.privilegios = user.tipo.ToString();
            this.idUsuario = user.id_user;
            this.administracionUsuario = user.administracion;
            /*this.usuario = usuario;
            this.privilegios = nivelUsuario;
            this.idUsuario = idUsuario;
            this.administracion = administracion;*/

         //   cbConcepto.Visible = false;


            llenarComboComprador();
            inicializarPermisos();
            pbPagos.Minimum = 0;
            pbPagos.Maximum = 100;
        }

     
        public string setProximoPago(string fecha)
        {
            return Convert.ToDateTime(fecha).ToString("dd/MM/yyyy HH:mm:ss");//fecha.Substring(0, 20);
        }

        public void setProximoPago()
        {
            proximoPago = Convert.ToDateTime(proximoPago).ToString("dd/MM/yyyy HH:mm:ss");//proximoPago.Substring(0, 20);
        }

        private void txtId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                cbComprador.Text = ws.getNombreComprador(txtId.Text);
            }
        }

        private void cbComprador_SelectedIndexChanged(object sender, EventArgs e)
        {
          


            vengoDeComboCliente = true;
            //lCargar.Text = "";
            pbPagos.Increment(-100);
            lCargar.ForeColor = Color.Red;
            lCargar.Text = "............";
            lCargar.Refresh();
            //tBarraProgreso.Start();

            if (cbComprador.SelectedIndex != -1)
            {
                limpiar();

                string IdComprador = "";

                try
                {
                    pbPagos.Increment(10);
                    pbPagos.PerformStep();
                    ws.Timeout = 600000;
                    //obtengo el id del comprado 
                    IdComprador = ws.getIdComprador(cbComprador.SelectedItem.ToString());
                    pbPagos.Increment(10);
                    //pbPagos.PerformStep();
                }
                catch (Exception) { }

                txtId.Text = IdComprador;
                //totalInteres = 0;
                llenarComboLote();

            }

            

        }

        private void Loadgv(string idVentatoquery)
        {
            dataGridView1.DataSource = null;
            CP.queryFree("", $"SELECT id_pago_concepto,fecha_concepto_pago,cantidad_total,cantidad_pagar,Concepto,status FROM `ConceptoPago` WHERE `fk_id_compraventa` = {idVentatoquery} ORDER BY id_pago_concepto DESC");
            if (!CP.json.Equals("false"))
            {
                _ListresultQueryCPconceptos = CP.list<resultconceptospagos>();
                dataGridView1.DataSource = _ListresultQueryCPconceptos;
            }
        }

        private void cbLote_SelectedIndexChanged(object sender, EventArgs e)
        {
            limpiar();

            string respuestaResgistraMora = "";

            if (chkNoGeneraraInteres.Checked)
            {
                respuestaResgistraMora = "Reestructuracion";
            }

            int incremento = 0;

            if (cbComprador.SelectedItem.ToString() != "")
            {
                cbMesesMora.Items.Clear();

                if (vengoDeComboCliente)
                {
                    incremento = 10;
                }
                else
                {
                    pbPagos.Increment(-100);
                    lCargar.Text = "............";
                    lCargar.Refresh();
                    incremento = 60;
                }

                string allVenta = "";

                try
                {
                    pbPagos.Increment(incremento);
                    ws.Timeout = 600000;
                    allVenta = ws.getAllVenta(splitIdLote[cbLote.SelectedIndex]);
                    pbPagos.Increment(10);
                }
                catch (Exception) { }



                string[] splitAllVenta = allVenta.Split(new char[] { '|' });
                string[] splitDatosVenta = splitAllVenta[0].Split(new char[] { ',' });
                string[] splitDatosProximoPago = splitAllVenta[1].Split(new char[] { ',' });
                string[] splitDatosPredio = splitAllVenta[2].Split(new char[] { ',' });

                idVenta = splitDatosVenta[0];
                mensualidadDeVentas = splitDatosVenta[1];
                fechaCompra = splitDatosVenta[2];
                string fecha_corte = splitDatosVenta[3];

                _detallePago = new DetallePago(Convert.ToDateTime(fechaCompra), Convert.ToDecimal(mensualidadDeVentas), Convert.ToInt32(idVenta));

                mensualidadProximoPago = splitDatosProximoPago[0];
                proximoPago = splitDatosProximoPago[1];
                pagoActual = splitDatosProximoPago[2];
                string pagoFinal = splitDatosProximoPago[3];
                statusMora = splitDatosProximoPago[4];

                setProximoPago();

                string numeroManzana = splitDatosPredio[0];
                string nombrePredio = splitDatosPredio[1];
                string numeroLote = splitDatosPredio[2];
                administracion = splitDatosPredio[3];

                //bool _statusPagado = _detallePago.statusPagado(_detallePago._listDetallePago, Convert.ToDecimal(mensualidadDeVentas));

                ///// AQUI SE LLENAR EL FORMULARIO  //////
                txtDiaCorte.Text = fecha_corte;
                //txtAbono.Text = ws.getAbono(txtId.Text);
                txtPagoActual.Text = _detallePago._numeroProximoPago.ToString();//pagoActual;
                pagoActualRecibo = _detallePago._numeroProximoPago.ToString();
                pagoActual = txtPagoActual.Text;
                txtPagoFinal.Text = pagoFinal;
                txtMensualidadMesActual.Text = string.Format("{0:N2}", _detallePago._mensualidadMenosAbono);// _detallePago.calcularMensualidad(Convert.ToDecimal(mensualidadDeVentas))); //string.Format("{0:N2}", Convert.ToDouble(mensualidadProximoPago));
                txtProximoPago.Text = Convert.ToDateTime(_detallePago.fechaProximoPago).ToString("MMMM - yyyy");//Convert.ToDateTime(proximoPago).ToString("MMMM - yyyy");
                proximoPago = _detallePago.fechaProximoPago;
                txtPredio.Text = nombrePredio;
                txtManzana.Text = numeroManzana;


                /////// SE REALIZA EL CALCULO DE LOS MESES MORATORIOS  /////////

                if (!chkNoGeneraraInteres.Checked)
                {
                    //respuestaResgistraMora = f.calculaMesesMorosos(f.estaEnMora(Convert.ToDateTime(proximoPago)),
                    //                                                      f.statusMora(statusMora),
                    //                                                      Convert.ToDateTime(proximoPago),
                    //                                                      idVenta,
                    //                                                      mensualidadProximoPago, actualizo, mensualidadDeVentas/*, lCargar*/);

                    //txtInteresMesActual.Text = string.Format("{0:N2}", f.calcularMora(Convert.ToDateTime(proximoPago), Convert.ToDouble(mensualidadProximoPago)));

                    pbPagos.Increment(10);
                    lCargar.ForeColor = Color.Blue;
                    lCargar.Text = "ESPERANDO RESPUESTA";
                    //MessageBox.Show(respuestaResgistraMora);
                    lCargar.ForeColor = Color.Red;
                    lCargar.Text = "............";
                    lCargar.Refresh();
                    pbPagos.Increment(10);
                }
                else
                {
                    pbPagos.Increment(10);
                    lCargar.ForeColor = Color.Blue;
                    lCargar.Text = "ESPERANDO RESPUESTA";
                    MessageBox.Show(respuestaResgistraMora);
                    lCargar.ForeColor = Color.Red;
                    lCargar.Text = "............";
                    lCargar.Refresh();
                    pbPagos.Increment(10);
                }


                if (statusMora != "1" && f.estaEnMora(Convert.ToDateTime(proximoPago)) == "mora")
                {
                    statusMora = "1";
                }

                ////// LLENAR DATOS MORA /////////
                pbPagos.Increment(10);

                cargarTabla("`idVenta` = " + idVenta + /*" AND  `comprador` =  '" + cbComprador.Text + "'*/ " ORDER BY `DetallePago`.`idDetallePago` ASC");

                llenarDatosMora();

                //obtenerIdMora();

                lCargar.ForeColor = Color.Green;
                lCargar.Text = "LISTO !!";
                //tBarraProgreso.Stop();
                pbPagos.Increment(10);

                if (bandera == false)
                {
                    Loadgv(idVenta);
                }
            }


        }

        public void llenarComboComprador()
        {

            string idComprador = "";
            string respuestaNombreComprador = "";

            try
            {
                idComprador = ws.getIdCompradordeVenta();
                respuestaNombreComprador = ws.getNombreComprador(idComprador);
            }
            catch (Exception) { }

            string[] splitComprador = respuestaNombreComprador.Split(new char[] { ',' });

            foreach (string comprador in splitComprador)
            {
                cbComprador.Items.Add(comprador);
            }
        }

        public void llenarDatosMora()
        {

            //bool entra =true;
            //string respuestaFechaMora = "";
            //string respuestaMontoMora = "";

            //try
            //{
            //    respuestaFechaMora = ws.getFechaMora(idVenta);
            //    respuestaMontoMora = ws.getMontoMora(idVenta);
            //}
            //catch (Exception) { }

            //int distanciaMes = 0;
            //int indexMes = 0;
            //double totalInteres = 0;
            //double totalMensualidad = 0;
            //double totalTotal = 0;
            //txtTotalInteres.Text = "";
            //txtInteresMesActual.Text = "";


            //splitFechaMora = respuestaFechaMora.Split(new char[] { ',' });
            //splitMontoMora = respuestaMontoMora.Split(new char[] { ',' });


            //if (splitFechaMora[0] == ""){
            //    entra = false;
            //}

            //if (splitFechaMora[0] != "")
            //{
            //foreach (string MontoMora in splitMontoMora)
            //{
            //    totalInteres = totalInteres + Convert.ToDouble(MontoMora);
            //}
            //totalInteres = obtenerTotalInteres();
            //totalMensualidad = obtenerTotalMensual(Convert.ToDateTime(proximoPago));
            //totalTotal = totalInteres + totalMensualidad;

            //txtTotalInteres.Text = string.Format("{0:N2}", totalInteres);
            //txtTotalMensualidad.Text = string.Format("{0:N2}", totalMensualidad);
            //txtTotalTotal.Text = string.Format("{0:N2}", totalTotal);
            //int contador = 0;

            //contador = (DateTime.Today.Year * 12 + DateTime.Today.Month) -
            //           (Convert.ToDateTime(proximoPago).Year * 12 + Convert.ToDateTime(proximoPago).Month);

            //DateTime AuxiliarProximoPago = Convert.ToDateTime(proximoPago).AddMonths(contador);

            //if (AuxiliarProximoPago >= DateTime.Today)
            //{
            //    contador -= 1;
            //}

            //distanciaMes = f.distanciaMesVerificandoMes(Convert.ToDateTime(proximoPago), DateTime.Today);

            //if(f.mesActual(Convert.ToDateTime(proximoPago)).Month != DateTime.Today.Month)
            //{
            //    distanciaMes -= 1;
            //}
            if (_detallePago._mesesVencidos > 0)
            {
                cbMesesMora.Items.Clear();

                for (int i = 0; i < (_detallePago._mesesVencidos - 1); i++)
                {
                    cbMesesMora.Items.Add(Convert.ToDateTime(_detallePago.fechaProximoPago).AddMonths(i + 1).ToString("MMMM - yyyy"));
                }
            }
            //obtenerCantidadMeses(); // lleno la variable arrayMeses que contiene la cantidad de veces que un mes entro en mora

            //if (cbMesesMora.Items.Count >= 1)
            //{
            //    cbMesesMora.SelectedIndex = 0;
            //}
            //else
            //{
            //    cbMesesMora.SelectedIndex = -1;
            //}

            //indexMes = cbMesesMora.FindString(Convert.ToDateTime(proximoPago).ToString("MMMM"));

            //if (indexMes != -1)
            //{
            //txtInteresMesActual.Text = string.Format("{0:N2}", obtenerInteresMensual(indexMes));
            //txtInteresMesActual.Text = string.Format("{0:N2}", f.calcularMora(Convert.ToDateTime(proximoPago), Convert.ToDouble(mensualidadProximoPago)));
            if (!chkNoGeneraraInteres.Checked)
            {
                txtInteresMesActual.Text = interesMesActual();
            }
            else
            {
                txtInteresMesActual.Text = "0.00";
            }

            txtTotalPagar.Text = string.Format("{0:N2}", (Convert.ToDouble(txtMensualidadMesActual.Text) +
                                                        Convert.ToDouble(txtInteresMesActual.Text)));
            //if (txtInteresMesActual.Text == "0.00")
            //{
            //    txtInteresMesActual.Text = "";
            //}

            txtInteresMesActual.Refresh();
            txtTotalPagar.Refresh();
            //}
            //}
            //else
            //{
            //    txtTotalPagar.Text = txtMensualidadMesActual.Text;
            //    //cbMesesMora.Items.Clear();
            //    //cbMesesMora.Items.Add("");
            //    txtTotalMensualidad.Text = "";
            //    txtTotalTotal.Text = "";
            //    cbMesesMora.SelectedIndex = -1;
            //    cbMesesMora.ResetText();
            //    //txtTotalMesSeleccionado.Text = "Interes mensual";
            //}
        }

        public void llenarComboLote()
        {
            cbLote.Items.Clear();
            string respuestaIdLoteDeVenta = "";
            int tamañoSplit = 0;                  //obtengo el tamaño del arreglo
            string[] numeroLotes;

            pbPagos.Increment(10);
            //tengo que obtener la lista de id de terrenos
            respuestaIdLoteDeVenta = ws.getIdLoteDeVenta(txtId.Text);//obtengo la lista de id de lotes
            pbPagos.Increment(10);

            if (respuestaIdLoteDeVenta != null)
            {
                splitIdLote = respuestaIdLoteDeVenta.Split(new char[] { ',' });//genero un arreglo con esa lista de id de lotes
                tamañoSplit = splitIdLote.Length;                          //obtengo el tamaño del arreglo
                numeroLotes = new string[splitIdLote.Length];         //creo un arreglo de numeroLotes el tamaño del 
                                                                      //arreglo es igual
                                                                      //al tamaño de id lotes.
                pbPagos.Increment(5);
                for (int i = 0; i < splitIdLote.Length; i++)
                {
                    numeroLotes[i] = ws.getNumeroLote(splitIdLote[i]);//lleno el arreglo con los numeros de lotes
                }
                pbPagos.Increment(5);

                try
                {
                    foreach (string idLotes in numeroLotes)
                    {
                        cbLote.Items.Add(idLotes);//lleno el combobox de Lotes con el numero de lotes
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("No se ingreso ningun cliente");
                }
            }

            if (tamañoSplit > 1 && actualizo != 1)
            {
                lCargar.ForeColor = Color.Blue;
                lCargar.Text = "ESPERANDO RESPUESTA";
                MessageBox.Show("Usuario con mas de una compra");
                lCargar.ForeColor = Color.Red;
                lCargar.Text = "..........";
                lCargar.Refresh();
            }

            try
            {
                if (actualizo != 1)
                {
                    cbLote.SelectedIndex = 0;
                }
                else
                {
                    cbLote.SelectedIndex = indexComboLote;
                }
            }
            catch (Exception)
            {


            }

        }

        private void cbMesesMora_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
           * EVENTO: CUANDO EL INDEX DEL COMBOBOX CAMBIE
           * ACCION: Calcula el interes selecionado el interes desde hasta y el interes total
           */
            try
            {
                if (cbMesesMora.SelectedItem.ToString() != "")
                {
                    if (cbMesesMora.SelectedIndex >= 0)
                    {
                        int mesesVencidos = _detallePago._mesesVencidos - (cbMesesMora.SelectedIndex + 1);
                        decimal interesMesSeleccionado = interes.mensual(Convert.ToDecimal(mensualidadDeVentas), 0.06M, mesesVencidos); //obtenerInteresMensual(Convert.ToDateTime(proximoPago), Convert.ToDouble(mensualidadDeVentas));
                        //decimal mensualidadMesSeleccionado = Convert.ToDouble(mensualidadDeVentas);
                        decimal totalMesSeleccionado = interesMesSeleccionado + Convert.ToDecimal(mensualidadDeVentas);

                        gbMensual.Text = cbMesesMora.Text;

                        txtInteresMesSeleccionado.Text = $"{interesMesSeleccionado:N2}";//string.Format("{0:N2}", interesMesSeleccionado);
                        txtMensualidadMesSeleccionado.Text = $"{mensualidadDeVentas:N2}";//string.Format("{0:N2}", mensualidadMesSeleccionado);
                        txtTotalMesSeleccionado.Text = $"{totalMesSeleccionado:N2}";// string.Format("{0:N2}", totalMesSeleccionado);

                        if (cbMesesMora.SelectedIndex >= 0)
                        {
                            obtenerDesdeHasta();
                        }
                    }
                    else
                    {
                        txtInteresMesSeleccionado.Text = "";
                        txtMensualidadMesSeleccionado.Text = "";
                        txtTotalMesSeleccionado.Text = "";
                        txtDesdeHastaInteres.Text = "";
                        txtDesdeHastaMensualidad.Text = "";
                        txtDesdeHastaTotal.Text = "";
                        gbDesdeHasta.Text = "Desde / Hasta";
                        gbMensual.Text = "Mensual";
                    }
                }
            }
            catch (NullReferenceException)
            {
                txtInteresMesSeleccionado.Text = "";
                txtMensualidadMesSeleccionado.Text = "";
                txtTotalMesSeleccionado.Text = "";
                txtDesdeHastaInteres.Text = "";
                txtDesdeHastaMensualidad.Text = "";
                txtDesdeHastaTotal.Text = "";
                gbDesdeHasta.Text = "Desde / Hasta";
                gbMensual.Text = "Mensual";
            }
        }

        //obtengo el interes y la mensualidad desde un parametro inicial y final
        private void obtenerDesdeHasta()
        {
            int indexCbMesesMora = cbMesesMora.SelectedIndex + 2;
            decimal interesMesual = interes.mensual(Convert.ToDecimal(mensualidadDeVentas), 0.06M, 1);
            decimal interesPrimerMes = interes.mensual(Convert.ToDecimal(mensualidadDeVentas), 0.06M, cbMesesMora.Items.Count);
            decimal interesDesdeHasta = 0.0M;
            decimal mensualidadDesdeHasta = 0;// Convert.ToDecimal(this.mensualidadProximoPago);
            decimal totalDesdeHasta = 0.0M;
            string mesDesde = Convert.ToDateTime(this.proximoPago).ToString("MMMM");
            string mesHasta = Convert.ToDateTime(this.proximoPago).AddMonths(cbMesesMora.SelectedIndex + 1).ToString("MMMM");
            int totalMesesEnMora = cbMesesMora.Items.Count;
            //int morasAcumuladas = 1;

            gbDesdeHasta.Text = mesDesde + " / " + mesHasta;

            for (int i = 1; i <= indexCbMesesMora - 1  ; i++)
            {
                //if (indexCbMesesMora > 1)
                //{
                //    morasAcumuladas = cbMesesMora.Items.Count - indexCbMesesMora;
                //}

                interesDesdeHasta += interesMesual * totalMesesEnMora;//interesPrimerMes - interesMesual;//Convert.ToDouble(obtenerInteresMensual(i));

                totalMesesEnMora--;
                //if (i < indexCbMesesMora)
                //{
                //    mensualidadDesdeHasta += Convert.ToDecimal(mensualidadDeVentas);
                //}
            }

            interesDesdeHasta += Convert.ToDecimal(txtInteresMesActual.Text);

            mensualidadDesdeHasta = Convert.ToDecimal(mensualidadDeVentas) * (indexCbMesesMora - 1);

            mensualidadDesdeHasta += Convert.ToDecimal(txtMensualidadMesActual.Text);

            totalDesdeHasta = interesDesdeHasta + mensualidadDesdeHasta;

            txtDesdeHastaInteres.Text = string.Format("{0:N2}", interesDesdeHasta);
            txtDesdeHastaMensualidad.Text = string.Format("{0:N2}", mensualidadDesdeHasta);
            txtDesdeHastaTotal.Text = string.Format("{0:N2}", totalDesdeHasta);
        }

        //obtengo el total de los intereses acumulados de todos los meses
        private double obtenerTotalInteres(DateTime proximoPago) 
        {
            double deudaMoraTotal = 0;
            int distancia = f.distanciaMesVerificandoMes(proximoPago, DateTime.Today);


            for (int i = 0; i <= distancia; i++) 
            {
                deudaMoraTotal += f.calcularInteres(proximoPago.AddMonths(i), Convert.ToDouble(mensualidadDeVentas)); 
            }
                //if (splitMontoMora[0] != "")
                //{
                //    for (int i = 0; i < splitMontoMora.Length; i++)
                //    {
                //        deudaMoraTotal += Convert.ToDouble(splitMontoMora[i]);
                //    }
                //}
                return deudaMoraTotal;
        }

        //obtengo el total de mensualidad de todos los meses
        private double obtenerTotalMensual(DateTime axuliarProximoPago)
        {
            int distanciaMes = 0;
            double deudaMensualTotal = Convert.ToDouble(mensualidadProximoPago);

            distanciaMes = f.distanciaMesVerificandoMes(Convert.ToDateTime(axuliarProximoPago), DateTime.Today);

            if (distanciaMes > 0 )
            {
                deudaMensualTotal += Convert.ToDouble(mensualidadDeVentas) * distanciaMes;
            }

            return deudaMensualTotal;
        }

        //obtengo el total de adeudo mensualidad mas intereses
        private double obtenerTotalAdeudo(DateTime auxiliarProximoPago) 
        {
            double totalAdeudo = 0;

            if (chkNoGeneraraInteres.Checked) 
            {
                totalAdeudo +=  obtenerTotalMensual(auxiliarProximoPago);
            }
            else 
            {
                totalAdeudo = obtenerTotalInteres(auxiliarProximoPago) + obtenerTotalMensual(auxiliarProximoPago);
            }

            //totalAdeudo = obtenerTotalInteres(auxiliarProximoPago) + obtenerTotalMensual(auxiliarProximoPago);

            return totalAdeudo;
        }

        //obtengo el adeudo pendiente si existiera despues de realizar un pago
        private double obtenerRestoDeuda(double montoPagado, DateTime auxiliarProximoPago) 
        {
            double totalAdeudo = obtenerTotalAdeudo(auxiliarProximoPago);
            double restoAdeudo = 0;

            restoAdeudo = totalAdeudo - montoPagado;

            if (restoAdeudo < 0) 
            {
                restoAdeudo = 0;
            }

            return restoAdeudo;
        }

        private void cmdPagoMensualidad_Click(object sender, EventArgs e)
        {
            cmdPagoMensualidad.Enabled = false;
            // MEDIDAS DEL LOTE NORTE Y ESTE 
            //string respuestaMedidaLote = ws.getMedidaLote(splitIdLote[cbLote.SelectedIndex]);
            //string[] splitMedidaLote = respuestaMedidaLote.Split(new char[] { ',' });
            //string norte = splitMedidaLote[0];
            //string este = splitMedidaLote[2];
            // FN

            // COLONIA Y MUNICIPIO DEL PREDIO  
            //string colonia = ws.getDatoPredio("colonia", "nombre_predio", txtPredio.Text);
            //string municipio = ws.getDatoPredio("municipio", "nombre_predio", txtPredio.Text);
            // FIN
            if (txtMonto.Text != "")
            {
                string nombreClienteExiste = cbComprador.Text;

                if (cbComprador.FindStringExact(nombreClienteExiste) >= 0)
                {
                    String mensaje = "¿Confirma realizar el pago?";
                    String caption = "Pago Mensualidad";
                    MessageBoxButtons botones = MessageBoxButtons.OKCancel;
                    DialogResult respuesta;

                    respuesta = MessageBox.Show(mensaje, caption, botones);

                    if (respuesta == System.Windows.Forms.DialogResult.OK)
                    {
                        pagar();

                        cmdPagoMensualidad.Enabled = true;
                    }
                    else
                    {
                        cmdPagoMensualidad.Enabled = true;
                    }
                }
                else
                {
                    MessageBox.Show("verifique el nombre del comprador","Alerta",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                    cmdPagoMensualidad.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Favor agregar el monto a pagar");
                cmdPagoMensualidad.Enabled = true;
            }
        }

        private double obtenerInteresMensual(string proximoPago, string mensualidad) 
        {

            double interesMensual = f.calcularInteres(Convert.ToDateTime(proximoPago),Convert.ToDouble( mensualidad));
            //int indexMontoMora = 0;

            //if (indexMes >= 0 && splitMontoMora[0] != "")
            //{
            //    for (int i = 0; i < indexMes; i++)
            //    {
            //        indexMontoMora += arrayMeses[i];
            //    }

            //    for (int i = 0; i < arrayMeses[indexMes]; i++)
            //    {
            //        try
            //        {
            //            interesMensual = interesMensual + Convert.ToDouble(splitMontoMora[indexMontoMora + i]);
            //        }
            //        catch (Exception){ };
            //    }
            //}
            return interesMensual;
        }

        //private void obtenerCantidadMeses() //me dice la cantidad de veces que aparece un mes en mora
        //{
        //    int distanciaMes = f.distanciaMes(Convert.ToDateTime(proximoPago), DateTime.Today);

        //    int tamaño = splitFechaMora.Length;
        //    int contador = 0;
        //    arrayMeses = new int[distanciaMes + 1];

        //    for (int i = 0; i <= distanciaMes; i++)
        //    {
        //        contador = 0;

        //        for (int j = 0; j < tamaño; j++)
        //        {
        //            if (Convert.ToDateTime(splitFechaMora[j]).Month == Convert.ToDateTime(proximoPago).AddMonths(i).Month)
        //            {
        //                contador++;
        //            }
        //            arrayMeses[i] = contador;
        //        }
        //    }
        //}

        private String[] pagoMora()
        {
            int indexMes = cbMesesMora.FindString(Convert.ToDateTime(proximoPago).ToString("MMMM"));
            int indexIdMora = 0;
            string[] idMoras = new String[arrayMeses[indexMes]];

            for (int i = 0; i < indexMes; i++)
            {
                indexIdMora += arrayMeses[i];
            }

            for (int i = 0; i < arrayMeses[indexMes]; i++)
            {
                idMoras[i] = splitIdMora[indexIdMora + i];
            }

            return idMoras;

            //String cadenaImplode = implode(splitIdMora);
            //string respuestaUpdateMora = ws.updateTablaMora(cadenaImplode, "1", "status");
        }

        //private int abonoPagoMora(Double resto, out String montoActualizar , out String[] auxiliarIdMora, out String idActualiza, out int bandera)
        //{
        //    montoActualizar = "";
        //    idActualiza = "";
        //    int indexMes = cbMesesMora.FindString(Convert.ToDateTime(proximoPago).ToString("MMMM"));
        //    int tamaño = Convert.ToInt32(resto) / Convert.ToInt32(Convert.ToInt32( mensualidadDeVentas)*0.06);
        //    string[] idMoras = new string [tamaño];
        //    auxiliarIdMora = idMoras;
        //    int indexIdMora = 0;
        //    bandera = 1;
        //    int siActualizo = 0;

        //    for (int i = 0; i < indexMes; i++)
        //    {
        //        indexIdMora += arrayMeses[i];
        //    }

        //    if (tamaño != 0)
        //    {
        //        for (int i = 0; i <=tamaño; i++)
        //        {
        //            double montoMora = Convert.ToDouble(splitMontoMora[indexIdMora + i]);

        //            if (resto != 0 && resto > montoMora || resto == montoMora)
        //            {
        //                idMoras[i] = splitIdMora[indexIdMora + i];
        //                resto -= Convert.ToDouble(splitMontoMora[i]);
        //            }
        //            else if (resto != 0)
        //            {
        //                idActualiza = splitIdMora[indexIdMora + i];
        //                montoActualizar = Convert.ToString(Convert.ToDouble(splitMontoMora[indexIdMora + i]) - resto);
        //                resto -= resto;
        //                siActualizo = 1;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        idActualiza = splitIdMora[indexIdMora];

        //        if (resto < Convert.ToInt32(splitMontoMora[indexIdMora]))
        //        {
        //            montoActualizar = Convert.ToString(Convert.ToDouble(splitMontoMora[indexIdMora]) - resto);
        //        }
        //        else
        //        {
        //            montoActualizar = Convert.ToString(resto - Convert.ToDouble(splitMontoMora[indexIdMora]));
        //        }

        //        bandera = 0;
        //        siActualizo = 1;
        //    }
        //    auxiliarIdMora = idMoras;

        //    return siActualizo;
        //}


        //public void actualizarMora(string idMesEnMora)
        //{
        //    ws.updateTablaMora(idMesActualizar,Convert.ToString(diferencia),"monto_mora");
        //}


        //private void abonoMensualidad(string colonia, string municipio, string norte, string este) {

        //    //obtengo la diferencia del abono con la menusualidad y obtengo la nueva mensualidad para updateProximoPago
        //    string nuevaMensualidad = Convert.ToString(Convert.ToInt32(mensualidadProximoPago) - 
        //                                               Convert.ToInt32(txtMensualidad.Text));
        //    //registro el pago como abono 
        //    ws.registraPago(idVenta, 
        //                    txtMensualidad.Text, 
        //                    Convert.ToString(DateTime.Today), 
        //                    proximoPago, 
        //                    "abono", 
        //                    pagoActual);

        //    //actualizo proximoPago con la nueva mensualidad
        //    ws.updateTablaProximoPago(idVenta, nuevaMensualidad);

        //    //registro el abono mensual del cliente
        //    ws.updateAbonoMensual(txtId.Text, txtMensualidad.Text);

        //    string interesMesActual = obtenerInteresMensual(cbMesesMora.FindString(Convert.ToDateTime(proximoPago).ToString("MMMM")));

        //    //genero el recibo
        //    //recibo.crearPdfRecibo("abono", 
        //    //                       pagoActual, 
        //    //                       txtPagoFinal.Text, 
        //    //                       cbLote.Text, 
        //    //                       txtManzana.Text, 
        //    //                       cbComprador.Text, 
        //    //                       txtMensualidad.Text, 
        //    //                       Convert.ToDateTime(proximoPago).ToString("MMMM").ToUpper(), 
        //    //                       txtPredio.Text, 
        //    //                       colonia, 
        //    //                       municipio, 
        //    //                       norte, 
        //    //                       este,
        //    //                       nuevaMensualidad,
        //    //                       interesMesActual
        //    //                       /*false*/);
        //}

        //private void pagoMensualidad() {

        //    //bool deudaDeMora = false;

        //    ////**la variable sumaMesMora es global y viene del metodo esMayorAQuinientos**
        //    //if (sumaMesMora > 0 && sumaMesMora < 500) //verifico que la deuda del mes sea menor 
        //    //{                                         //a 500 para imprimirla en el recibo 
        //    //    deudaDeMora = true;
        //    //}

        //    // REGISTRO DEL PAGO  
        //    string registraPago = ws.registraPago(idVenta,                  //id de la venta
        //                                          txtMensualidad.Text,      //mensualidad
        //                                          DateTime.Today.ToString(),//fecha de pago
        //                                          proximoPago,              //fecha de corte pagada
        //                                          "Mensualidad",            //concepto del pago
        //                                          pagoActual);             //numero de pago
        //    MessageBox.Show(registraPago);
        //    // FIN

        //    // CALCULO DE LA PROXIMA FECHA DE PAGO  
        //    DateTime proximaFechaDePago = f.setProximoPago(Convert.ToDateTime(fechaCompra),                 // fecha de compra          
        //                                                   Convert.ToString((Convert.ToInt32(pagoActual))));// pago actual
        //    MessageBox.Show(proximaFechaDePago.ToString());
        //    // FIN 

        //    //  REGISTRO DE LOS DATOS DE LA TABLA FECHA DE PROXIMO PAGO 
        //    string respuetaUpdateProximoPago = ws.updateProximoPago(idVenta,                      //id de la venta
        //                                                            mensualidadDeVentas,          //mesualidad del mes
        //                                                            proximaFechaDePago.ToString(),//proxime fecha de pago
        //                                                            "0",                          //tipo de pago
        //                                                            Convert.ToString(Convert.ToInt32(pagoActual)+1),//pago actual
        //                                                            "0");                         //status de mora
        //    // FIN   

        //    //// GENERA EL RECIBO  
        //    //recibo.crearPdfRecibo("contado",                                                   //forma de pago
        //    //                       pagoActual,                                                //pago actual            
        //    //                       txtPagoFinal.Text,                                          //ultimo pago
        //    //                       cbLote.Text,                                                //numero de lote
        //    //                       txtManzana.Text,                                            //numero de manzana                                 
        //    //                       cbComprador.Text,                                           //nombre comprador
        //    //                       txtMensualidad.Text,                                        //mensualidad pagada
        //    //                       Convert.ToDateTime(proximoPago).ToString("MMMM").ToUpper(), //nombre de mes
        //    //                       txtPredio.Text,                                             //nombre del predio
        //    //                       colonia,                                                    //nombre de la colonia
        //    //                       municipio,                                                  //municipio
        //    //                       norte,                                                      //medida norte
        //    //                       este,                                                       //medida este
        //    //                       "",                                                         //resto mensualidad
        //    //                       txtIntereses.Text,                                          //deuda de intereses 
        //    //                       deudaDeMora);                                               //si hay mora mensual
        //    //// FIN
        //}


        private void pagar()
        {
            string[] auxiliarIdMora = { };
            string[] idMora = { };
            string mesProximoPago = "";
            string mesParaProximoPago = "";
            string idActualizaMora = "";
            string montoActualizaMora = "";
            string fechaActualizaMes = "";
            string montoActualizaMes = "";
            string cambiarMensualidad = "0";
            string auxiliarProximoPago = proximoPago;
            string montoPagoInteres = "";
            string montoPagoMensualidad = "";
            string mesesPagoInteres = "";
            string mesesPagoMensualidad = "";
            string estaEnMora = "";

            //********************************
            //variables para saber los montos abonados
            string montosAbonadosInteres = "";
            string montosAbonadosMesualidad = "";
            //***********************************

            double descuento = 0;
            decimal interesMensual = 0;
            decimal resto = Math.Round(Convert.ToDecimal(txtMonto.Text));
            //double mensualidadCompara = Math.Round(Convert.ToDouble(mensualidadProximoPago));
            //double deudaMensualTotal = 0D;
            //double deudaMoraTotal = 0D;
            double abonoExtraOficial = 0;
            double deudaTotal = obtenerTotalAdeudo(Convert.ToDateTime(proximoPago));
            decimal[] montosPagados = new decimal[4]; //0 interes, 1 abono interes, 2 pago mensualidad, 3 abono mensualidad


            int[] cuantasVecesEntro = { 0, 0, 0, 0 };//0 pago mora, 1 abono mora, 2 pago mensualidad, 3 abono mensualidad
            int tamañoIdMora = 0;
            int tamañoAuxiliarIdMora = 0;
            int indexMes = 0;
            int nuevoTamaño = 0;
            int entreEnMora = 0;
            int entroActualizaMora = 1;
            int llenarMontoPagoInteres = 0;
            int llenarMontoPagoMensualidad = 0;

            decimal _mensualidadMesActual = Convert.ToDecimal(txtMensualidadMesActual.Text);
            decimal _mensualidadCompraVenta = Convert.ToDecimal(mensualidadDeVentas);

            decimal mensualidadCompara = Math.Round(Convert.ToDecimal(_mensualidadMesActual));


            //if (!double.TryParse(txtDescuento.Text, out descuento)) 
            //{
            //    MessageBox.Show("formato no valido para descuento");
            //}

            //if (Convert.ToDouble(txtMonto.Text) > deudaTotal)
            //{
            //    String mensaje = "Tu monto es mayor a la deuda actual \nSi desea adelantar mensualidades presiona ' SI '"
            //                      + "\n Si deseas abonar la cantidad exedente presiona ' NO '";

            //    String caption = "Monto Exedente";
            //    MessageBoxButtons botones = MessageBoxButtons.YesNoCancel;
            //    DialogResult respuesta;

            //    respuesta = MessageBox.Show(mensaje, caption, botones);

            //    if (respuesta == System.Windows.Forms.DialogResult.No)
            //    {
            //        abonoExtraOficial = Convert.ToDouble(txtTotalPagar.Text) - deudaTotal;
            //        txtTotalPagar.Text = Convert.ToString(Convert.ToDouble(txtTotalPagar.Text) - abonoExtraOficial);
            //    }
            //}

            ///////////////aqui empieza el metodo de pagos el ciclo do while ***********************
            //do
            //{
            //    if (statusMora == "1")
            //    {
            //        //mesProximoPago = Convert.ToDateTime(proximoPago).ToString("MMMM");
            //        //indexMes = cbMesesMora.FindString(mesProximoPago);
            //        ////interesMensual = sumarInteresPagado(f.calcularInteres(Convert.ToDateTime(proximoPago), mensualidadCompara));
            //        if (!chkNoGeneraraInteres.Checked)
            //        {
            //            interesMensual = Convert.ToDecimal(this.interesMesActual());
            //        }

            //        //if (descuento > 0 && interesMensual != 0.0) 
            //        //{

            //        //    interesMensual -= descuento;
            //        //}

            //        if (resto >= interesMensual && interesMensual != 0.0M)
            //        {
            //            //Array.Clear(auxiliarIdMora, 0, auxiliarIdMora.Length); 

            //            if (llenarMontoPagoInteres == 1)
            //            {
            //                montoPagoInteres += ",";
            //                mesesPagoInteres += ",";
            //            }

            //            //auxiliarIdMora = pagoMora();
            //            resto -= interesMensual;

            //            //montoPagoInteres += Convert.ToString(interesMensual);

            //            montosPagados[0] += interesMensual;
            //            montoPagoInteres += Convert.ToString(interesMensual);
            //            mesesPagoInteres += proximoPago;

            //            ////tamañoIdMora = idMora.Length;
            //            //tamañoAuxiliarIdMora = auxiliarIdMora.Length;
            //            //nuevoTamaño += tamañoAuxiliarIdMora;

            //            //Array.Resize(ref idMora, nuevoTamaño);

            //            //for (int i = tamañoIdMora, j = 0; j < tamañoAuxiliarIdMora; i++, j++)
            //            //{
            //            //    idMora[i] = auxiliarIdMora[j];
            //            //}

            //            //tamañoIdMora = idMora.Length;
            //            cuantasVecesEntro[0]++;
            //            entreEnMora++;

            //            llenarMontoPagoInteres = 1;
            //        }
            //        else if (interesMensual != 0.0M)
            //        {
            //            int bandera = 1;

            //            //entroActualizaMora = abonoPagoMora(resto, out montoActualizaMora, out auxiliarIdMora, out idActualizaMora, out bandera);
            //            ////actualizarMora(idMesActualizar);

            //            montosPagados[1] = resto;

            //            montosAbonadosInteres = Convert.ToString(resto);

            //            resto -= resto;

            //            //if (bandera == 1)
            //            //{
            //            //    tamañoAuxiliarIdMora = auxiliarIdMora.Length;
            //            //    nuevoTamaño += tamañoAuxiliarIdMora;

            //            //    Array.Resize(ref idMora, nuevoTamaño);

            //            //    for (int i = tamañoIdMora, j = 0; j < tamañoAuxiliarIdMora; i++, j++)
            //            //    {
            //            //        idMora[i] = auxiliarIdMora[j];
            //            //    }

            //            //    tamañoIdMora = idMora.Length;
            //            //    //cuantasVecesEntro[1]++;
            //            //    //entreEnMora++;
            //            //}
            //            fechaActualizaMes = Convert.ToDateTime(proximoPago).ToString();
            //            cuantasVecesEntro[1]++;
            //            entreEnMora++;
            //        }
            //    }

            //    //if (cuantasVecesEntro[1] == 0)
            //    //{
            //    //    entroActualizaMora = 0;
            //    //}

            //    ////cambio la variable de mes si es que ya se habia actualizado///
            //    if (_mensualidadCompraVenta != _mensualidadMesActual && cuantasVecesEntro[2] > 0)
            //    {
            //        mensualidadCompara = Math.Round(Convert.ToDecimal(mensualidadDeVentas));
            //        cambiarMensualidad = "1"; ////esta variable se pasa al webservices como si entro
            //    }
            //    /////////fin cambio variable mes

            //    if (resto > mensualidadCompara || resto == mensualidadCompara)
            //    {
            //        //pagoMensualidad();

            //        if (llenarMontoPagoMensualidad == 1)
            //        {
            //            montoPagoMensualidad += ",";
            //            mesesPagoMensualidad += ",";
            //        }

            //        cuantasVecesEntro[2]++;
            //        montosPagados[2] += mensualidadCompara;

            //        montoPagoMensualidad += Convert.ToString(mensualidadCompara);
            //        mesesPagoMensualidad += proximoPago;

            //        resto = resto - Convert.ToDecimal(mensualidadCompara);//_mensualidadMesActual);//mensualidadProximoPago);
            //        proximoPago = Convert.ToDateTime(proximoPago).AddMonths(1).ToString();

            //        llenarMontoPagoMensualidad = 1;
            //    }
            //    else if (Math.Round(resto) != 0)
            //    {
            //        //abonoMensualidad(colonia, municipio , norte, este);
            //        cuantasVecesEntro[3]++;

            //        fechaActualizaMes = Convert.ToDateTime(proximoPago).ToString();

            //        montoActualizaMes = Convert.ToString(mensualidadCompara - resto);
            //        montosPagados[3] += resto;

            //        montosAbonadosMesualidad = Convert.ToString(resto);//

            //        resto -= resto;
            //    }

            //} while (resto != 0);/****fin del ciclo do while**********************/

            //if (cuantasVecesEntro[3] == 1 || cuantasVecesEntro[1] == 1)
            //{
            //    mesParaProximoPago = fechaActualizaMes;
            //}
            //else if (cuantasVecesEntro[2] > 0 && cuantasVecesEntro[3] == 0)
            //{
            //    mesParaProximoPago = Convert.ToDateTime(proximoPago).ToString();
            //}

            //if (f.estaEnMora(Convert.ToDateTime(proximoPago)) != "mora")
            //{
            //    estaEnMora = "0";
            //}
            //else
            //{
            //    estaEnMora = "1";
            //}

            //string[] fechas = new string[cuantasVecesEntro[2]];

            //for (int i = 0; i < fechas.Length; i++)
            //{
            //    fechas[i] = Convert.ToDateTime(auxiliarProximoPago).AddMonths(i).ToString();
            //}

            //string fechasMesPagado = implode(fechas);
            //string stringIdMora = implode(idMora);


            //MessageBox.Show(/*"|Mora ->|" + stringIdMora + " " + " 1 " + " status "+
            //                "|<- Mora| "+" |Venta ->| "+idVenta+" "+mensualidadDeVentas+" "+mensualidadProximoPago+" "+DateTime.Today.ToString()+" "+fechasMesPagado+
            //                " MENSUALIDAD "+" "+pagoActual+"| <-Venta| "*/" |Datos extra ->|"+" \n"+idActualizaMora+" "+montoActualizaMora+
            //                " "+fechaActualizaMes+" "+montoActualizaMes+" "+cambiarMensualidad+" "+estaEnMora+" "+statusMora);

            //try
            //{
            //    ws.Timeout = 6000000;
            //    string respuestaAllPago = ws.registraAllPago
            //        (
            //            stringIdMora, 
            //            "1", 
            //            "status",
            //            /*datos para registrar pago*/
            //            idVenta, 
            //            mensualidadDeVentas, 
            //            mensualidadProximoPago, 
            //            DateTime.Today.ToString(), 
            //            mesesPagoMensualidad, 
            //            "Mensualidad", 
            //            pagoActual, 
            //            txtMonto.Text,
            //            /*datos requisito para relizar pagos*/  
            //            idActualizaMora, 
            //            montoActualizaMora, 
            //            fechaActualizaMes, 
            //            montoActualizaMes, 
            //            cambiarMensualidad, 
            //            estaEnMora,
            //            statusMora,
            //            Convert.ToString(entroActualizaMora), 
            //            Convert.ToString(cuantasVecesEntro[3]),
            //            mesParaProximoPago, 
            //            Convert.ToString(entreEnMora), 
            //            Convert.ToString(cuantasVecesEntro[2]),
            //            mesesPagoInteres, 
            //            montoPagoInteres, 
            //            montoPagoMensualidad, 
            //            Convert.ToString(cuantasVecesEntro[0]), 
            //            montosAbonadosInteres, 
            //            montosAbonadosMesualidad
            //        );

                
            //}
            //catch (Exception) { }

            string pagoFinal = txtPagoFinal.Text;
            string idLote = splitIdLote[cbLote.SelectedIndex];
            double restoAdeudo = obtenerRestoDeuda(Convert.ToDouble(txtMonto.Text), Convert.ToDateTime(auxiliarProximoPago));
            string cuantasPagoMora = Convert.ToString(cuantasVecesEntro[0]);
            string cuantasAbonoMora = Convert.ToString(cuantasVecesEntro[1]);
            string cuantasPagoMes = Convert.ToString(cuantasVecesEntro[2]);
            string cuantasabonoMes = Convert.ToString(cuantasVecesEntro[3]);
            string montoPagoMora = "0";//Convert.ToString(montosPagados[0]);
            string montoAbonoMora = "0";//Convert.ToString(montosPagados[1]);
            string montoPagoMes = "0";//Convert.ToString(montosPagados[2]);
            string montoAbonoMes = "0";//Convert.ToString(montosPagados[3]);
            string comprador = cbComprador.Text;
            string respuestaInsertarActividad = "";
            decimal _monto = Convert.ToDecimal(txtMonto.Text);
            //decimal _mensualidadMesActual = Convert.ToDecimal(txtMensualidadMesActual.Text);
            //decimal _mensualidadCompraVenta = Convert.ToDecimal(mensualidadDeVentas);
            decimal _total = Convert.ToDecimal(txtTotalPagar.Text);
            decimal _interes = Convert.ToDecimal(txtInteresMesActual.Text);
            string idDetallePago = "0";
            //bool banderaUmentarNumeroPagoYfecha = false;

            _detallePago.fechaProximoPago = _detallePago.fechaProximoPagoMetodo
                   (
                       _detallePago._listAmortizacion,
                       _detallePago._numeroProximoPago
                   ).ToString();

            do
            {
                montoPagoMora = "0";//Convert.ToString(montosPagados[0]);
                montoAbonoMora = "0";//Convert.ToString(montosPagados[1]);
                montoPagoMes = "0";//Convert.ToString(montosPagados[2]);
                montoAbonoMes = "0";//Convert.ToString(montosPagados[3]);


                if (_interes != 0)
                {
                    if (_monto >= _interes)
                    {
                        montoPagoMora = _interes.ToString();
                        _monto -= _interes;
                        montosPagados[0] += _interes;
                        cuantasVecesEntro[0]++;
                        _interes = 0.0M;
                    }
                    else if(_monto > 0)
                    {
                        montoAbonoMora = _monto.ToString();

                        montosPagados[1] += _monto;
                        cuantasVecesEntro[1]++;

                        _monto = 0.0M;
                        _interes = 0.0M;

                    }
                }
                if (_monto >= _mensualidadMesActual)
                {
                    montoPagoMes = _mensualidadMesActual.ToString();
                    _monto -= _mensualidadMesActual;

                    montosPagados[2] += _mensualidadMesActual;
                    cuantasVecesEntro[2]++;

                    if (_mensualidadMesActual < _mensualidadCompraVenta)
                    {
                        _mensualidadMesActual = _mensualidadCompraVenta;
                    }

                    //incrementea el numero de pago y la fecha de pago
                    //if (banderaUmentarNumeroPagoYfecha)
                    //{
                    //    pagoActual = Convert.ToString(Convert.ToInt32(pagoActual) + 1);

                    //    _detallePago.fechaProximoPago = _detallePago.fechaProximoPagoMetodo(Convert.ToInt32(pagoActual)).ToString();
                    //}

                    if (!chkNoGeneraraInteres.Checked)
                    {
                        if (_detallePago._mesesVencidos > 0)
                        {
                            _detallePago._mesesVencidos--;

                            _interes = interes.mensual(_mensualidadMesActual, 0.06M, _detallePago._mesesVencidos);

                            //entro++;
                        }
                    }
                    //else
                    //{
                    //    entro++;
                    //}
                }
                else if(_monto > 0)
                {
                    montoAbonoMes = _monto.ToString();

                    montosPagados[3] += _monto;
                    cuantasVecesEntro[3]++;

                    _monto = 0.0M;
                }

                //_detallePago.fechaPago = DateTime.Now.ToString();


                if (chkNoGeneraraInteres.Checked)
                {
                    _interes = 0;
                }

                // _detallePago.insert();

                ws.Timeout = 6000000;
                string respuestaInsertarDetallePago = ws.insertarDetallePago
                    (
                        idVenta,
                        pagoActual,
                        pagoFinal,
                        idLote,
                        mensualidadProximoPago,
                        Convert.ToString(deudaTotal),
                        Convert.ToString(restoAdeudo),
                        comprador,
                        _detallePago.fechaProximoPago,//auxiliarProximoPago,
                        DateTime.Now.ToString(),
                        cuantasPagoMora,
                        cuantasAbonoMora,
                        cuantasPagoMes,
                        cuantasabonoMes,
                        montoPagoMora,//_detallePago.montoPagoMora,
                        montoAbonoMora,//_detallePago.montoAbonoMora,
                        montoPagoMes,//_detallePago.montoPagoMes,
                        montoAbonoMes,//_detallePago.montoAbonoMes,
                        administracion
                    );

                string[] splitRespuestaInsertarDetallePago = respuestaInsertarDetallePago.Split(new char[] { ',' });

                string respuestaDetallePago = splitRespuestaInsertarDetallePago[0];
                idDetallePago = splitRespuestaInsertarDetallePago[1];

                if (tsCmdReestructuracion.BackColor != Color.Green)
                {
                    respuestaInsertarActividad = ws.registroActividad(usuario, idDetallePago, "pago", DateTime.Now.ToString());
                }

                //if (Convert.ToDecimal(montoAbonoMora) > 0 || Convert.ToDecimal(montoAbonoMes) > 0)
                //{
                //    _monto = 0;
                //}

                //if (Convert.ToDecimal(montoPagoMes) > 0)
                //{
                //    _monto -= Convert.ToDecimal(mensualidadDeVentas);

                //    if (_mensualidadMesActual < _mensualidadCompraVenta)
                //    {
                //        _mensualidadMesActual = _mensualidadCompraVenta;
                //    }
                //}
                //_monto -= Convert.ToDecimal(mensualidadDeVentas);

                if (_monto > 0)
                {
                    pagoActual = Convert.ToString(Convert.ToInt32(pagoActual) + 1);

                    _detallePago.fechaProximoPago = _detallePago.fechaProximoPagoMetodo(Convert.ToInt32(pagoActual)).ToString();
                }
                //banderaUmentarNumeroPagoYfecha = true;

            }   while (_monto > 0);

            if (idDetallePago != "0")
            {
                MessageBox.Show("Pago Realizado correctamente");
            }

            string numeroLote = cbLote.Text;
            string numeroManzana = txtManzana.Text;
            string predio = txtPredio.Text;
            string interesMesActual = txtInteresMesActual.Text;


            //GENERA EL RECIBO  
            recibo.crearPdfRecibo(pagoActualRecibo, //varible globla asignada en el mentodo cbLote_SelectedIndexChanged            
                                  pagoFinal,
                                  numeroLote,
                                  numeroManzana,
                                  predio,
                                  comprador,
                                  mensualidadProximoPago,
                                  deudaTotal, //adeudo Total                                      
                                  restoAdeudo,
                                  auxiliarProximoPago,
                                  proximoPago,
                                  cuantasVecesEntro, //arreglo con la cantidad de veces que realice un pago y un abono 
                                  montosPagados,
                                  0,
                                  privilegios); //la cantidad de dinero que pague y que concepto
            // FIN

            //actualixacion de datos despues del pago
            //recibo.reporteDiario(comprador, montosPagados, numeroLote, numeroManzana, auxiliarProximoPago, predio);

            //limpiar();

            //int indexNombre = cbComprador.SelectedIndex;

            ////hago que el index cambie para volverlo a reasignar
            //cbComprador.SelectedIndex = -1;

            ////variable bandera usada en el metodo cbLote_SelectedIndexChanged para seber que estoy actualizando 
            //actualizo = 1;

            //indexComboLote = cbLote.SelectedIndex;

            ////reasigno el index para cargar
            //cbComprador.SelectedIndex = indexNombre;

            //actualizo = 0;
            ////fin de la actualizacion despues del pago
            reloadForm();
        }

        public void reloadForm()
        {
            limpiar();

            int indexNombre = cbComprador.SelectedIndex;

            //hago que el index cambie para volverlo a reasignar
            cbComprador.SelectedIndex = -1;

            //variable bandera usada en el metodo cbLote_SelectedIndexChanged para seber que estoy actualizando 
            actualizo = 1;

            indexComboLote = cbLote.SelectedIndex;

            //reasigno el index para cargar
            cbComprador.SelectedIndex = indexNombre;

            actualizo = 0;
            //fin de la actualizacion despues del pago
        }

        //private void obtenerIdMora()
        //{
        //    string respuestaIdMora = ws.getIdMora(idVenta);
        //    splitIdMora = respuestaIdMora.Split(new char[] { ',' });
        //}

        private bool vacio() { 
        
          if(txtId.Text.Length == 0 || cbComprador.Text.Length == 0)
          {
              return false;
          }
          else
          {
            return true;
          }
        }
        private void limpiarcp()
        {

            dgvPagos.Rows.Clear();
            
            txtPredio.Text = "";
            txttotal.Text = "";
            txtManzana.Text = "";
            cbConcepto.SelectedItem = null;
            cbLote.Text = ""; txtMonto.Text = "";
            tbactual.Text = "";
            cbComprador.SelectedItem = null;
            //anterior
            txtProximoPago.Text = "";
            txtPagoActual.Text = "";
            txtPagoFinal.Text = "";
            txtMensualidadMesActual.Text = "";
            //txtDescuento.Text = "";
            txtTotalPagar.Text = "";
            txtMonto.Text = "";
            cbMesesMora.SelectedIndex = -1;
            dataGridView1.DataSource=null;;
        }
        private void limpiar() 
        {


     
            txtProximoPago.Text = "";
            txtPagoActual.Text = "";
            txtPagoFinal.Text = "";
            txtMensualidadMesActual.Text = "";
            //txtDescuento.Text = "";
            txtTotalPagar.Text = "";
            txtMonto.Text = "";
            cbMesesMora.SelectedIndex = -1;
        }

        public string implode(string[] array) 
        {
            string implode= "";
            int tamaño = array.Length;

            for (int i = 0; i <= tamaño; i++)
            {
                if (i < tamaño)
                {
                    implode += array[i];
                }

                if (i < tamaño - 1)
                {
                    implode += ",";
                }
            }
            return implode;
        }

        private void lMonto_Click(object sender, EventArgs e)
        {
            txtMonto.Text = txtTotalPagar.Text;
        }

        private void cmdBuscarCliente_Click(object sender, EventArgs e)
        {
            FrmBuscarCliente bc = new FrmBuscarCliente();
            bc.recibir += new FrmBuscarCliente.pasar(ejecutar);
            bc.Show();
        }

        private void ejecutar(string nombre) 
        {
            int index = this.cbComprador.FindString(nombre);
            cbComprador.SelectedIndex = index;
        }

        private void FrmPago_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (privilegios == "3")
            {
                Application.Exit();
            }
        }

        public void cargarTabla(string consulta)
        {
            dgvPagos.Rows.Clear();

            if (_detallePago._listDetallePago != null)
            {
                foreach (DetallePago dp in _detallePago._listDetallePago.OrderBy(x => x.idDetallePago))
                {
                    double montoPagoMora = Convert.ToDouble(dp.montoPagoMora);
                    double montoAbonoMora = Convert.ToDouble(dp.montoAbonoMora);
                    double montoPagoMes = Convert.ToDouble(dp.montoPagoMes);
                    double montoAbonoMes = Convert.ToDouble(dp.montoAbonoMes);

                    double total = montoPagoMora + montoAbonoMora + montoPagoMes + montoAbonoMes;

                    string totalPagado = string.Format("{0:N2}", total);

                    //string numeroLote = splitDatosPago[5];
                    //string numeroManzana = splitDatosPago[6];
                    //string fechaProximoPago = Convert.ToDateTime(setProximoPago(splitDatosPago[7])).ToString("MMMM - yyyy");
                    //string fechaPago = Convert.ToDateTime(setProximoPago(splitDatosPago[8])).ToString("dd-MM-yyyy hh:mm tt");
                    //string nombrePredio = splitDatosPago[9];
                    //string idDetallePago = splitDatosPago[11];
                    //string pagoActual = splitDatosPago[12];



                    dgvPagos.Rows.Insert
                                        (
                                            0,
                                            "Eliminar",
                                            dp.idDetallePago,
                                            dp.pagoActual,
                                            dp.montoPagoMes,// montoPagoMes, 
                                            dp.montoAbonoMes,//string.Format("{0:N2}", montoAbonoMes), 
                                            dp.montoPagoMora,//string.Format("{0:N2}", montoPagoMora), 
                                            dp.montoAbonoMora,//string.Format("{0:N2}", montoAbonoMora), 
                                            totalPagado,
                                            Convert.ToDateTime(dp.fechaProximoPago).ToString("MMMM - yyyy"),// fechaProximoPago, 
                                            Convert.ToDateTime(dp.fechaPago).ToString("dd-MM-yyyy hh:mm tt"));// fechaPago);

              

                }

              

        }
            //string respuestaConsulta = "";

            //try
            //{
            //    respuestaConsulta = ws.getDetallePago(consulta);
            //}
            //catch (Exception) 
            //{
            //    MessageBox.Show("Los datos no se cargaron correctamente\n\nACTUALIZE DE NUEVO LA INFORMACION");
            //}

            //if (respuestaConsulta != "" && respuestaConsulta != "0")
            //{
            //    splitDatosDetallePago = respuestaConsulta.Split(new char[] { '|' });

            //    foreach (string datosInsertar in splitDatosDetallePago)
            //    {
            //        string[] splitDatosPago = datosInsertar.Split(new char[] { ',' });

            //        string comprador = splitDatosPago[0];

            //        //string[] sinSimbolo = splitDatosPago[1].Split(new char[] { '$' });
            //        //double montoPagoMora = Convert.ToDouble(sinSimbolo[1].Trim());
            //        double montoPagoMora = Convert.ToDouble(splitDatosPago[1].Trim());
            //        double montoAbonoMora = Convert.ToDouble(splitDatosPago[2].Trim());
            //        double montoPagoMes = Convert.ToDouble(splitDatosPago[3].Trim());
            //        double montoAbonoMes = Convert.ToDouble(splitDatosPago[4].Trim());

            //        double total = montoAbonoMora + montoAbonoMes + montoPagoMes + montoPagoMora;

            //        string totalPagado = string.Format("{0:N2}", total);

            //        string numeroLote = splitDatosPago[5];
            //        string numeroManzana = splitDatosPago[6];
            //        string fechaProximoPago = Convert.ToDateTime(setProximoPago(splitDatosPago[7])).ToString("MMMM - yyyy");
            //        string fechaPago = Convert.ToDateTime(setProximoPago(splitDatosPago[8])).ToString("dd-MM-yyyy hh:mm tt");
            //        string nombrePredio = splitDatosPago[9];
            //        string idDetallePago = splitDatosPago[11];
            //        string pagoActual = splitDatosPago[12];

            //        dgvPagos.Rows.Insert(0, "Eliminar", idDetallePago, pagoActual, string.Format("{0:N2}", montoPagoMes), string.Format("{0:N2}", montoAbonoMes), string.Format("{0:N2}", montoPagoMora), string.Format("{0:N2}", montoAbonoMora), totalPagado, fechaProximoPago, fechaPago);
            //    }
            //}
            //else 
            //{
            //    this.cargarTabla(consulta);
            //}
            //sumarTotalPagado();
        }

        private void dgvPagos_DoubleClick(object sender, EventArgs e)
        {
            string mensaje = "¿Reimprimir recibo?";
            string caption = "Reimpresion de recibo";
            MessageBoxButtons botones = MessageBoxButtons.OKCancel;
            DialogResult respuestaMessage;

            respuestaMessage = MessageBox.Show(mensaje, caption, botones);

            if (respuestaMessage == System.Windows.Forms.DialogResult.OK)
            {
                string id = dgvPagos.CurrentRow.Cells[1].Value.ToString();

                reimprimirRecibo(id);
            }    
        }

        private void dgvPagos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (privilegios != NivelAcceso.CAJERO.ToString())
            {
                if (dgvPagos.Columns[e.ColumnIndex].Name.Equals("rowCmdEliminar"))
                {
                    int idDetallePago = Convert.ToInt32(dgvPagos.CurrentRow.Cells[1].Value.ToString());
                    string consulta = "DELETE FROM `DetallePago` WHERE `idDetallePago` = "+idDetallePago+"";

                    consulataDetallePago(consulta, "1", "eliminar", "Eliminar");
                }
            }
            //else 
            //{
            //    MessageBox.Show("No tienes permiso de edicion");
            //}
        }

        

        private void reimprimirRecibo(string id)
        {
            string idDetallePago = id;
            DetallePago reimpresionDetallePago = _detallePago._listDetallePago.Find(x => x.idDetallePago == Convert.ToInt32(id));

            //int indexDetallePago = this.indexDetallePago(idDetallePago);

            if (reimpresionDetallePago != null)//indexDetallePago > 0)
            {
                //string[] splitDatosPago = splitDatosDetallePago[indexDetallePago].Split(new char[] { ',' });

                string comprador = cbComprador.Text; //splitDatosPago[0];

                //string[] sinSimbolo = splitDatosPago[1].Split(new char[] { '$' });
                //double montoPagoMora = Convert.ToDouble(sinSimbolo[1].Trim());
                decimal montoPagoMora = Convert.ToDecimal(reimpresionDetallePago.montoPagoMora);//splitDatosPago[1].Trim());
                decimal montoAbonoMora = Convert.ToDecimal(reimpresionDetallePago.montoAbonoMora);//splitDatosPago[2].Trim());
                decimal montoPagoMes = Convert.ToDecimal(reimpresionDetallePago.montoPagoMes);//splitDatosPago[3].Trim());
                decimal montoAbonoMes = Convert.ToDecimal(reimpresionDetallePago.montoAbonoMes);//splitDatosPago[4].Trim());

                int cuantasPagoMora = (montoPagoMora > 0) ? 1 : 0; //Convert.ToInt32(splitDatosPago[14]);
                int cuantasAbonoMora = (montoAbonoMora > 0) ? 1 : 0; //Convert.ToInt32(splitDatosPago[15]);
                int cuantasPagoMes = (montoPagoMes > 0) ? 1 : 0; //Convert.ToInt32(splitDatosPago[16]);
                int cuantasAbonoMes = (montoAbonoMes > 0) ? 1 : 0; //Convert.ToInt32(splitDatosPago[17]);

                double total = Convert.ToDouble(montoAbonoMora + montoAbonoMes + montoPagoMes + montoPagoMora);

                string totalPagado = string.Format("{0:N2}", total);

                string numeroLote = cbLote.Text;//splitDatosPago[5];
                string numeroManzana = txtManzana.Text;//splitDatosPago[6];
                string fechaProximoPago = reimpresionDetallePago.fechaProximoPago;//Convert.ToDateTime(splitDatosPago[7]).ToString("MMMM - yyyy");
                string fechaPago = reimpresionDetallePago.fechaPago; //Convert.ToDateTime(splitDatosPago[8]).ToString("dd-MM-yyyy hh:mm tt");
                string nombrePredio = txtPredio.Text;//splitDatosPago[9];
                //string idDetallePago = splitDatosPago[11];
                string pagoActual = reimpresionDetallePago.pagoActual;//splitDatosPago[12];
                double restoAdeudo = 0;//Convert.ToDouble(splitDatosPago[13]);

                decimal[] montosPagados = { montoPagoMora, montoAbonoMora, montoPagoMes, montoAbonoMes };
                int[] quePagueYCuntasVeces = { cuantasPagoMora, cuantasAbonoMora, cuantasPagoMes, cuantasAbonoMes };

                recibo.crearPdfRecibo(pagoActual,
                                      txtPagoFinal.Text,
                                      numeroLote,
                                      numeroManzana,
                                      nombrePredio,
                                      comprador,
                                      "0",
                                      total,
                                      restoAdeudo,
                                      fechaProximoPago,
                                      fechaPago,
                                      quePagueYCuntasVeces,
                                      montosPagados,
                                      1,
                                      privilegios);
            }
        }

        private void consulataDetallePago(string consulta, string tipo, string accion, string caption)
        {
            string respuestaWebService = "";
            string mensaje = "¿Confirmar " + accion + " registro?";
            MessageBoxButtons botones = MessageBoxButtons.OKCancel;
            DialogResult respuestaMessage;

            //DetallePago updateDetallePago = _detallePago._listDetallePago.Find(x => x.idDetallePago == Convert.ToInt32(idDetallePago));

            respuestaMessage = MessageBox.Show(mensaje, caption, botones);
 
            if (respuestaMessage == System.Windows.Forms.DialogResult.OK)
            {
                respuestaWebService = ws.consultaDetallePago(consulta, tipo);

                MessageBox.Show(respuestaWebService);

                reloadForm();//cargarTabla("`idVenta` = " + idVenta + /*" AND  `comprador` =  '" + cbComprador.Text +"'*/" ORDER BY `DetallePago`.`idDetallePago` ASC");
            }
        }

        private void updateDetallePago(int idDetallePago)//string consulta, string tipo, string accion, string caption)
        {
            string respuestaWebService = "";
            string mensaje = "¿Confirmar actualizar registro?";
            MessageBoxButtons botones = MessageBoxButtons.OKCancel;
            DialogResult respuestaMessage;

            DetallePago updateDetallePago = _detallePago._listDetallePago.Find(x => x.idDetallePago == Convert.ToInt32(idDetallePago));

            respuestaMessage = MessageBox.Show(mensaje, "Actualizar", botones);

            if (respuestaMessage == DialogResult.OK)
            {
                updateDetallePago.update($"WHERE id_detalle_pago = {idDetallePago}");
                //respuestaWebService = ws.consultaDetallePago(consulta, tipo);

                if (Mensaje.result_bool)
                {
                    Mensaje.responseMessage("", "", true);
                    //MessageBox.Show(Mensaje.responseMessage(Mensaje.);
                }
                //MessageBox.Show(respuestaWebService);

                reloadForm();//cargarTabla("`idVenta` = " + idVenta + /*" AND  `comprador` =  '" + cbComprador.Text +"'*/" ORDER BY `DetallePago`.`idDetallePago` ASC");
            }
        }

        private int indexDetallePago(string idDetallePago) 
        {
            int incremento = 0;

            foreach (string datos in splitDatosDetallePago)
            {
                string[] datosDetallePago = datos.Split(new char[] { ',' });

                string deDatosidDetallePago = datosDetallePago[11];

                if (deDatosidDetallePago == idDetallePago)
                {
                    return incremento;
                }
                incremento++;
            }
            return -1;
        }

        private void dgvPagos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (privilegios == NivelAcceso.SISTEMAS.ToString() || privilegios == NivelAcceso.SUPERVISOR.ToString() || privilegios == NivelAcceso.ADMINISTRADOR.ToString())
            {
                int idDetallePago = Convert.ToInt32(dgvPagos.CurrentRow.Cells[1].Value.ToString());
                string dato = dgvPagos[e.ColumnIndex, e.RowIndex].Value.ToString();
                string consulta = "UPDATE `DetallePago` SET ";
                int numeroPago = 0;
                double mensualidad = 0;
                double abonoMes = 0;
                double pagoInteres = 0;
                double abonoInteres = 0;
                string mesPagadoString = "";
                DateTime fecha;
                string fechaPago = "";
                bool entra = true;

                string seleccion = dgvPagos.Columns[e.ColumnIndex].Name;

                DetallePago updateDetallePago = _detallePago._listDetallePago.Find(x => x.idDetallePago == Convert.ToInt32(idDetallePago));

                //UPDATE `DetallePago` SET `idDetallePago`=[value-1],`idVenta`=[value-2],=[value-3],`pagoFinal`=[value-4],`idLote`=[value-5],`mensualidad`=[value-6],`deudaTotal`=[value-7],`restoAdeudo`=[value-8],`comprador`=[value-9],`fechaProximoPago`=[value-10],`fechaPago`=[value-11],`cuantasPagoMora`=[value-12],`cuantasAbonoMora`=[value-13],`cuantasPagoMes`=[value-14],`cuantasAbonoMes`=[value-15], WHERE 1
                

                switch (seleccion)
                {
                    case "rowIdDetallePago":
                        MessageBox.Show("Celda no editable");
                        entra = false;
                        break;
                    case "rowPagoActual":
                        if (int.TryParse(dato, out numeroPago))
                        {
                            updateDetallePago.pagoActual = numeroPago.ToString();//consulta += "`pagoActual` = '" + Convert.ToString(numeroPago) + "' WHERE `idDetallePago` = " + idDetallePago;
                        }
                        else
                        {
                            MessageBox.Show("formato no valido solo Numero");
                        }
                        break;
                    case "rowMontoPagoMes":
                        if (double.TryParse(dato, out mensualidad))
                        {
                            updateDetallePago.mensualidad = $"{mensualidad:N2}";//.ToString();//consulta += "`montoPagoMes` = '" + Convert.ToString(mensualidad) + "' WHERE `idDetallePago` = " + idDetallePago;
                        }
                        else
                        {
                            MessageBox.Show("formato no valido solo Numero");
                        }
                        break;
                    case "rowMotoAbonoMes":
                        if (double.TryParse(dato, out abonoMes))
                        {
                            updateDetallePago.montoAbonoMes = $"{abonoMes:N2}"; //.ToString();//consulta += "`montoAbonoMes` = '" + Convert.ToString(abonoMes) + "' WHERE `idDetallePago` = " + idDetallePago;
                        }
                        else
                        {
                            MessageBox.Show("formato no valido solo Numero");
                        }
                        break;
                    case "rowMontoPagoMora":
                        if (double.TryParse(dato, out pagoInteres))
                        {
                            updateDetallePago.montoPagoMora = $"{pagoInteres:N2}";//.ToString();//consulta += "`montoPagoMora` = '" + Convert.ToString(pagoInteres) + "' WHERE `idDetallePago` = " + idDetallePago;
                        }
                        else
                        {
                            MessageBox.Show("formato no valido solo Numero");
                        }
                        break;
                    case "rowMontoAbonoMora":
                        if (double.TryParse(dato, out abonoInteres))
                        {
                            updateDetallePago.montoAbonoMora = $"{abonoInteres:N2}";//.ToString();//consulta += "`montoAbonoMora` = '" + Convert.ToString(abonoInteres) + "' WHERE `idDetallePago` = " + idDetallePago;
                        }
                        else
                        {
                            MessageBox.Show("formato no valido solo Numero");
                        }
                        break;
                    case "rowTotalPagado":
                        MessageBox.Show("Celda no editable");
                        entra = false;
                        break;
                    case "rowMesPagado":
                        mesPagadoString = txtDiaCorte.Text + " - " + dgvPagos.CurrentRow.Cells["rowMesPagado"].Value.ToString();

                        if (DateTime.TryParse(mesPagadoString, out fecha))
                        {
                            updateDetallePago.fechaProximoPago = fecha.ToString();//consulta += "`fechaProximoPago` = '" + Convert.ToString(mesPagadoDate) + "' WHERE `idDetallePago` = " + idDetallePago;
                        }
                        else
                        {
                            MessageBox.Show("formato no valido para la fecha");
                        }
                        break;
                    case "rowFechaPago":
                        fechaPago = dgvPagos.CurrentRow.Cells["rowFechaPago"].Value.ToString();

                        if (DateTime.TryParse(fechaPago, out fecha))
                        {
                            updateDetallePago.fechaPago = fecha.ToString();//consulta += "`fechaProximoPago` = '" + Convert.ToString(mesPagadoDate) + "' WHERE `idDetallePago` = " + idDetallePago;
                        }
                        else
                        {
                            MessageBox.Show("formato no valido para la fecha");
                        }
                        break;
                }

                if (entra)
                {
                    //string respuestaWebService = "";
                    string mensaje = "¿Confirmar actualizar registro?";
                    MessageBoxButtons botones = MessageBoxButtons.OKCancel;
                    DialogResult respuestaMessage;

                    //DetallePago updateDetallePago = _detallePago._listDetallePago.Find(x => x.idDetallePago == Convert.ToInt32(idDetallePago));

                    respuestaMessage = MessageBox.Show(mensaje, "Actualizar", botones);

                    if (respuestaMessage == DialogResult.OK)
                    {
                        updateDetallePago.update($"WHERE idDetallePago = {idDetallePago}");
                        //respuestaWebService = ws.consultaDetallePago(consulta, tipo);

                        if (Mensaje.result_bool)
                        {
                            Mensaje.responseMessage("", "", true);
                            //MessageBox.Show(Mensaje.responseMessage(Mensaje.);
                        }
                        //MessageBox.Show(respuestaWebService);

                        reloadForm();//cargarTabla("`idVenta` = " + idVenta + /*" AND  `comprador` =  '" + cbComprador.Text +"'*/" ORDER BY `DetallePago`.`idDetallePago` ASC");
                    }
                }
            }
        }

        private void tsCmdReestructuracion_Click(object sender, EventArgs e)
        {
            if (cbComprador.SelectedIndex > 0)
            {
                if (tsCmdReestructuracion.BackColor == Color.Green)
                {
                    tsCmdReestructuracion.BackColor = SystemColors.Control;
                }
                else
                {
                    tsCmdReestructuracion.BackColor = Color.Green;
                    chkNoGeneraraInteres.Checked = true;
                }
            }
            else
            {
                MessageBox.Show("Seleccione un comprador de la lista");
            }
            //if (cbComprador.SelectedIndex > 0)
            //{
            //    FrmTablaDeReestructuracion frmTablaDeReestructuracion = new FrmTablaDeReestructuracion(fechaCompra, proximoPago, mensualidadDeVentas, mensualidadProximoPago);

            //    frmTablaDeReestructuracion.Show();
            //}
            //else 
            //{
            //    MessageBox.Show("Seleccione un comprador de la lista");
            //}
        }

        private string interesMesActual()
        {

            decimal _interesTotal = 0;
            decimal MensualidadActual = Convert.ToDecimal(txtMensualidadMesActual.Text);

            int InteresPorPagar = _detallePago.calcularInteresAbonado(MensualidadActual);
            int mesesVencidos = _detallePago._mesesVencidos;

            decimal _interesMesActual = interes.mensual(MensualidadActual, 0.06M, InteresPorPagar); //_detallePago.calcularInteresAbonado(MensualidadActual));
            //decimal _interesMesActual = interes.mensual(Convert.ToDecimal(mensualidadDeVentas), 0.06M, _detallePago._mesesVencidos);
            if(_interesMesActual == 0)
            {
                txtInteresMesActual.Text = string.Format("{0:N2}", _interesTotal);

                return string.Format("{0:N2}", _interesTotal);
            }
            else if (_interesMesActual > _detallePago._totalAbonoInteres)
            {
               
                _interesTotal = _interesMesActual - _detallePago._totalAbonoInteres;//_detallePago.cacularInteres(_interesMesActual);
            }
            else
            {
                _interesTotal = _detallePago._totalAbonoInteres - _interesMesActual;
            }

            txtInteresMesActual.Text = string.Format("{0:N2}", _interesTotal);

            return string.Format("{0:N2}", _interesTotal);

            //double interesCalculado = f.calcularInteres(Convert.ToDateTime(proximoPago), Convert.ToDouble(mensualidadProximoPago));

            //if (interesCalculado != 0)
            //{
            //    //double interesPagado = Convert.ToDouble(dgvPagos[5, 0].Value.ToString());
            //    //double interesAbonado = Convert.ToDouble(dgvPagos[6, 0].Value.ToString());
            //    //double sumaInteres = 0;
            //    //double abonoMensualidad = Convert.ToDouble(dgvPagos[4, 0].Value.ToString());
            //    DateTime fechapago = DateTime.Today;//Convert.ToDateTime(dgvPagos[9, 0].Value.ToString());
            //    string pagoActual = txtPagoActual.Text;
            //    //string ultimoPagoActual = dgvPagos[2, 0].Value.ToString();
            //    double interesPagado = sumarInteresPagado(interesCalculado);
            //    bool abonoMensual = abonoMensualidad(ref fechapago);

            //    if (abonoMensual)
            //    {
            //        int distancia = f.distanciaMes(Convert.ToDateTime(proximoPago), fechapago);
            //        int distancia2 = f.distanciaMesVerificandoMes(Convert.ToDateTime(proximoPago), fechapago);

            //        if (distancia == distancia2)
            //        {
            //            if (distancia != 0 && distancia2 != 0) 
            //            {
            //                distancia += 1;
            //            }
            //            interesCalculado = f.calcularInteres(Convert.ToDateTime(proximoPago).AddMonths(distancia), Convert.ToDouble(mensualidadProximoPago));
            //            interesPagado = sumarInteresPagado(interesCalculado);

            //            //txtInteresMesActual.Text = string.Format("{0:N2}", interesCalculado);
            //            return string.Format("{0:N2}", Math.Round(interesPagado));
            //        }
            //        else 
            //        {
            //            interesCalculado = f.calcularInteres(Convert.ToDateTime(proximoPago).AddMonths(distancia), Convert.ToDouble(mensualidadProximoPago));
            //            interesPagado = sumarInteresPagado(interesCalculado);
            //            //txtInteresMesActual.Text = string.Format("{0:N2}", interesCalculado);
            //            return string.Format("{0:N2}", Math.Round(interesPagado));
            //        }
            //    }
            //    else
            //    {
            //        return string.Format("{0:N2}", Math.Round(interesPagado));
            //    //    if (interesAbonado != 0)
            //    //    {
            //    //        foreach (DataGridViewRow row in dgvPagos.Rows)
            //    //        {
            //    //            if (row.Cells[2].Value == DBNull.Value)
            //    //                continue;

            //    //            if (row.Cells[2].Value.ToString() == pagoActual)
            //    //            {
            //    //                double valorCeldaAbono = 0;
            //    //                double.TryParse(row.Cells[6].Value.ToString(), out valorCeldaAbono);
            //    //                double valorCeldaPago = 0;
            //    //                double.TryParse(row.Cells[5].Value.ToString(), out valorCeldaPago);

            //    //                sumaInteres += valorCeldaAbono + valorCeldaPago;
            //    //            }
            //    //            else
            //    //            {
            //    //                break;
            //    //            }
            //    //        }
            //    //        interesCalculado -= sumaInteres;
            //    //        //txtInteresMesActual.Text = string.Format("{0:N2}", interesCalculado);
            //    //        return string.Format("{0:N2}", interesCalculado);
            //    //    }
            //    //    else
            //    //    {
            //    //        if (interesPagado != 0 && ultimoPagoActual == pagoActual)
            //    //        {
            //    //            interesCalculado -= interesPagado;
            //    //            //txtInteresMesActual.Text = string.Format("{0:N2}", interesCalculado);
            //    //            return string.Format("{0:N2}", interesCalculado);
            //    //        }
            //    //        else 
            //    //        {
            //    //            //interesCalculado -= interesPagado;
            //    //            //txtInteresMesActual.Text = string.Format("{0:N2}", interesCalculado);
            //    //            return string.Format("{0:N2}", interesCalculado);
            //    //        }
            //    //    }
            //    }
            //}
            //else 
            //{
            //    return "0.00";
            //}
        }

        private double sumarInteresPagado(double interesCalculado) 
        {
            double sumaInteres = 0;

            foreach (DataGridViewRow row in dgvPagos.Rows)
            {
                if (row.Cells[2].Value == null)
                    break;

                if (row.Cells[2].Value.ToString() == pagoActual && row.Cells[4].Value.ToString() == "0.00" && row.Cells[3].Value.ToString() == "0.00")
                {
                    double valorCeldaAbono = 0;
                    double.TryParse(row.Cells[6].Value.ToString(), out valorCeldaAbono);
                    double valorCeldaPago = 0;
                    double.TryParse(row.Cells[5].Value.ToString(), out valorCeldaPago);

                    sumaInteres += valorCeldaAbono + valorCeldaPago;
                }
                else
                {
                    break;
                }
            }
            
            interesCalculado -= sumaInteres;
            //txtInteresMesActual.Text = string.Format("{0:N2}", interesCalculado);
            return  interesCalculado;
        }

        private bool abonoMensualidad(ref DateTime fechapago) 
        {
            foreach (DataGridViewRow row in dgvPagos.Rows)
            {
                if (row.Cells[2].Value == null)
                    break;

                if (row.Cells[2].Value.ToString() == pagoActual)
                {
                    if (row.Cells[4].Value.ToString() != "0.00") 
                    {
                        fechapago = Convert.ToDateTime(row.Cells[9].Value.ToString());
                        return true;
                    }
                }
            }
            return false;
        }

        private void tsCmdCorteCaja_Click(object sender, EventArgs e)
        {
            FrmCorteCaja pagos = new FrmCorteCaja(usuario, privilegios, idUsuario, administracionUsuario);

            pagos.Show();
        }

        private void inicializarPermisos() 
        {
            dataGridView1.Visible = false;
            if (privilegios == NivelAcceso.CAJERO.ToString())
            {
                tsCmdReestructuracion.Visible = false;
                chkNoGeneraraInteres.Visible = false;
                tsBtnConcepto.Visible = false;
                toolStripButton1.Visible = false;


                //tsCmdCorteCaja.Visible = false;
                //toolStrip1.Visible = false;
            }
        }

        private void tsBtnBuscarComprador_Click(object sender, EventArgs e)
        {
            FrmBuscarCliente bc = new FrmBuscarCliente();
            bc.recibir += new FrmBuscarCliente.pasar(ejecutar);
            bc.Show();
        }

        private void tsBtnPagar_Click(object sender, EventArgs e)
        {
            cmdPagoMensualidad.Enabled = false;
            // MEDIDAS DEL LOTE NORTE Y ESTE 
            //string respuestaMedidaLote = ws.getMedidaLote(splitIdLote[cbLote.SelectedIndex]);
            //string[] splitMedidaLote = respuestaMedidaLote.Split(new char[] { ',' });
            //string norte = splitMedidaLote[0];
            //string este = splitMedidaLote[2];
            // FN

            // COLONIA Y MUNICIPIO DEL PREDIO  
            //string colonia = ws.getDatoPredio("colonia", "nombre_predio", txtPredio.Text);
            //string municipio = ws.getDatoPredio("municipio", "nombre_predio", txtPredio.Text);
            // FIN
            if (txtMonto.Text != "")
            {
                string nombreClienteExiste = cbComprador.Text;

                if (cbComprador.FindStringExact(nombreClienteExiste) >= 0)
                {
                    String mensaje = "¿Confirma realizar el pago?";
                    String caption = "Pago Mensualidad";
                    MessageBoxButtons botones = MessageBoxButtons.OKCancel;
                    DialogResult respuesta;

                    respuesta = MessageBox.Show(mensaje, caption, botones);

                    if (respuesta == System.Windows.Forms.DialogResult.OK)
                    {
                        pagar();

                        cmdPagoMensualidad.Enabled = true;
                    }
                    else
                    {
                        cmdPagoMensualidad.Enabled = true;
                    }
                }
                else
                {
                    MessageBox.Show("verifique el nombre del comprador", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    cmdPagoMensualidad.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Favor agregar el monto a pagar");
                cmdPagoMensualidad.Enabled = true;
            }
        }
        public void ocultarcp()
        {
            txtPagoActual.Visible = true;
            txtDiaCorte.Visible = true;
            groupBox2.Visible = true;
            label11.Visible = true;
            label14.Visible = true;
            chkNoGeneraraInteres.Visible = true;
            txtInteresMesActual.Visible = true;
            lTotalInteres.Visible = true;
            label4.Visible = true;
            lMensualidad.Visible = true;
            bandera = true;
            txtMensualidadMesActual.Visible = true;
            txtTotalPagar.Visible = true;
            txtProximoPago.Visible = true;
            label12.Visible = true;
            tsBtnConcepto.Text = "Conceptos";
            tsBtnPagar.Visible = true;
            dgvPagos.Visible = true;
            dataGridView1.Visible = false;

        }
        public void mostrarcp()
        {
txtPagoActual.Visible = false;
            txtDiaCorte.Visible = false;
            groupBox2.Visible = false;
            label11.Visible = false;
            label14.Visible = false;
            chkNoGeneraraInteres.Visible = false;
            txtInteresMesActual.Visible = false;
            lTotalInteres.Visible = false;
            label4.Visible = false;
            lMensualidad.Visible = false;
            bandera = false;
            txtMensualidadMesActual.Visible = false;
            txtTotalPagar.Visible = false;
            txtProximoPago.Visible = false;
            label12.Visible = false;
            tsBtnPagar.Visible = false;
            dgvPagos.Visible = false;
            dataGridView1.Visible = true;
            tsBtnConcepto.Text = "*Conceptos*";
        }

        private void tsBtnConcepto_Click(object sender, EventArgs e)


        {
            if (txtPredio.Text == "" && txttotal.Text == "" && cbConcepto.SelectedItem == null && txtManzana.Text == "" && cbLote.Text == "" && txtMonto.Text == "" && tbactual.Text == "" && cbComprador.SelectedItem == null)
             {
                //MessageBox.Show("todo esta bacio");
                //insert y se oculta
                if (bandera == true)
                {
                    dgvPagos.Visible = false;
                    mostrarcp();
                }
                else if (bandera == false)
                {
                    dgvPagos.Visible = true;
                    ocultarcp();
                }
            }
            else if (txtPredio.Text != "" && txttotal.Text != ""  && txtManzana.Text != "" && cbConcepto.SelectedItem != null && cbLote.Text != "" && txtMonto.Text != "" && tbactual.Text != "" && cbComprador.SelectedItem != null)
            {

                string fecha = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").ToString();
                string concepto = cbConcepto.SelectedItem.ToString();
                int total = Int32.Parse(txttotal.Text);
                int pagarcp = Int32.Parse(tbactual.Text);
                int montopagar = Int32.Parse(txtMonto.Text);
                int montopagarCP = montopagar + Int32.Parse(tbactual.Text);
               
                CP.fk_id_compraventa = Int32.Parse(idVenta);
                CP.comprador = cbComprador.SelectedItem.ToString();  //no get
                CP.administracion = administracion;
                CP.fk_id_user = usuario;
                CP.fecha_concepto_pago = fecha;
                CP.cantidad_total = total;
                CP.cantidad_pagar = montopagar;
                CP.concepto = concepto;
                CP.lote = cbLote.SelectedItem.ToString();  //no get
                CP.manzana = txtManzana.Text;
                CP.predio = txtPredio.Text;

                if (concepto.Equals("Traspaso") || concepto.Equals("Cambio de titular"))
                {
                    if (total == montopagar)
                    {
                        //cambiar pagado
                        CP.status = "pagado";
                        CP.insert();

                        MessageBox.Show(Mensaje.getMessage(messageResponse.actionSuccess.ToString()));
                       
                        
                        limpiarcp();
                        ocultarcp();
                    }
                    else if (total > montopagar)
                    {
                        MessageBox.Show("en este concepto no se puede aboonar");
                        // mesnaje los valores de ven coincidir ya que no se puede abanor a este consepto   
                    }

                }
                //cambiar c to s
                else if (concepto.Equals("escrituracion") || concepto.Equals("Subdivision"))
                {
                    if (total == montopagarCP)
                    {
                        //cambiar pagado
                        CP.status = "pagado";
                        CP.insert();
                        MessageBox.Show(Mensaje.getMessage(messageResponse.actionSuccess.ToString()));
                        limpiarcp();
                        ocultarcp();
                    }
                    else if (total > montopagarCP)
                    {
                        CP.status = "abono";
                        CP.insert();
                        MessageBox.Show(Mensaje.getMessage(messageResponse.actionSuccess.ToString()));
                        limpiarcp();
                        ocultarcp();
                    }
                }
                //resultado del webservice 
               
            }
            else 
            {
                if (bandera == true)
                {
                    //limpia y se mete a concepto pago
                    limpiarcp();
                    mostrarcp();
                }
                else if (cbComprador.SelectedItem != null && txtPredio.Text != "" && txtManzana.Text != "" && cbLote.SelectedItem != null && cbConcepto.SelectedItem == null)
                {
                    limpiarcp();
                    ocultarcp();
                    //ocultarcp();
                }
                else
                {
                    MessageBox.Show("algo esta bacio");
                }
               
            }
        }

        private void txtMensualidadMesActual_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbConcepto_SelectedIndexChanged(object sender, EventArgs e)
        {

            Limpiardatosdepago();
            if (cbConcepto.SelectedItem != null)
            {
                string conceptocb = cbConcepto.SelectedItem.ToString();
                int Listcantidad_total = 0;
                int Listsumcantidad_pagar = 0;
                int restanteCP = 0;

                CP.queryFree("", $"SELECT cantidad_total, sum(cantidad_pagar) as sumcantidad_pagar FROM `ConceptoPago` WHERE fk_id_compraventa = {Int32.Parse(idVenta)} and concepto = '{conceptocb.ToUpper()}'");

                if (!CP.json.Contains("null"))
                {
                    _ListresultQueryCP = CP.list<ResultQueryCP>();
                    Listcantidad_total = _ListresultQueryCP[0].cantidad_total;
                    Listsumcantidad_pagar = _ListresultQueryCP[0].sumcantidad_pagar;
                    restanteCP = Listcantidad_total - Listsumcantidad_pagar;

                    if (Listcantidad_total == Listsumcantidad_pagar)
                    {
                        MessageBox.Show("ya esta pagado este concepto");                     
                    }
                    else if (Listcantidad_total > Listsumcantidad_pagar)
                    {
                        txttotal.ReadOnly = true;
                        txttotal.Text = Listcantidad_total.ToString();
                        tbactual.Text = Listsumcantidad_pagar.ToString();
                        txtMonto.Text = restanteCP.ToString();
                    }
                }
                else
                {
                    txttotal.ReadOnly = !true;
                    tbactual.Text = 0.ToString();
                    MessageBox.Show("ingresa total a pagar y la cantidad a pagar");
                }

            }
        }

        private void Limpiardatosdepago()
        {
            txttotal.Text = null;
            tbactual.Text = null;
            txtMonto.Text = null;
            


        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (dgvPagos.Visible == true)
            {
                dgvPagos.Visible = false;
                toolStripButton1.Image = imageList1.Images[0];
            }
            else
            {
                dgvPagos.Visible = true;
                toolStripButton1.Image = imageList1.Images[1];
            }
            

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (privilegios != NivelAcceso.CAJERO.ToString())
            {
                if (dataGridView1.Columns[e.ColumnIndex].Name.Equals("Eliminar"))
                {
                    int idconceptopago = Convert.ToInt32(dataGridView1.CurrentRow.Cells[1].Value.ToString());
                    //string consulta = "DELETE FROM `DetallePago` WHERE `idDetallePago` = " + idconceptopago + "";

                    //string respuestaWebService = "";
                    //string mensaje = "¿Confirmar " + accion + " registro?";
                    //MessageBoxButtons botones = MessageBoxButtons.OKCancel;
                    //DialogResult respuestaMessage;

                    ////DetallePago updateDetallePago = _detallePago._listDetallePago.Find(x => x.idDetallePago == Convert.ToInt32(idDetallePago));

                    //respuestaMessage = MessageBox.Show(mensaje, caption, botones);

                    //if (respuestaMessage == System.Windows.Forms.DialogResult.OK)
                    //{
                    //    respuestaWebService = ws.consultaDetallePago(consulta, tipo);
                    DialogResult result = MessageBox.Show("Esta seguro de eliminar el pago?", "Warning",
MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (result == DialogResult.Yes)
                    {

                        loadupdateCP(idconceptopago);
                        MessageBox.Show(Mensaje.getMessage(messageResponse.actionSuccess.ToString()));
                        // CP.queryFree("",$"DELETE FROM `ConceptoPago` WHERE `id_pago_concepto` = {idconceptopago}");
                        // CP.queryFree("", $"UPDATE `ConceptoPago` SET `fk_id_compraventa`= 0  WHERE `id_pago_concepto` = {idconceptopago}"); 
                        Loadgv(idVenta);
                       
                      //code for Yes

                    }
                    else if (result == DialogResult.No)
                    {
                        MessageBox.Show("cancelar  "); //code for No
                    }
                  
                  

                    //    reloadForm();//cargarTabla("`idVenta` = " + idVenta + /*" AND  `comprador` =  '" + cbComprador.Text +"'*/" ORDER BY `DetallePago`.`idDetallePago` ASC");
                    //}
                    // consulataDetallePago(consulta, "1", "eliminar", "Eliminar");


                    //  CP.queryFree("", $"DELETE FROM `ConceptoPago` WHERE `id_pago_concepto` = {idconceptopago}");



                }
            }

        }

        public void loadupdateCP(int idpagocp)
        {
            CP.queryFree("", $"SELECT * FROM `ConceptoPago` WHERE id_pago_concepto={idpagocp}");
            if (!CP.json.Equals("false"))
            {
                CP._list = CP.list<ConceptoPago>();
                CP._list[0].fk_id_compraventa = 0;
                CP._list[0].update($"WHERE id_pago_concepto = {idpagocp}");
                cbConcepto_SelectedIndexChanged(this,EventArgs.Empty);

            }


        }
    }
} 