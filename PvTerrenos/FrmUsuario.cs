﻿   using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;

namespace PvTerrenos
{
    public partial class FrmUsuario : Form 
    {
        #region VARIABLES
        int index = -1;
        string _indexLiteral = "";

        User user = new User();
        ComboBox cbVendedores = new ComboBox();
        //string DATE_CAST;
        #endregion

        #region CONSTRUCTOR
        public FrmUsuario(ref User user, ref ComboBox cbVendedores)
        {
            InitializeComponent();
            this.user = user;
            this.cbVendedores = cbVendedores;
            //user.loadList();
            //cbUsername = cbVendedores;
            user.loadComboBox(cbUsername, false);
        }
        #endregion

        #region EVENTOS
        #region CLIC
        //INSERTAR USUARIO
        private void btnCrear_Click(object sender, EventArgs e)
        {
            if (!ValidateForm.empty(this))
            {
                if (validatePassword())
                {
                    if (Mensaje.responseMessage(Mensaje.list_MessageBox[0], "usuario", false) == DialogResult.OK)
                    {
                        insert();
                    }
                }
                else
                {
                    MessageBox.Show("La contraseña no coincide");
                }
            }
            else
            {
                MessageBox.Show(Mensaje.result);
            }
        }

        //LIMPIAR FORMULARIO
        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Clear.clearForm(this);
            btnCrear.Text = " ";//GENERAR
            btnCrear.Image = Properties.Resources.insertar;//Recursos.imgResource("btnCrear.Image");
        }
        #endregion

        #region SELECTED INDEX CHANGE
        //CARGAR USUARIO
        private void cbUsername_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadForm();
            //index = cbUsername.SelectedIndex;
            btnCrear.Text = "  ";//ACTULIZAR
            btnCrear.Image = Properties.Resources.actulizar;//Recursos.imgResource("actulizar");
        }
        #endregion
        #endregion

        #region METODOS
        //INSERTAR ACTULIZAR USUARIO
        public void insert()
        {
            initializeUser();

            if (btnCrear.Text == " ")//GENERAR
            { 
                user._list.Last().insert();

                if (Mensaje.result_bool)
                {
                    user.insertLastId();
                    cbVendedores.Items.Add(user._list.Last().nombre);
                    reloadComboBoxUser();
                }
            }
            else
            {
                initizaliceUserUpdate();

                user._list[index].update
                    (
                        $"{user.WHERE} {user.id_user_a} {user.igual} {user._list[index].id_user}"
                    );
            }

            MessageBox.Show(Mensaje.result);

            if (Mensaje.result_bool)
            {
                //Clear.clearForm(this);
                //btnCrear.Text = " ";//GENERAR
                //btnCrear.Image = Properties.Resources.insertar;//Recursos.imgResource("btnCrear.Image");
            }
        }

        //VALIDAR PASSWORD
        public bool validatePassword()
        {
            string password = txtContraseña.Text;
            string confirmacion = txtConfirmacion.Text;

            if (password == confirmacion)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //CARGAR INFORMACION DEL USUARIO EN EL FORMULARIO
        public void loadForm()
        {
            //User user_update = new User();

            index = user._list.IndexOf
                (
                    user._list.Find(x => x.nombre == cbUsername.Text.ToLower())
                );
            
            //user.id_user = user._list[index].id_user;
            txtContraseña.Text = user._list[index].password;
            txtConfirmacion.Text = user._list[index].password;
            cbNivelAcceso.Text = user._list[index].tipo.ToString();
        }

        //INICIALIZAR LA INSTANCIA DEL USUARIO
        public void initializeUser()
        {
            user._list.Add
                (
                    new User()
                    {
                        nombre = cbUsername.Text.ToLower(),
                        password = txtContraseña.Text,
                        tipo = (NivelAcceso)cbNivelAcceso.SelectedIndex,
                        administracion = "GENERAL",
                        fecha_creacion = DateTime.Now.ToString(user.DATE_CAST),
                        fk_empleado = 0
                    }
                );
            //user.nombre = cbUsername.Text.ToLower();
            //user.password = txtContraseña.Text;
            //user.tipo = (NivelAcceso)cbNivelAcceso.Sele  ctedIndex;
            //user.administracion = "GENERAL";
            //user.fecha_creacion = DateTime.Now.ToString(user.DATE_CAST);
            //user.fk_empleado = "0";
        }

        public void initizaliceUserUpdate()
        {
            user._list[index].nombre = cbUsername.Text.ToLower();
            user._list[index].password = txtContraseña.Text;
            user._list[index].tipo = (NivelAcceso)cbNivelAcceso.SelectedIndex + 1;
            user._list[index].administracion = "GENERAL";
            user._list[index].fecha_creacion = DateTime.Now.ToString(user.DATE_CAST);
            user._list[index].fk_empleado = 0;
        }

        //RELOAD DATOS COMBOBOX USER
        public void reloadComboBoxUser()
        {
            _indexLiteral = cbUsername.Text;
            user.loadComboBox(cbUsername, false);
            cbUsername.SelectedIndex = cbUsername.FindString(_indexLiteral);
        }
        #endregion
    }
}
