﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvTerrenos
{
    class Recibo : CrearPdf
    {
        /*public List<string> pathPlantillaFrente { get; set; } = new List<string>();
        public List<string> pathPlantillaVuelta { get; set; } = new List<string>();
        public List<string> pathFoto { get; set; } = new List<string>();

        static int xx = 215;
        static int yy = 0;

        public int width_R1_frente = 245, height_R1_frente = 155, x_R1_Plantilla_frente = 87, y_R1_plantilla_frente = 664, rotacion_R1_frente = 0;
        public int width_R1_vuelta = 245, height_R1_vuelta = 155, x_R1_Plantilla_vuelta = 87, y_R1_plantilla_vuelta = 659, rotacion_R1_vuelta = 0;

        public int width_R2_frente = 245, height_R2_frente = 155, x_R2_Plantilla_frente = 87, y_R2_plantilla_frente = 491, rotacion_R2_frente = 0;
        public int width_R2_vuelta = 245, height_R2_vuelta = 155, x_R2_Plantilla_vuelta = 87, y_R2_plantilla_vuelta = 486, rotacion_R2_vuelta = 0;

        //Ciclo escolar, Nombre Alumno, leyenda, Nivel Estudios, Matricula , Grado y grupo 
        List<int> xR1_frente = new List<int>() { 295, xx, xx, 130, xx, 130 };
        List<int> yR1_frente = new List<int>() { 740, 720, 710, 690, 690, 675 };

        //tipo sangre, clave_incorporacion, bachillerato
        List<int> xR1_vuelta = new List<int>() { 275, 160, 160 };
        List<int> yR1_vuelta = new List<int>() { 710, 680, 672 };

        //Ciclo escolar, Nombre Alumno, leyenda, Nivel Estudios, Matricula , Grado y grupo 
        List<int> xR2_frente = new List<int>() { 295, xx, xx, 130, xx, 128 };
        List<int> yR2_frente = new List<int>() { 567, 547, 537, 516, 517, 501 };

        //tipo sangre, clave_incorporacion, bachillerato
        List<int> xR2_vuelta = new List<int>() { 275, 160, 160 };
        List<int> yR2_vuelta = new List<int>() { 538, 508, 500 };

        List<int> font_size_frente = new List<int> { 7, 9, 6, 8, 9, 9 };
        List<bool> negrita_frente = new List<bool> { true, true, false, true, true, true };

        List<int> font_size_vuelta = new List<int> { 20, 7, 7 };
        List<bool> negrita_vuelta = new List<bool> { true, true, true };

        List<int> x = new List<int>();
        List<int> y = new List<int>();

        List<int> font_size = new List<int>();
        List<bool> negrita = new List<bool>();*/
        List<string> nombreColumnas = new List<string> { "CONCEPTO", "PAGO", "TOTAL", "" };

        public void create(string _predio, string _manzana, string _lote, string _comprador, DetallePago _detallePago)
        {
            createPdf();
            openWrite();

            parrafo();
            parrafoAlignment(3);
            parrafoFont();

            parrafoAdd("                                                                        "
                        + "                                        " + _predio
                        + nL()
                        + "PAGO No " + _detallePago.pagoActual + " de " + "100"
                        + "                                                                  "
                        + "LOTE No " + _lote + " MANZANA " + _manzana
                        + nL()
                        + nL()
                        + "CLIENTE: " + _comprador
                        + nL()
                        + nL()
                        + "FECHA DE PAGO: " + DateTime.Today.ToString("dd-MMMM-yyyy"));

            addParrafo();

            pdfTable(4);

            tableConfiguracion();

            foreach (var nombre in nombreColumnas)
            {
                pdfCell(nombre,12);
                addCell();
            }

            pdfCell($"{_detallePago.tipo_pago}",11);
            cellPaddingBottom(70f);
            addCell();

            pdfCell($"${string.Format("{0:N2}", Convert.ToDecimal(_detallePago.mensualidad))}",11);
            addCell();

            pdfCell($"\n\n\n${string.Format("{0:N2}", Convert.ToDecimal(_detallePago.mensualidad))}", 11);
            addCell();

            pdfCell("",11);
            addCell();

            addTable();

            //if (frente)
            //{
            //    x = xR1_frente;
            //    y = yR1_frente;

            //    font_size = font_size_frente;
            //    negrita = negrita_frente;

            //    addImage(pathPlantillaFrente[0], width_R1_frente, height_R1_frente, x_R1_Plantilla_frente, y_R1_plantilla_frente, rotacion_R1_frente);

            //    addImage(pathFoto[0], 55, 68, 190, 735, 0);

            //    if (is_admin[0])
            //    {
            //        yR1_frente[1] = 710;
            //        yR1_frente[2] = 700;
            //    }

            //    //if (datos[0] == "DOCENTE")
            //    //{
            //    //    font_size[0] = 10;
            //    //}
            //}
            //else
            //{
            //    x = xR1_vuelta;
            //    y = yR1_vuelta;

            //    font_size = font_size_vuelta;
            //    negrita = negrita_vuelta;

            //    addImage(pathPlantillaVuelta[0].ToLower(), width_R1_vuelta, height_R1_vuelta, x_R1_Plantilla_vuelta, y_R1_plantilla_vuelta, rotacion_R1_vuelta);
            //}


            //for (int i = 0; i < datos.Count; i++)
            //{
            //    pdf.addLine(datos[i], x[i], y[i], 0, pdf.fontFormat(negrita[i]), font_size[i]);
            //}

            //if (dosFrentes)
            //{
            //    yy = 486;

            //    if (frente)
            //    {
            //        x = xR2_frente;
            //        y = yR2_frente;

            //        font_size = font_size_frente;
            //        negrita = negrita_frente;

            //        addImage(pathPlantillaFrente[1], width_R2_frente, height_R2_frente, x_R2_Plantilla_frente, y_R2_plantilla_frente, rotacion_R2_frente);
            //        addImage(pathFoto[1], 55, 68, 190, 562, 0);

            //        if (is_admin[1])
            //        {
            //            yR2_frente[1] = 537;
            //            yR2_frente[2] = 527;
            //        }

            //        //if (datos[0] == "DOCENTE")
            //        //{
            //        //    font_size[0] = 10;
            //        //}
            //    }
            //    else
            //    {
            //        x = xR2_vuelta;
            //        y = yR2_vuelta;

            //        font_size = font_size_vuelta;
            //        negrita = negrita_vuelta;

            //        addImage(pathPlantillaVuelta[1].ToLower(), width_R2_vuelta, height_R2_vuelta, x_R2_Plantilla_vuelta, y_R2_plantilla_vuelta, 0);
            //    }

            //    for (int i = 0; i < datos_2.Count; i++)
            //    {
            //        pdf.addLine(datos_2[i], x[i], y[i], 0, pdf.fontFormat(negrita[i]), font_size[i]);
            //    }
            //}

            closeWrite();
            closePdf();

            openPdf();
        }
    }
}
