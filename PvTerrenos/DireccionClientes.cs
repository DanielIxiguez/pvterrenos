﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvTerrenos
{
    class DireccionClientes : CRUD 
    {
        public string nombre { get; set; }
        public string status_venta { get; set; }
        public string fecha_compra { get; set; }

        private string table = "";

        public List<DireccionClientes> _list = new List<DireccionClientes>();
        public DireccionClientes()
        {
            table = COMPRAVENTA;
        }
        public void queryFree(string values_string, string query)
        {
            execute(table, $"{values_string}", action.free.ToString(), query);
        }

        public string values()
        {
            return
            //  $"{fk_id_lote.ToLower().Trim()}{cc}" +
            $"{nombre}{cc}" +
            $"{status_venta}{cc}" +
            $"{fecha_compra.ToUpper().Trim()}{cc}";
            
        }
    }

    


}
