﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace PvTerrenos
{
    class Comision : CRUD
    {
        public string nombre { get; set; }
        public string ultimoPago { get; set; }
        public string predio { get; set; }
        public string manzana { get; set; }
        public string lote { get; set; }

        public int fk_user { get; set; }
        public string fecha_pago_comision { get; set; }
        public int id_venta { get; set; }
        

        private string table = "";
        public List<Comision> _list = new List<Comision>();
        //private string json = "";

        //public List<Comision> comision_list { get; set; }

        public Comision()
        {
            table = COMISION;
        }

        #region METODOS
        public void insert()
        {
            execute(table, values(), action.insert.ToString(), "");
        }

        public void query(string values_string, string where)
        {
            execute(table, $"{values_string}", action.query.ToString(), where);
        }

        public void queryFree(string values_string, string query)
        {
            execute(table, $"{values_string}", action.free.ToString(), query);
        }

        public void update(string where)
        {
            execute(table, values(), action.update.ToString(), where);
        }

        public void loadList()
        {
            _list = list<Comision>();
        }

        #endregion

        #region METODOS INTERFACE
        /*public void loadComboBox(ComboBox cb)
        {
            load("1", $"{WHERE} ?");

            deserializeJson();

            foreach (var comision in comision_list)
            {
                cb.Items.Add(comision.ultimoPago.ToUpper());
            }
        }*/
        #endregion

        #region VALUES
        public string values()
        {
            return
            $"{ultimoPago.ToLower().Trim()}{cc}" +
            $"{predio.ToUpper().Trim()}{cc}" +
            $"{manzana}{cc}" +
            $"{lote}";
        }
        #endregion
    }
}
