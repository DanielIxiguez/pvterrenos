﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace PvTerrenos
{
    class ProximoPago : CRUD
    {
        public string id_proximo_pago { get; set; }
        public string monto { get; set; }
        public string proximo_pago { get; set; }
        public string  pago_actual { get; set; }
        public string pago_final { get; set; }
        public string status_mora { get; set; }
        public int pk_venta { get; set; }

        private string table = "";
        //private string json = "";

        public List<ProximoPago> proximo_pago_list { get; set; }

        public ProximoPago()
        {
            table = PROXIMOPAGO;
        }

        #region METODOS
        public void insert()
        {
            execute(table, values(), action.insert.ToString(), "");
        }

        public void query(string valuesString, string where)
        {
            execute(table, $"{valuesString}", action.query.ToString(), where);
        }

        public void update(string where)
        {
            execute(table, values(), action.update.ToString(), where);
        }

        public ProximoPago Initialize(ProximoPago proximo_pago)
        {
            if (proximo_pago_list != null)
            {
                return proximo_pago = proximo_pago_list[0];
            }
            else
            {
                return null;
            }
        }

        public void deserializeJson()
        {
            proximo_pago_list = JsonConvert.DeserializeObject<List<ProximoPago>>(json);
        }

        /*public string idManzana(string monto, string id_predio)
        {
            Manzana Manzana = ProximoPago_list.Find(x => x.monto == monto && x.pago_actual == id_predio);

            return Manzana.id_proximo_pago;
        }*/
        #endregion

        #region METODOS INTERFACE
        /*public void loadComboBox(ComboBox cb)
        {
            load("1", $"{WHERE} ?");

            deserializeJson();

            foreach (var ProximoPago in ProximoPago_list)
            {
                cb.Items.Add(ProximoPago.monto.ToUpper());
            }
        }*/
        #endregion

        #region VALUES
        public string values()
        {
            return
            $"{monto.ToLower().Trim()}{cc}" +
            $"{proximo_pago.ToUpper().Trim()}{cc}" +
            $"{pago_actual.ToUpper().Trim()}{cc}" +
            $"{pago_final.ToUpper().Trim()}{cc}" +
            $"{status_mora.ToUpper().Trim()}{cc}" +
            $"{pk_venta}";
        }
        #endregion
    }
}
