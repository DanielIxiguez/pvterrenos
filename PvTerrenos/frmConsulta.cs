﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace PvTerrenos
{
    public partial class frmConsulta : Form
    {
        WSpvt.PVT ws = new WSpvt.PVT();
        Fecha f = new Fecha();
        PdfCreate recibo = new PdfCreate();
        User user = new User();
        string[] splitId;
        string[] splitIdManzana;
        string datosVentaCliente;
        List<string> datosParaMostrar = new List<string>();
        string predio;
        string manzana;
        string noLote;
        string idVenta;
        string idLote;
        string mensualidad;
        string monto;
        string fechaCompra;
        string statusVenta;
        string proximoPago;
        string idPredio;
        string respuestaCargaManzana;
        string idLoteDeComboLote;
        string respuestaNombreComprador;


        public frmConsulta()
        {
            InitializeComponent();
            
            //TabControl1.TabPages(2).Enabled = False
            llenarComboPredio();
            //user.loadComboBox(cbVendedores);
        }

        private void cmbClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbClientes.SelectedIndex != -1)
            {
                txtIdCliente.Text = splitId[cbClientes.SelectedIndex];
                //string idCliente = txtIdCliente.Text;
                //datosVentaCliente = ws.getVentasCliente(idCliente);
                //cbLotesCargaInfo.Visible = false;
                //lNumeroLote.Visible = false;
                //cmdModificarVenta.Visible = false;
                //cmdTraspasar.Visible = false;
                //cmdModificarPago.Visible = false;
                //lFechaCompra.Visible = false;
                //lProximoPago.Visible = false;
                //dtpFechaCompra.Visible = false;
                //dtpProximoPago.Visible = false;
                //cbClienteTraspaso.Visible = false;
                //cbLotesCargaInfo.Text = "";
                //cbLotesCargaInfo.Items.Clear();
                //dgvDatosVenta.Rows.Clear();
                limpiar();
                llenaVentas();
            }
        }


        //modificar datos compra venta

        private void cmdModificarVenta_Click(object sender, EventArgs e)
        {
            string[] datosModificacion;
            string respuesta;
            string monto;
            string mensualidad;
            string fechaCompra;
            string diaCorte;
            int fk_user;

            datosModificacion = datosParaMostrar[cbLotesCargaInfo.SelectedIndex].Split(new char[] { ',' });
            idLote = datosModificacion[7];
            idVenta = datosModificacion[8];

            string mensaje = "¿Estas seguro de que quieres modificar esta venta?";
            string caption = "Modificación de venta";
            MessageBoxButtons botones = MessageBoxButtons.OKCancel;
            DialogResult resultado;

            if (valida())
            {
                resultado = MessageBox.Show(mensaje, caption, botones);
                if (resultado == System.Windows.Forms.DialogResult.OK)
                {
                    monto = dgvDatosVenta[3, 0].Value.ToString();
                    mensualidad = dgvDatosVenta[4, 0].Value.ToString();
                    fechaCompra = dtpFechaCompra.Value.ToString();
                    diaCorte = dtpFechaCompra.Value.Day.ToString();
                    fk_user = user.list<User>().Find(x => x.nombre == cbVendedores.Text.ToLower()).id_user;//idUser(cbVendedores.Text.ToLower());

                    //respuesta = ws.modificaVenta(monto, mensualidad, fechaCompra, diaCorte, idLote, idVenta);
                    //MessageBox.Show(respuesta);
                }
            }
        }

        private void cmdModificarPago_Click_1(object sender, EventArgs e)
        {
            string[] datosModificacion;
            string respuesta;
            string mensualidad;
            string pagoActual;
            string pagoFinal;
            string proximoPago;
            string status;
            datosModificacion = datosParaMostrar[cbLotesCargaInfo.SelectedIndex].Split(new char[] { ',' });
            idLote = datosModificacion[7];
            idVenta = datosModificacion[8];

            string mensaje = "¿Estas seguro de que quieres modificar esta venta?";
            string caption = "Modificación de venta";
            MessageBoxButtons botones = MessageBoxButtons.OKCancel;
            DialogResult resultado;

            if (valida())
            {
                resultado = MessageBox.Show(mensaje, caption, botones);
                if (resultado == System.Windows.Forms.DialogResult.OK)
                {
                    mensualidad = dgvProximoPago[0, 0].Value.ToString();
                    pagoActual = dgvProximoPago[1, 0].Value.ToString();
                    pagoFinal = dgvProximoPago[2, 0].Value.ToString();
                    proximoPago = dtpProximoPago.Value.ToString();
                    status = dgvProximoPago[3, 0].Value.ToString();

                    if (datosModificacion[9] != "" || datosModificacion[10] != "")
                    {

                        if (status == "Cuenta al corriente")
                        {
                            status = "0";
                        }
                        else if (status == "Cuenta vencida")
                        {
                            status = "1";
                        }

                        try
                        {
                            respuesta = ws.updateProximoPago(idVenta, mensualidad, proximoPago, pagoActual, pagoFinal, status);
                            MessageBox.Show(respuesta);
                        }
                        catch (Exception) { }
                    }
                    else 
                    {
                        respuesta = ws.registraProximoPago(idVenta, mensualidad, proximoPago, pagoActual, pagoFinal);
                        MessageBox.Show(respuesta);
                        int selectedIndex = cbClientes.SelectedIndex;
                        this.cbClientes.SelectedIndex = -1;
                        this.cbClientes.SelectedIndex = selectedIndex;
                    }
                }
            }
        }

        private void cmdLiberarLote_Click_1(object sender, EventArgs e)
        {
            string respuesta = "";

            if (idVenta != null && idVenta != "")
            {
                respuesta = ws.cancelarVenta(idVenta, idLote);

                MessageBox.Show("El lote " + cbLotes.Text + " de la manzana " + cbManzana.Text + " del predio " + cbPredio.Text + "\n" + respuesta);
            }
            else
            {
                respuesta = ws.updateStatusLote(idLoteDeComboLote, "0");
                MessageBox.Show("El lote " + cbLotes.Text + " de la manzana " + cbManzana.Text + " del predio " + cbPredio.Text + "\n " + respuesta);
            }
            cmdLiberarLote.Enabled = false;
            limpiar();
        }

        private void cmdTraspasar_Click_1(object sender, EventArgs e)
        {
            string cliente = this.cbClienteTraspaso.Text;
            string query = "UPDATE `CompraVenta` SET `pk_comprador`= (SELECT `id_comprador` FROM `Comprador` WHERE `nombre` = '" + cliente + "') WHERE `id_venta` = '" + idVenta + "'";
            string respuesta = this.ws.updateVenta(query);

            if (respuesta == "No se realizo el traspaso!")
            {
                MessageBox.Show(respuesta);
            }
            else
            {
                MessageBox.Show(respuesta + "\n\nEl nuevo cliente de este terreno es " + cliente);
            }
            int selectedIndex = this.cbClienteTraspaso.SelectedIndex;
            this.cbClientes.SelectedIndex = -1;
            this.cbClientes.SelectedIndex = selectedIndex;
            this.cbClienteTraspaso.SelectedIndex = -1;
        }


        private void cbPredio_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            cbManzana.Items.Clear();

            idPredio = ws.getIdPredio(cbPredio.SelectedItem.ToString());

            respuestaCargaManzana = ws.cargaColumnaTablaManzana(idPredio, "n_manzana");

            if (respuestaCargaManzana != "")
            {
                string respuestaIdManzana = ws.cargaColumnaTablaManzana(idPredio, "id_manzana");

                splitIdManzana = respuestaIdManzana.Split(new char[] { ',' });

                string[] splitCargaManzana = respuestaCargaManzana.Split(new char[] { ',' });

                foreach (string cargaCombo in splitCargaManzana)
                {

                    cbManzana.Items.Add(cargaCombo);
                }
            }
            else
            {
                cbManzana.Items.Clear();
                cbLotes.Items.Clear();

                // string idPredio = ws.getIdPredio((string)cbPredio.SelectedItem);
                //string idManzana = ws.getIdManzana((string)cbManzana.SelectedItem, idPredio);
                string respuestaCargaLote = ws.cargaLotesDePredio(idPredio);

                string[] splitCargaLotes = respuestaCargaLote.Split(new char[] { ',' });
                int tamaño = splitCargaLotes.Length;
                foreach (string cargaLotes in splitCargaLotes)
                {
                    cbLotes.Items.Add(cargaLotes);
                }
            }
        }

        private void cbManzana_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            cbLotes.Items.Clear();
            //WSpvt.PVT ws = new WSpvt.PVT();

            // string idPredio = ws.getIdPredio((string)cbPredio.SelectedItem);
            //string idManzana = ws.getIdManzana((string)cbManzana.SelectedItem, idPredio);
            string respuestaCargaLote = ws.cargaLotesVenta(splitIdManzana[cbManzana.SelectedIndex]);

            string[] splitCargaLotes = respuestaCargaLote.Split(new char[] { ',' });

            int tamaño = splitCargaLotes.Length;

            foreach (string cargaLotes in splitCargaLotes)
            {
                cbLotes.Items.Add(cargaLotes);
            }
        }

        private void cbLotes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (respuestaCargaManzana != "")
            {
                idLoteDeComboLote = ws.getIdLote(splitIdManzana[cbManzana.SelectedIndex], cbLotes.SelectedItem.ToString(), "pk_manzana");
            }
            else
            {
                idLoteDeComboLote = ws.getIdLote(idPredio, cbLotes.SelectedItem.ToString(), "pk_predio");
            }

            if (cbLotes.SelectedIndex >= 0)
            {
                cmdLiberarLote.Enabled = true;
            }
        }

        private void cbClienteTraspaso_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cbClienteTraspaso.SelectedIndex == -1)
            {
                this.cmdTraspasar.Enabled = false;
            }
            else
            {
                cmdTraspasar.Enabled = true;
            }
        }

       
        public void llenaComboClientes(Comprador comprador)
        {
            respuestaNombreComprador = ws.cargaComprador();
            string[] splitDatosComprador = respuestaNombreComprador.Split(new char[] { '|' });
            string[] splitComprador = splitDatosComprador[0].Split(new char[] { ',' });
            splitId = splitDatosComprador[1].Split(new char[] { ',' });
            
            foreach (var comprador_name in comprador._list)
            {
                cbClientes.Items.Add(comprador_name.nombre);
                cbClienteTraspaso.Items.Add(comprador_name.nombre);
            }
        }

        public void llenaVentas()
        {
            txtIdCliente.Text = splitId[cbClientes.SelectedIndex];
            datosParaMostrar.Clear();
            string idCliente = txtIdCliente.Text;
            string[] separaVenta;
            string[] splitVenta;

            try
            {
                datosVentaCliente = ws.getVentasCliente(idCliente);
            }
            catch (Exception) { }

            if (datosVentaCliente.Length > 6)
            {
                cbLotesCargaInfo.Visible = true;
                lNumeroLote.Visible = true;

                separaVenta = datosVentaCliente.Split(new char[] { '|' });/// primero separamos venta a venta, en caso de q haya mas de una

                foreach (string datos in separaVenta)
                {
                    splitVenta = datos.Split(new char[] { ',' });/// separamos los datos que estamos reciviendo de la venta para ponerlo

                    idLote = splitVenta[1];

                    string datosLote = ws.getInfoLotes(idLote);
                    string[] desgloseDatosLote = datosLote.Split(new char[] { ',' });

                    //datosLote -> numero Lote, Predio, numero manzana
                    string[] splitLote = datosLote.Split(new char[] { ',' });

                    idVenta = splitVenta[0];
                    mensualidad = splitVenta[2];
                    monto = splitVenta[3];
                    fechaCompra = splitVenta[4];

                    noLote = splitLote[0];
                    predio = splitLote[1];
                    manzana = splitLote[2];

                    string proximoPagoValues = ws.getProximoPago(idVenta);
                    string[] splitPago = proximoPagoValues.Split(new char[] { ',' });

                    string mensualidadProximoPago = splitPago[0];
                    proximoPago = splitPago[1];
                    string pagoActual = splitPago[2];
                    string pagoFinal = splitPago[3];
                    string status = splitPago[4];

                    cbLotesCargaInfo.Items.Add(desgloseDatosLote[0]);
                    datosParaMostrar.Add(predio + "," + manzana + "," + noLote + "," + monto + "," + mensualidad + "," + fechaCompra + "," + proximoPago + "," + idLote + "," + idVenta + "," + mensualidadProximoPago + "," + pagoActual + "," + pagoFinal + "," + status);//9 10 11 12
                }
                if (datosParaMostrar.Count > 1)
                {
                    MessageBox.Show("Usuario con mas de una compra");
                }
                cbLotesCargaInfo.SelectedIndex = 0;
            }
            else
            {
                MessageBox.Show("Este cliente no tiene ventas registradas");
            }
        }

        public bool valida()
        {

            if (dgvDatosVenta[3, 0].Value.ToString() == null || dgvDatosVenta[3, 0].Value.ToString() == " ")
            {

                MessageBox.Show("El campo monto está vacío!");
                return false;
            }

            if (dgvDatosVenta[4, 0].Value.ToString() == null || dgvDatosVenta[4, 0].Value.ToString() == " ")
            {

                MessageBox.Show("El campo mensualidad está vacío!");
                return false;
            }
            else
            {
                return true;
            }

        }

        public void llenarComboPredio()
        {
            string respuestaCargaPredio = "";

            try
            {
                respuestaCargaPredio = ws.cargaColumnaTablaPredio("nombre_predio");
            }
            catch (Exception)
            {
                MessageBox.Show("Tiempo de espera ecxedido posible problema en la red");
                //FrmVentaLote ventalote = new FrmVentaLote();
                //ventalote.Close();
            }
            string[] splitPredios = respuestaCargaPredio.Split(new char[] { ',' });

            foreach (string cargaCombo in splitPredios)
            {
                cbPredio.Items.Add(cargaCombo);
            }
        }

        //llenar datos en todo el formulario
        private void cbLotesCargaInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] datosDGV;
            //limpiamos el dataGridview
            this.dgvDatosVenta.Rows.Clear();
            cmdModificarVenta.Visible = true;
            cmdTraspasar.Visible = true;
            cmdModificarPago.Visible = true;
            cbClienteTraspaso.Visible = true;
            lFechaCompra.Visible = true;
            lProximoPago.Visible = true;
            dtpFechaCompra.Visible = true;
            dtpProximoPago.Visible = true;
            //rowcbStatusMora.Items.Clear();
            dgvProximoPago.Rows.Clear();

            int index = cbLotesCargaInfo.SelectedIndex;
            datosDGV = datosParaMostrar[index].Split(new char[] { ',' });

            idVenta = datosDGV[8];
            idLote = datosDGV[7];

            string predio = datosDGV[0];
            string manzana = datosDGV[1];
            string numeroLote = datosDGV[2];
            string monto = datosDGV[3];
            string mensualidad = datosDGV[4];
            string mensualidadProximoPago = datosDGV[9];
            string pagoActual = datosDGV[10];
            string pagoFinal = datosDGV[11];
            int status = 0;

            if (datosDGV[12] != "")
            {
                status = Convert.ToInt32(datosDGV[12]);
            }

            if (datosDGV[6] == "")
            {
                datosDGV[6] = DateTime.Today.ToString();
            }


            //llenamos el dataGridView
            dgvDatosVenta.Rows.Insert(0, predio, manzana, numeroLote, monto, mensualidad);
            dgvProximoPago.Rows.Insert(0, mensualidadProximoPago, pagoActual, pagoFinal);

            if (status == 1 || f.estaEnMora(Convert.ToDateTime(datosDGV[6])) == "mora")
            {

                //rowcbStatusMora.Items.Add("Cuenta vencida");
                dgvProximoPago.CurrentRow.Cells["rowcbStatusMora"].Value = "Cuenta vencida";
                dgvProximoPago.CurrentRow.Cells["rowcbStatusMora"].ReadOnly = false;
                //dgvProximoPago.Rows.Insert(3, "Cuenta vencida");
                //dgvProximoPago.CurrentRow.Cells[3].Value = "Cuenta vencida";
            }
            else if (status == 0)
            {
                //rowcbStatusMora.Items.Add("Cuenta al corriente");
                dgvProximoPago.CurrentRow.Cells["rowcbStatusMora"].Value = "Cuenta al corriente";
                dgvProximoPago.CurrentRow.Cells["rowcbStatusMora"].ReadOnly = true;
            }

            //lleno la fecha de compra
            dtpFechaCompra.Value = Convert.ToDateTime(datosDGV[5]);
            //lleno la fecha de corte
            dtpProximoPago.Value = Convert.ToDateTime(datosDGV[6]);
        }

        private void limpiar() 
        {
            txtIdCliente.Text = "";

            cbLotesCargaInfo.Visible = false;
            lNumeroLote.Visible = false;
            cmdModificarVenta.Visible = false;
            cmdTraspasar.Visible = false;
            cmdModificarPago.Visible = false;
            lFechaCompra.Visible = false;
            lProximoPago.Visible = false;
            dtpFechaCompra.Visible = false;
            dtpProximoPago.Visible = false;
            cbClienteTraspaso.Visible = false;

            cbLotesCargaInfo.Text = "";
            cbLotesCargaInfo.Items.Clear();

            cbPredio.Text = "";
            cbManzana.Text = "";
            cbPredio.Text = "";

            dgvProximoPago.Rows.Clear();
            dgvDatosVenta.Rows.Clear();
        }
    }
}
