﻿namespace PvTerrenos
{
    partial class frmConsulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbClientes = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIdCliente = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmdModificarPago = new System.Windows.Forms.Button();
            this.dgvProximoPago = new System.Windows.Forms.DataGridView();
            this.rowMensualidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowPagoActual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowPagoFinal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowcbStatusMora = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.lProximoPago = new System.Windows.Forms.Label();
            this.dtpProximoPago = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmdTraspasar = new System.Windows.Forms.Button();
            this.cbVendedores = new System.Windows.Forms.ComboBox();
            this.cbClienteTraspaso = new System.Windows.Forms.ComboBox();
            this.lFechaCompra = new System.Windows.Forms.Label();
            this.dtpFechaCompra = new System.Windows.Forms.DateTimePicker();
            this.lNumeroLote = new System.Windows.Forms.Label();
            this.cbLotesCargaInfo = new System.Windows.Forms.ComboBox();
            this.dgvDatosVenta = new System.Windows.Forms.DataGridView();
            this.predioDGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.manzanaDGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loteDGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montoDGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mensualidadDGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmdModificarVenta = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.cbManzana = new System.Windows.Forms.ComboBox();
            this.cbLotes = new System.Windows.Forms.ComboBox();
            this.cbPredio = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmdLiberarLote = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProximoPago)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosVenta)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbClientes
            // 
            this.cbClientes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbClientes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbClientes.FormattingEnabled = true;
            this.cbClientes.Location = new System.Drawing.Point(100, 6);
            this.cbClientes.Name = "cbClientes";
            this.cbClientes.Size = new System.Drawing.Size(355, 21);
            this.cbClientes.TabIndex = 1;
            this.cbClientes.SelectedIndexChanged += new System.EventHandler(this.cmbClientes_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nombre cliente:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(461, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "ID cliente:";
            // 
            // txtIdCliente
            // 
            this.txtIdCliente.Location = new System.Drawing.Point(522, 6);
            this.txtIdCliente.Name = "txtIdCliente";
            this.txtIdCliente.Size = new System.Drawing.Size(77, 20);
            this.txtIdCliente.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(605, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Status:";
            this.label9.Visible = false;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(651, 7);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(96, 20);
            this.textBox7.TabIndex = 6;
            this.textBox7.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox2.Controls.Add(this.cmdModificarPago);
            this.groupBox2.Controls.Add(this.dgvProximoPago);
            this.groupBox2.Controls.Add(this.lProximoPago);
            this.groupBox2.Controls.Add(this.dtpProximoPago);
            this.groupBox2.Location = new System.Drawing.Point(10, 179);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(896, 81);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos proximo pago";
            // 
            // cmdModificarPago
            // 
            this.cmdModificarPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdModificarPago.Location = new System.Drawing.Point(778, 38);
            this.cmdModificarPago.Name = "cmdModificarPago";
            this.cmdModificarPago.Size = new System.Drawing.Size(115, 27);
            this.cmdModificarPago.TabIndex = 21;
            this.cmdModificarPago.Text = "Modificar pago";
            this.cmdModificarPago.UseVisualStyleBackColor = true;
            this.cmdModificarPago.Visible = false;
            this.cmdModificarPago.Click += new System.EventHandler(this.cmdModificarPago_Click_1);
            // 
            // dgvProximoPago
            // 
            this.dgvProximoPago.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProximoPago.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProximoPago.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rowMensualidad,
            this.rowPagoActual,
            this.rowPagoFinal,
            this.rowcbStatusMora});
            this.dgvProximoPago.Location = new System.Drawing.Point(9, 23);
            this.dgvProximoPago.Name = "dgvProximoPago";
            this.dgvProximoPago.Size = new System.Drawing.Size(471, 48);
            this.dgvProximoPago.TabIndex = 21;
            // 
            // rowMensualidad
            // 
            this.rowMensualidad.HeaderText = "Mensualidad";
            this.rowMensualidad.Name = "rowMensualidad";
            // 
            // rowPagoActual
            // 
            this.rowPagoActual.HeaderText = "Pago actual";
            this.rowPagoActual.Name = "rowPagoActual";
            // 
            // rowPagoFinal
            // 
            this.rowPagoFinal.HeaderText = "Pago Final";
            this.rowPagoFinal.Name = "rowPagoFinal";
            // 
            // rowcbStatusMora
            // 
            this.rowcbStatusMora.FillWeight = 120F;
            this.rowcbStatusMora.HeaderText = "Status Mora";
            this.rowcbStatusMora.Items.AddRange(new object[] {
            "Cuenta al corriente",
            "Cuenta vencida"});
            this.rowcbStatusMora.Name = "rowcbStatusMora";
            this.rowcbStatusMora.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.rowcbStatusMora.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // lProximoPago
            // 
            this.lProximoPago.AutoSize = true;
            this.lProximoPago.Location = new System.Drawing.Point(486, 45);
            this.lProximoPago.Name = "lProximoPago";
            this.lProximoPago.Size = new System.Drawing.Size(75, 13);
            this.lProximoPago.TabIndex = 20;
            this.lProximoPago.Text = "Proximo Pago:";
            this.lProximoPago.Visible = false;
            // 
            // dtpProximoPago
            // 
            this.dtpProximoPago.Location = new System.Drawing.Point(571, 41);
            this.dtpProximoPago.Name = "dtpProximoPago";
            this.dtpProximoPago.Size = new System.Drawing.Size(200, 20);
            this.dtpProximoPago.TabIndex = 18;
            this.dtpProximoPago.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.LightGray;
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cmdTraspasar);
            this.groupBox1.Controls.Add(this.cbVendedores);
            this.groupBox1.Controls.Add(this.cbClienteTraspaso);
            this.groupBox1.Controls.Add(this.lFechaCompra);
            this.groupBox1.Controls.Add(this.dtpFechaCompra);
            this.groupBox1.Controls.Add(this.lNumeroLote);
            this.groupBox1.Controls.Add(this.cbLotesCargaInfo);
            this.groupBox1.Controls.Add(this.dgvDatosVenta);
            this.groupBox1.Controls.Add(this.cmdModificarVenta);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox1.Location = new System.Drawing.Point(10, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(896, 125);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos venta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(483, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 42;
            this.label3.Text = "Vendedor:";
            // 
            // cmdTraspasar
            // 
            this.cmdTraspasar.Enabled = false;
            this.cmdTraspasar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdTraspasar.Location = new System.Drawing.Point(774, 52);
            this.cmdTraspasar.Name = "cmdTraspasar";
            this.cmdTraspasar.Size = new System.Drawing.Size(115, 27);
            this.cmdTraspasar.TabIndex = 20;
            this.cmdTraspasar.Text = "Traspasar";
            this.cmdTraspasar.UseVisualStyleBackColor = true;
            this.cmdTraspasar.Visible = false;
            this.cmdTraspasar.Click += new System.EventHandler(this.cmdTraspasar_Click_1);
            // 
            // cbVendedores
            // 
            this.cbVendedores.FormattingEnabled = true;
            this.cbVendedores.Location = new System.Drawing.Point(483, 98);
            this.cbVendedores.Name = "cbVendedores";
            this.cbVendedores.Size = new System.Drawing.Size(121, 21);
            this.cbVendedores.TabIndex = 41;
            // 
            // cbClienteTraspaso
            // 
            this.cbClienteTraspaso.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbClienteTraspaso.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbClienteTraspaso.FormattingEnabled = true;
            this.cbClienteTraspaso.Location = new System.Drawing.Point(483, 55);
            this.cbClienteTraspaso.Name = "cbClienteTraspaso";
            this.cbClienteTraspaso.Size = new System.Drawing.Size(285, 21);
            this.cbClienteTraspaso.TabIndex = 7;
            this.cbClienteTraspaso.Visible = false;
            this.cbClienteTraspaso.SelectedIndexChanged += new System.EventHandler(this.cbClienteTraspaso_SelectedIndexChanged_1);
            // 
            // lFechaCompra
            // 
            this.lFechaCompra.AutoSize = true;
            this.lFechaCompra.Location = new System.Drawing.Point(483, 23);
            this.lFechaCompra.Name = "lFechaCompra";
            this.lFechaCompra.Size = new System.Drawing.Size(79, 13);
            this.lFechaCompra.TabIndex = 19;
            this.lFechaCompra.Text = "Fecha Compra:";
            this.lFechaCompra.Visible = false;
            // 
            // dtpFechaCompra
            // 
            this.dtpFechaCompra.Location = new System.Drawing.Point(568, 19);
            this.dtpFechaCompra.Name = "dtpFechaCompra";
            this.dtpFechaCompra.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaCompra.TabIndex = 17;
            this.dtpFechaCompra.Visible = false;
            // 
            // lNumeroLote
            // 
            this.lNumeroLote.AutoSize = true;
            this.lNumeroLote.Location = new System.Drawing.Point(782, 23);
            this.lNumeroLote.Name = "lNumeroLote";
            this.lNumeroLote.Size = new System.Drawing.Size(48, 13);
            this.lNumeroLote.TabIndex = 16;
            this.lNumeroLote.Text = "No Lote:";
            this.lNumeroLote.Visible = false;
            // 
            // cbLotesCargaInfo
            // 
            this.cbLotesCargaInfo.FormattingEnabled = true;
            this.cbLotesCargaInfo.Location = new System.Drawing.Point(836, 18);
            this.cbLotesCargaInfo.Name = "cbLotesCargaInfo";
            this.cbLotesCargaInfo.Size = new System.Drawing.Size(53, 21);
            this.cbLotesCargaInfo.TabIndex = 15;
            this.cbLotesCargaInfo.Visible = false;
            this.cbLotesCargaInfo.SelectedIndexChanged += new System.EventHandler(this.cbLotesCargaInfo_SelectedIndexChanged);
            // 
            // dgvDatosVenta
            // 
            this.dgvDatosVenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatosVenta.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.predioDGV,
            this.manzanaDGV,
            this.loteDGV,
            this.montoDGV,
            this.mensualidadDGV});
            this.dgvDatosVenta.Location = new System.Drawing.Point(6, 19);
            this.dgvDatosVenta.Name = "dgvDatosVenta";
            this.dgvDatosVenta.Size = new System.Drawing.Size(471, 88);
            this.dgvDatosVenta.TabIndex = 14;
            // 
            // predioDGV
            // 
            this.predioDGV.FillWeight = 151.8692F;
            this.predioDGV.HeaderText = "Predio";
            this.predioDGV.Name = "predioDGV";
            this.predioDGV.ReadOnly = true;
            this.predioDGV.Width = 130;
            // 
            // manzanaDGV
            // 
            this.manzanaDGV.FillWeight = 79.83741F;
            this.manzanaDGV.HeaderText = "Manzana";
            this.manzanaDGV.Name = "manzanaDGV";
            this.manzanaDGV.ReadOnly = true;
            this.manzanaDGV.Width = 68;
            // 
            // loteDGV
            // 
            this.loteDGV.FillWeight = 50.80823F;
            this.loteDGV.HeaderText = "No. Lote";
            this.loteDGV.Name = "loteDGV";
            this.loteDGV.ReadOnly = true;
            this.loteDGV.Width = 44;
            // 
            // montoDGV
            // 
            this.montoDGV.FillWeight = 126.147F;
            this.montoDGV.HeaderText = "Monto";
            this.montoDGV.Name = "montoDGV";
            this.montoDGV.Width = 108;
            // 
            // mensualidadDGV
            // 
            this.mensualidadDGV.FillWeight = 91.33821F;
            this.mensualidadDGV.HeaderText = "Mensualidad";
            this.mensualidadDGV.Name = "mensualidadDGV";
            this.mensualidadDGV.Width = 78;
            // 
            // cmdModificarVenta
            // 
            this.cmdModificarVenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdModificarVenta.Location = new System.Drawing.Point(775, 92);
            this.cmdModificarVenta.Name = "cmdModificarVenta";
            this.cmdModificarVenta.Size = new System.Drawing.Size(115, 27);
            this.cmdModificarVenta.TabIndex = 13;
            this.cmdModificarVenta.Text = "Modificar venta";
            this.cmdModificarVenta.UseVisualStyleBackColor = true;
            this.cmdModificarVenta.Visible = false;
            this.cmdModificarVenta.Click += new System.EventHandler(this.cmdModificarVenta_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Predio:";
            // 
            // cbManzana
            // 
            this.cbManzana.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbManzana.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbManzana.FormattingEnabled = true;
            this.cbManzana.Location = new System.Drawing.Point(349, 29);
            this.cbManzana.Name = "cbManzana";
            this.cbManzana.Size = new System.Drawing.Size(44, 21);
            this.cbManzana.TabIndex = 23;
            this.cbManzana.SelectedIndexChanged += new System.EventHandler(this.cbManzana_SelectedIndexChanged_1);
            // 
            // cbLotes
            // 
            this.cbLotes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbLotes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbLotes.FormattingEnabled = true;
            this.cbLotes.Location = new System.Drawing.Point(439, 29);
            this.cbLotes.Name = "cbLotes";
            this.cbLotes.Size = new System.Drawing.Size(41, 21);
            this.cbLotes.TabIndex = 22;
            this.cbLotes.SelectedIndexChanged += new System.EventHandler(this.cbLotes_SelectedIndexChanged);
            // 
            // cbPredio
            // 
            this.cbPredio.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbPredio.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPredio.FormattingEnabled = true;
            this.cbPredio.Location = new System.Drawing.Point(52, 29);
            this.cbPredio.Name = "cbPredio";
            this.cbPredio.Size = new System.Drawing.Size(225, 21);
            this.cbPredio.TabIndex = 21;
            this.cbPredio.SelectedIndexChanged += new System.EventHandler(this.cbPredio_SelectedIndexChanged_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(283, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Manazana:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(402, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Lote:";
            // 
            // cmdLiberarLote
            // 
            this.cmdLiberarLote.Enabled = false;
            this.cmdLiberarLote.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdLiberarLote.Location = new System.Drawing.Point(778, 26);
            this.cmdLiberarLote.Name = "cmdLiberarLote";
            this.cmdLiberarLote.Size = new System.Drawing.Size(115, 27);
            this.cmdLiberarLote.TabIndex = 22;
            this.cmdLiberarLote.Text = "Liberar Lote";
            this.cmdLiberarLote.UseVisualStyleBackColor = true;
            this.cmdLiberarLote.Click += new System.EventHandler(this.cmdLiberarLote_Click_1);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.LightGray;
            this.groupBox3.Controls.Add(this.cmdLiberarLote);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.cbPredio);
            this.groupBox3.Controls.Add(this.cbLotes);
            this.groupBox3.Controls.Add(this.cbManzana);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(10, 266);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(896, 72);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Cancelacion";
            // 
            // frmConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 347);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtIdCliente);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbClientes);
            this.Name = "frmConsulta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultas";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProximoPago)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosVenta)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbClientes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIdCliente;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button cmdModificarPago;
        private System.Windows.Forms.DataGridView dgvProximoPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowMensualidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowPagoActual;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowPagoFinal;
        private System.Windows.Forms.DataGridViewComboBoxColumn rowcbStatusMora;
        private System.Windows.Forms.Label lProximoPago;
        private System.Windows.Forms.DateTimePicker dtpProximoPago;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button cmdTraspasar;
        private System.Windows.Forms.ComboBox cbClienteTraspaso;
        private System.Windows.Forms.Label lFechaCompra;
        private System.Windows.Forms.DateTimePicker dtpFechaCompra;
        private System.Windows.Forms.Label lNumeroLote;
        private System.Windows.Forms.ComboBox cbLotesCargaInfo;
        private System.Windows.Forms.DataGridView dgvDatosVenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn predioDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn manzanaDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn loteDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn montoDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn mensualidadDGV;
        private System.Windows.Forms.Button cmdModificarVenta;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbManzana;
        private System.Windows.Forms.ComboBox cbLotes;
        private System.Windows.Forms.ComboBox cbPredio;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button cmdLiberarLote;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbVendedores;
    }
}