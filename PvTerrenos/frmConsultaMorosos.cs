﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvTerrenos
{
    public partial class FrmConsultaMorosos : Form
    {
        WSpvt.PVT ws = new WSpvt.PVT();

        public FrmConsultaMorosos()
        {
            InitializeComponent();

            //DateTime fecha = new DateTime(2017, 11, 06);

            //string fecha = DateTime() .ToShortDateString();

            //cargarTabla(fecha.ToShortDateString());
        }

        private void cmdRealizarConsulta_Click(object sender, EventArgs e)
        {

            string fecha = dtpFechaMorosos.Value.AddMonths(-1).ToShortDateString();
           
            cargarTabla(fecha);
        }

        public void cargarTabla(string fecha)
        {
            dgvMorosos.Rows.Clear();

            string respuestaConsulta = "";

            try
            {
                respuestaConsulta = ws.getMorosos(fecha);
            }
            catch (Exception) { }

            if (respuestaConsulta != "0" && respuestaConsulta != "")
            {
                string[] splitDatos = respuestaConsulta.Split(new char[] { '|' });

                foreach (string datosInsertar in splitDatos)
                {
                    string[] splitDatosPago = datosInsertar.Split(new char[] { ',' });

                    string comprador = splitDatosPago[0];
                    string fechaCompra = splitDatosPago[1];
                    string ultimoPago = splitDatosPago[2];
                    string fechaUltimoPago = splitDatosPago[3];
                    string predio = splitDatosPago[4];
                    string manzana = splitDatosPago[5];
                    string lote = splitDatosPago[6];
                    string telefono1 = splitDatosPago[7];
                    string telefono2 = splitDatosPago[8];

                    //string[] sinSimbolo = splitDatosPago[1].Split(new char[] { '$' });
                    //double montoPagoMora = Convert.ToDouble(sinSimbolo[1].Trim());
                    //double montoPagoMora = Convert.ToDouble(splitDatosPago[1].Trim());
                    //double montoAbonoMora = Convert.ToDouble(splitDatosPago[2].Trim());
                    //double montoPagoMes = Convert.ToDouble(splitDatosPago[3].Trim());
                    //double montoAbonoMes = Convert.ToDouble(splitDatosPago[4].Trim());

                    //double total = montoAbonoMora + montoAbonoMes + montoPagoMes + montoPagoMora;

                    //string totalPagado = string.Format("{0:N2}", total);

                    //string numeroLote = splitDatosPago[5];
                    //string numeroManzana = splitDatosPago[6];
                    //string fechaProximoPago = Convert.ToDateTime(splitDatosPago[7]).ToString("MMMM - yyyy");
                    //string fechaPago = Convert.ToDateTime(splitDatosPago[8]).ToString("dd/MM/yy hh:mm:ss");
                    //string nombrePredio = splitDatosPago[9];

                    dgvMorosos.Rows.Insert(0, comprador, fechaCompra, ultimoPago, fechaUltimoPago, predio, manzana, lote, telefono1, telefono2, "", "", "");
                }  
                lRegistros.Text = "Registros encontrados -> " + Convert.ToString(dgvMorosos.Rows.Count -1);
            }
            else 
            {
                MessageBox.Show("No se encontraron resultados");
            }
            //sumarTotalPagado();
        }
    }
}
