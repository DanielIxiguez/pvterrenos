﻿namespace PvTerrenos
{
    partial class FrmMedidaLote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnMedidas = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMedidaNorte = new System.Windows.Forms.TextBox();
            this.txtMedidaSur = new System.Windows.Forms.TextBox();
            this.txtMedidaEste = new System.Windows.Forms.TextBox();
            this.txtMedidaOeste = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Norte";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(177, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Sur";
            // 
            // btnMedidas
            // 
            this.btnMedidas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMedidas.Location = new System.Drawing.Point(23, 176);
            this.btnMedidas.Name = "btnMedidas";
            this.btnMedidas.Size = new System.Drawing.Size(258, 46);
            this.btnMedidas.TabIndex = 5;
            this.btnMedidas.Text = "Aceptar";
            this.btnMedidas.UseVisualStyleBackColor = true;
            this.btnMedidas.Click += new System.EventHandler(this.btnMedidas_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(177, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Oeste";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(21, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "Este";
            // 
            // txtMedidaNorte
            // 
            this.txtMedidaNorte.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedidaNorte.Location = new System.Drawing.Point(23, 35);
            this.txtMedidaNorte.Name = "txtMedidaNorte";
            this.txtMedidaNorte.Size = new System.Drawing.Size(100, 31);
            this.txtMedidaNorte.TabIndex = 1;
            // 
            // txtMedidaSur
            // 
            this.txtMedidaSur.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedidaSur.Location = new System.Drawing.Point(181, 35);
            this.txtMedidaSur.Name = "txtMedidaSur";
            this.txtMedidaSur.Size = new System.Drawing.Size(100, 31);
            this.txtMedidaSur.TabIndex = 2;
            // 
            // txtMedidaEste
            // 
            this.txtMedidaEste.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedidaEste.Location = new System.Drawing.Point(23, 123);
            this.txtMedidaEste.Name = "txtMedidaEste";
            this.txtMedidaEste.Size = new System.Drawing.Size(100, 31);
            this.txtMedidaEste.TabIndex = 3;
            // 
            // txtMedidaOeste
            // 
            this.txtMedidaOeste.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedidaOeste.Location = new System.Drawing.Point(181, 123);
            this.txtMedidaOeste.Name = "txtMedidaOeste";
            this.txtMedidaOeste.Size = new System.Drawing.Size(100, 31);
            this.txtMedidaOeste.TabIndex = 4;
            // 
            // FrmMedidaLote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 239);
            this.Controls.Add(this.txtMedidaOeste);
            this.Controls.Add(this.txtMedidaEste);
            this.Controls.Add(this.txtMedidaSur);
            this.Controls.Add(this.txtMedidaNorte);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnMedidas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmMedidaLote";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Medidas Lote";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnMedidas;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMedidaNorte;
        private System.Windows.Forms.TextBox txtMedidaSur;
        private System.Windows.Forms.TextBox txtMedidaEste;
        private System.Windows.Forms.TextBox txtMedidaOeste;
    }
}