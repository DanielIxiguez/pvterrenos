﻿namespace PvTerrenos
{
    partial class FrmDireccion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDireccion));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ClientesActivos = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.dGVClientesActivos = new System.Windows.Forms.DataGridView();
            this.nombreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusventaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccionClientesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dGVEscrituras = new System.Windows.Forms.DataGridView();
            this.nombreDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombrepredioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nmanzanaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nloteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusventaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccionEscriturasBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.direccionEscriturasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.direccionClientesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGVClientesActivos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.direccionClientesBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGVEscrituras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.direccionEscriturasBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.direccionEscriturasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.direccionClientesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ClientesActivos,
            this.toolStripButton1,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1702, 91);
            this.toolStrip1.TabIndex = 52;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // ClientesActivos
            // 
            this.ClientesActivos.Image = ((System.Drawing.Image)(resources.GetObject("ClientesActivos.Image")));
            this.ClientesActivos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ClientesActivos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ClientesActivos.Name = "ClientesActivos";
            this.ClientesActivos.Size = new System.Drawing.Size(117, 88);
            this.ClientesActivos.Text = "Clientes Activos";
            this.ClientesActivos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ClientesActivos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ClientesActivos.Click += new System.EventHandler(this.ClientesActivos_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(68, 88);
            this.toolStripButton1.Text = "Predios";
            this.toolStripButton1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(186, 88);
            this.toolStripButton2.Text = "Escriturados-por-pagados";
            this.toolStripButton2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // dGVClientesActivos
            // 
            this.dGVClientesActivos.AutoGenerateColumns = false;
            this.dGVClientesActivos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVClientesActivos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nombreDataGridViewTextBoxColumn,
            this.statusventaDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn1});
            this.dGVClientesActivos.DataSource = this.direccionClientesBindingSource1;
            this.dGVClientesActivos.Location = new System.Drawing.Point(12, 111);
            this.dGVClientesActivos.Name = "dGVClientesActivos";
            this.dGVClientesActivos.RowTemplate.Height = 24;
            this.dGVClientesActivos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dGVClientesActivos.Size = new System.Drawing.Size(1396, 704);
            this.dGVClientesActivos.TabIndex = 53;
            // 
            // nombreDataGridViewTextBoxColumn
            // 
            this.nombreDataGridViewTextBoxColumn.DataPropertyName = "nombre";
            this.nombreDataGridViewTextBoxColumn.HeaderText = "nombre";
            this.nombreDataGridViewTextBoxColumn.Name = "nombreDataGridViewTextBoxColumn";
            this.nombreDataGridViewTextBoxColumn.Width = 600;
            // 
            // statusventaDataGridViewTextBoxColumn
            // 
            this.statusventaDataGridViewTextBoxColumn.DataPropertyName = "status_venta";
            this.statusventaDataGridViewTextBoxColumn.HeaderText = "status_venta";
            this.statusventaDataGridViewTextBoxColumn.Name = "statusventaDataGridViewTextBoxColumn";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "fecha_compra";
            this.dataGridViewTextBoxColumn1.HeaderText = "fecha_compra";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 200;
            // 
            // direccionClientesBindingSource1
            // 
            this.direccionClientesBindingSource1.DataSource = typeof(PvTerrenos.DireccionClientes);
            // 
            // dGVEscrituras
            // 
            this.dGVEscrituras.AutoGenerateColumns = false;
            this.dGVEscrituras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVEscrituras.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nombreDataGridViewTextBoxColumn1,
            this.nombrepredioDataGridViewTextBoxColumn,
            this.nmanzanaDataGridViewTextBoxColumn,
            this.nloteDataGridViewTextBoxColumn,
            this.statusventaDataGridViewTextBoxColumn1});
            this.dGVEscrituras.DataSource = this.direccionEscriturasBindingSource1;
            this.dGVEscrituras.Location = new System.Drawing.Point(12, 111);
            this.dGVEscrituras.Name = "dGVEscrituras";
            this.dGVEscrituras.RowTemplate.Height = 24;
            this.dGVEscrituras.Size = new System.Drawing.Size(1396, 704);
            this.dGVEscrituras.TabIndex = 54;
            this.dGVEscrituras.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dGVEscrituras_ColumnHeaderMouseClick);
            // 
            // nombreDataGridViewTextBoxColumn1
            // 
            this.nombreDataGridViewTextBoxColumn1.DataPropertyName = "nombre";
            this.nombreDataGridViewTextBoxColumn1.HeaderText = "nombre";
            this.nombreDataGridViewTextBoxColumn1.Name = "nombreDataGridViewTextBoxColumn1";
            this.nombreDataGridViewTextBoxColumn1.Width = 400;
            // 
            // nombrepredioDataGridViewTextBoxColumn
            // 
            this.nombrepredioDataGridViewTextBoxColumn.DataPropertyName = "nombre_predio";
            this.nombrepredioDataGridViewTextBoxColumn.HeaderText = "nombre_predio";
            this.nombrepredioDataGridViewTextBoxColumn.Name = "nombrepredioDataGridViewTextBoxColumn";
            this.nombrepredioDataGridViewTextBoxColumn.Width = 200;
            // 
            // nmanzanaDataGridViewTextBoxColumn
            // 
            this.nmanzanaDataGridViewTextBoxColumn.DataPropertyName = "n_manzana";
            this.nmanzanaDataGridViewTextBoxColumn.HeaderText = "n_manzana";
            this.nmanzanaDataGridViewTextBoxColumn.Name = "nmanzanaDataGridViewTextBoxColumn";
            // 
            // nloteDataGridViewTextBoxColumn
            // 
            this.nloteDataGridViewTextBoxColumn.DataPropertyName = "n_lote";
            this.nloteDataGridViewTextBoxColumn.HeaderText = "n_lote";
            this.nloteDataGridViewTextBoxColumn.Name = "nloteDataGridViewTextBoxColumn";
            // 
            // statusventaDataGridViewTextBoxColumn1
            // 
            this.statusventaDataGridViewTextBoxColumn1.DataPropertyName = "status_venta";
            this.statusventaDataGridViewTextBoxColumn1.HeaderText = "status_venta";
            this.statusventaDataGridViewTextBoxColumn1.Name = "statusventaDataGridViewTextBoxColumn1";
            this.statusventaDataGridViewTextBoxColumn1.Width = 200;
            // 
            // direccionEscriturasBindingSource1
            // 
            this.direccionEscriturasBindingSource1.DataSource = typeof(PvTerrenos.DireccionEscrituras);
            // 
            // direccionEscriturasBindingSource
            // 
            this.direccionEscriturasBindingSource.DataSource = typeof(PvTerrenos.DireccionEscrituras);
            // 
            // direccionClientesBindingSource
            // 
            this.direccionClientesBindingSource.DataSource = typeof(PvTerrenos.DireccionClientes);
            // 
            // FrmDireccion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1702, 864);
            this.Controls.Add(this.dGVEscrituras);
            this.Controls.Add(this.dGVClientesActivos);
            this.Controls.Add(this.toolStrip1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "FrmDireccion";
            this.Text = "FrmDireccion";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGVClientesActivos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.direccionClientesBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGVEscrituras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.direccionEscriturasBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.direccionEscriturasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.direccionClientesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ClientesActivos;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreClinteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusClienteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dGVClientesActivos;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaCompraDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource direccionClientesBindingSource;
        private System.Windows.Forms.BindingSource direccionClientesBindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusventaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.DataGridView dGVEscrituras;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreporedioDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource direccionEscriturasBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombrepredioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nmanzanaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nloteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusventaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource direccionEscriturasBindingSource1;
    }
}