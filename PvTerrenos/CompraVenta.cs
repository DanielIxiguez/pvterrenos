﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace PvTerrenos
{
    public class CompraVenta : CRUD
    {
        public int id_venta { get; set; }
        private string mensualidad;
        //public string mensualidad { get; set; }
        public string fecha_corte { get; set; }
        public string status_venta { get; set; }
        public string fecha_compra { get; set; }
        public string monto { get; set; }//por implementacion se borrara
        public string pk_comprador { get; set; }
        public int pk_lote { get; set; }
        //public string last_id_compra_venta { get; set; }
        public int fk_user { get; set; }
        public string fecha_pago_comision { get; set; }

        public List<CompraVenta> _listaCompraVentaCliente { get; set; }
        public List<CompraVenta> _list;

        string table = "";

        public string Mensualidad
        {
            get { return string.Format("{0:N2}", Convert.ToDecimal(mensualidad)); }
            set { mensualidad = value; }
        }

        public CompraVenta()
        {
            table = COMPRAVENTA;

        }

        #region METODOS
        public void insert()
        {
            execute(table, values(), action.insert.ToString(), "");
        }

        public void query(string valuesString, string where)
        {
            execute(table, $"{valuesString}", action.query.ToString(), where);
        }

        public void update(string where)
        {
            execute(table, values(), action.update.ToString(), where);
        }

        public int idCompraVenta(int pk_comprador)
        {
            return list<CompraVenta>().Find(x => x.id_venta == pk_comprador).id_venta;
        }

        public void cancelarCompraVenta()
        {
            update($"WHERE {id_venta_a} {igual} {this.id_venta}");
        }

        public int lastIdCompraVenta()
        {
            return _list.Last().id_venta;
        }

        public void insertLastId()
        {
            int index = _list.Count() - 2;
            
            int penultimoId = _list[index].id_venta;

            int ultimoId = penultimoId + 1;

            _list.Last().id_venta = ultimoId;
        } 

        /// <summary>
        /// Devuelve una lista con todas las compras que tiene un cliente
        /// </summary>
        /// <param name="id_comprador"></param>
        public void loadListComprasClientes(string id_comprador)
        {
            _listaCompraVentaCliente = new List<CompraVenta>();

            _listaCompraVentaCliente = _list.FindAll
                (
                    x => x.pk_comprador == id_comprador /*& x.status_venta == "ACTIVO"*/
                );
        }

        public void loadList()
        {
            _list = list<CompraVenta>();
        }

        public bool statusLiberarLote()
        {
            switch (status_venta)
            {
                case "CANCELADO":
                    return true;
                case "REESTRUCTURADO":
                    return true;
                case "TRASPASO":
                    return true;
                case "ACTIVO":
                    return false;
                default:
                    break;
            }
            return true;
        }
        #endregion

        #region METODOS INTERFACE
        public void loadComboBox(ComboBox cb)
        {
            query("1", $"{WHERE} ?");

            foreach (CompraVenta compra_venta in list<CompraVenta>())
            {
                cb.Items.Add(compra_venta.id_venta);
            }
        }
        #endregion

        #region VALUES
        public string values()
        {
            return
            $"{mensualidad.ToUpper().Trim()}{cc}" +
            $"{fecha_corte.ToUpper().Trim()}{cc}" +
            $"{status_venta.ToUpper().Trim()}{cc}" +
            $"{fecha_compra.ToUpper().Trim()}{cc}" +
            $"{monto.ToUpper().Trim()}{cc}" +
            $"{pk_comprador.ToUpper().Trim()}{cc}" +
            $"{pk_lote}{cc}" +
            $"{fk_user}{cc}" +
            $"{fecha_pago_comision}";
        }
        #endregion
    }
}