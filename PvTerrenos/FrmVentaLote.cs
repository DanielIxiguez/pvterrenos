﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvTerrenos
{
    public partial class FrmVentaLote : Form
    {
        #region VARIABLES
        int _indexLote = -1;
        int _indexCompraVenta = -1;
        int _indexLoteAnterior = -1;
        int index = -1;

        bool entra = true;
        bool _cargarFormularioAlCambiarLote = true;
        bool BanderaVideo = false;
        Comision comision;
        GestionDGV dgv;
        ProximoPago proximo_pago;

        User user = new User();
        Lote lote = new Lote();
        Predio predio = new Predio();
        Manzana manzana = new Manzana();
        Interes interes = new Interes();
        Recibo recibo = new Recibo();
        Comprador comprador = new Comprador();
        DetallePago detalle_pago = new DetallePago();
        CompraVenta compra_venta = new CompraVenta();
        CompraVenta _loadCompraVenta = new CompraVenta();
        #endregion

        #region CONSTRUCTOR
        public FrmVentaLote(User user)
        {
            InitializeComponent();

            this.user = user;

            predio.query("1", predio.queryAll("", false, false));
            lote.query("1", lote.queryAll(lote.n_lote_a, true, true));
            user.query("1", user.queryAll(user.id_user_a, true, true));
            manzana.query("1", manzana.queryAll(manzana.n_manzana_a, true, true));
            compra_venta.query("1", compra_venta.whereVentasActivas);
            comprador.query("1", comprador.queryAll(comprador.id_comprador_a, true, true));

            user.loadList();
            compra_venta.loadList();
            comprador.inicializarlistaId();
            comprador.loadList();

            manzana.loadLlist();
            predio.loadList();
            lote.loadList();

            predio.loadComboBox(cbPredio);
            user.loadComboBox(cbVendedores, true);
            comprador.loadComboBox(cbNombre);

            mostrarBotonesInicio();

            String Actualizacion = compra_venta.VERCIONACTUALIZACION;
            this.Text = $"Compra venta {Actualizacion.ToUpper()} - {this.user.nombre.ToUpper()}";
        }
        #endregion

        #region ENVENTOS
        #region CLICK
        //AGREGAR VENTA
        private void tsBtnAgregarVenta_Click(object sender, EventArgs e)
        {
            if (!ValidateForm.empty(this))
            {
                if (tsBtnGenerarVenta.Text != "Actualizar")
                {
                    if (Mensaje.responseMessage(Mensaje.list_MessageBox[0], "venta", false) == DialogResult.OK)
                    {

                        //inicializa variables para la instancia compra_venta
                        //string idComprador = comprador.idComprador(cbNombre.Text);

                        inicializarInsertarCompraVenta();

                        compra_venta._list.Last().insert();

                        compra_venta.insertLastId();

                        if (Mensaje.result_bool)
                        {
                            lote._list[_indexLote].status_lote = "1";

                            lote._list[_indexLote].update
                                (
                                    lote._list[_indexLote].queryUpdate
                                    (
                                        lote._list[_indexLote].id_lote_a,
                                        lote._list[_indexLote].id_lote.ToString()
                                    )
                                );

                            if (Mensaje.result_bool)
                            {
                                compra_venta.lastIdCompraVenta();

                                inicializarProximoPago();

                                proximo_pago.insert();

                                if (Mensaje.result_bool)
                                {
                                    MessageBox.Show(Mensaje.result);

                                    _cargarFormularioAlCambiarLote = true;

                                    //reloadCliente();

                                    //cargarFormulario(cbLotes.Items.Count -1);
                                    //Clear.clearForm(this);
                                }
                                /*compra_venta.lastIdCompraVenta();

                                inicializarComision();

                                comision.insert();
                                  
                                if (Mensaje.result_bool)
                                {
                                    MessageBox.Show(Mensaje.result);
                                    Clear.clearForm(this);
                                }*/
                            }
                            /*if (Mensaje.result_bool)
                            {
                                compra_venta.lastIdCompraVenta();

                                inicializarProximoPago();

                                proximo_pago.insert();

                                if (Mensaje.result_bool)
                                {
                                    inicializarComision();

                                    comision.insert();

                                    if (Mensaje.result_bool)
                                    {
                                        MessageBox.Show(Mensaje.result);
                                        Clear.clearForm(this);
                                    }
                                }
                            }*/
                        }
                    }
                }
                else
                {
                    inicializarActualizarCompraVenta();

                    compra_venta._list[_indexCompraVenta].update
                        (
                            $"WHERE `id_venta` = {compra_venta._list[_indexCompraVenta].id_venta}"
                        );

                    if (Mensaje.result_bool)
                    {
                        _cargarFormularioAlCambiarLote = true;

                        //Libera el lote para su posterior venta
                        //si el status del lote cambio a CANCELADO, TRASPASO
                        if (compra_venta._list[_indexCompraVenta].statusLiberarLote())
                        {
                            lote.status(_indexLote, "0");
                            /*lote._list[_indexLote].status_lote = "0";

                            lote._list[_indexLote].update
                                (
                                    lote._list[_indexLote].queryUpdate
                                    (
                                        lote._list[_indexLote].id_lote_a, 
                                        lote._list[_indexLote].id_lote.ToString()
                                    )
                                );*/
                        }

                        //Libera cuando el comprador cambia de lote
                        if (lote.cambioDeLote(_indexLoteAnterior, _indexLote))
                        {
                            lote.status(_indexLoteAnterior, "0");
                            lote.status(_indexLote, "1");
                            /*lote._list[_indexLoteAnterior].status_lote = "0";

                            lote._list[_indexLoteAnterior].update
                                (
                                    lote._list[_indexLoteAnterior].queryUpdate
                                    (
                                        lote._list[_indexLoteAnterior].id_lote_a,
                                        lote._list[_indexLoteAnterior].id_lote.ToString()
                                    )
                                );*/
                        }
                    }

                    MessageBox.Show(Mensaje.result);
                }
            }
            else
            {
                MessageBox.Show(Mensaje.result);
            }
        }

        //LIMPIAR FORMULARIO
        private void tsBtnLimpiar_Click(object sender, EventArgs e)
        {
            Clear.clearForm(this);
            ocultarBotones();
            //ocultarFormularioPago();
            //dtpProximoPago.Value = DateTime.Today;
            tsBtnGenerarVenta.Text = "Agregar venta";
            cbLotes.Items.Clear();
            _cargarFormularioAlCambiarLote = true;
            inicializarEnCero();
            dgvHistorial.Rows.Clear();
        }

        //inicializa los campos del formulario de pago en cero
        //cuando no se usan estos campos, pase la validacion de isEmpty
        //que evalua que todos los campos del formulario no esten vacios
        public void inicializarEnCero()
        {
            txtInteres.Text = "0";
            txtTotal.Text = "0";
            txtMonto.Text = "0";
            txtPagoActual.Text = "0";
            cbEstatus.Text = "_";
        }

        //Si se paga el total carga el txtMonto con el total del pago.
        private void lMonto_Click(object sender, EventArgs e)
        {
            txtMonto.Text = txtTotal.Text;
        }
        #endregion

        #region SELECTED_INDEX
        //CB_NOMBRE
        private void cbNombre_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbNombre.SelectedIndex != -1)
            {
                compra_venta.loadListComprasClientes
                    (
                        comprador.idComprador(cbNombre.SelectedIndex)
                    );

                if (compra_venta._listaCompraVentaCliente.Count > 0)
                {
                    cbLotes.Items.Clear();
                    //_cargarFormularioAlCambiarLote = false;
                    foreach (CompraVenta cv in compra_venta._listaCompraVentaCliente)
                    {
                        cbLotes.Items.Add(lote.numeroLote(cv.pk_lote));
                    }
                }
            }
        }

        //CB_PREDIO
        private void cbPredio_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra)
            {
                cbManzana.Items.Clear();
                _cargarFormularioAlCambiarLote = false;
                manzana.loadComboBox(cbManzana, predio.idPredio(cbPredio.Text));

                if (cbManzana.Items.Count == 0)
                {
                    lote.loadComboBox(cbLotes, predio.idPredio(cbPredio.Text), "0");
                }
            }
        }

        //CB_MANZANA
        private void cbManzana_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra)
            {
                _cargarFormularioAlCambiarLote = false;
                //_indexLoteAnterior = lote._list[_indexLote].id_lote;

                cbLotes.Items.Clear();

                string id_predio = predio.idPredio(cbPredio.Text);
                string id_manzana = manzana.idManzana(cbManzana.Text, id_predio);

                lote.loadComboBox(cbLotes, id_predio, id_manzana);
            }
        }

        /*El comboBox tiene 2 funciones
          1 Nueva compra venta
          2 Cargar compra venta*/
        private void cbLotes_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbLotes.DropDownStyle = ComboBoxStyle.DropDownList;
            /*1 Cuando se va a realizar una compra venta y se 
            selecciona el lote abre una ventana para 
            capturar las medidas del lote*/
            if (cbPredio.Text != "" & tsBtnGenerarVenta.Text != "Actualizar") // si cbpredio tiene algo y y boton es actualizar/generar no dice actualizar
            {
                string id_predio = predio.idPredio(cbPredio.Text);
                string id_manzana = manzana.idManzana(cbManzana.Text, id_predio);
                string n_lote = cbLotes.Text;

                //devuelve la posicion del objeto en la coleccion
                _indexLote = lote.index(id_predio, id_manzana, n_lote);

                FrmMedidaLote medida_lote = new FrmMedidaLote(ref lote, _indexLote);
                medida_lote.Show();
            }
            /*2 Cargar compra venta Cargar la informacion de un lote de una
            compra venta realizada*/
            else
            {
                index = cbLotes.SelectedIndex;

                if (index >= 0 & cbLotes.Text != "")
                {
                    /*La interface cambia para getionar cambios
                      de la compra venta*/
                    tsBtnGenerarVenta.Text = "Actualizar";
                    mostrarBotonesGestionVenta();
                    entra = false;
                    dgvHistorial.Rows.Clear();

                    /*Esta validacion sirve para saber si se va a cargar
                      los datos del formulario o se realiza un cambio de 
                      lote en el formulario de la compraventa*/
                    if (_cargarFormularioAlCambiarLote)
                    {
                        cargarFormulario(index);
                    }
                    else
                    {
                        _indexLoteAnterior = _indexLote;
                        string id_predio = predio.idPredio(cbPredio.Text);
                        string id_manzana = manzana.idManzana(cbManzana.Text, id_predio);
                        string n_lote = cbLotes.Text;
                        //devuelve la posicion del objeto en la coleccion
                        _indexLote = lote.index(id_predio, id_manzana, n_lote);
                    }
                    entra = true;
                }
            }
        }

        #endregion

        #region FRM_CLOSING
        private void FrmVentaLote_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        #endregion
        #endregion

        #region METODOS
        #region INICIALIZAR INSTANCIAS
        //COMPRA VENTA
        private void inicializarInsertarCompraVenta()
        {

            compra_venta._list.Add
                (
                    new CompraVenta()
                    {
                        Mensualidad = txtMensualidad.Text,
                        fecha_corte = dtpFechaVenta.Value.Day.ToString(),
                        status_venta = "ACTIVO",
                        fecha_compra = dtpFechaVenta.Value.ToString(compra_venta.DATE_CAST),
                        monto = "0",
                        pk_comprador = comprador.idComprador(cbNombre.SelectedIndex),//cbNombre.Text);
                        pk_lote = lote._list[_indexLote].id_lote,
                        fk_user = user.idUser(cbVendedores.Text.ToLower()),
                        fecha_pago_comision = ""
                    }
                );
        }

        private void inicializarActualizarCompraVenta()
        {

            compra_venta._list[_indexCompraVenta].Mensualidad = string.Format("{0:f0}", Convert.ToDecimal(txtMensualidad.Text));
            compra_venta._list[_indexCompraVenta].fecha_corte = dtpFechaVenta.Value.Day.ToString();
            compra_venta._list[_indexCompraVenta].status_venta = cbEstatus.Text;
            compra_venta._list[_indexCompraVenta].fecha_compra = dtpFechaVenta.Value.ToString(compra_venta.DATE_CAST);
            compra_venta._list[_indexCompraVenta].monto = "";
            compra_venta._list[_indexCompraVenta].pk_comprador = comprador.idComprador(cbNombre.SelectedIndex);//cbNombre.Text);
            compra_venta._list[_indexCompraVenta].pk_lote = lote._list[_indexLote].id_lote;//lote.id_lote;
            compra_venta._list[_indexCompraVenta].fk_user = user.idUser(cbVendedores.Text.ToLower());
            //compra_venta._list[_indexCompraVenta].fecha_pago_comision = "";
        }

        //PROXIMO PAGO
        private void inicializarProximoPago()
        {
            proximo_pago = new ProximoPago()
            {
                monto = txtMensualidad.Text,
                proximo_pago = dtpFechaVenta.Value.ToString(compra_venta.DATE_CAST),// proximoPago.Value.ToString(),
                pago_actual = "1",
                pago_final = "100",
                status_mora = "0",
                pk_venta = compra_venta.lastIdCompraVenta()
            };
        }

        //DETALE PAGO
        private void inicializarDetallePago()
        {
            detalle_pago.idVenta = compra_venta._list[_indexCompraVenta].id_venta;
            detalle_pago.pagoActual = txtPagoActual.Text;
            detalle_pago.mensualidad = string.Format("{0:f0}", Convert.ToDecimal(txtMonto.Text));
            detalle_pago.fechaProximoPago = dtpProximoPago.Value.ToString(detalle_pago.DATE_CAST);
            detalle_pago.fechaPago = DateTime.Now.ToString(detalle_pago.DATE_CAST);
            detalle_pago.administracion = predio.nombreAdministracion(lote._list[_indexLote].pk_predio);
            detalle_pago.monto = "";//txtMonto.Text;
            //detalle_pago.tipo_pago = se carga antes de la inicializacion de pago;
        }

        public void reloadCliente()
        {
            int index = cbNombre.SelectedIndex;
            cbNombre.SelectedIndex = index;
        }
        //COMOSION
        /*private void inicializarComision()
        {
            comision = new Comision()
            {
                status = "0",
                fecha_pago = "",
                fk_user = user.idUser(cbVendedores.Text),// user.idUser(cbVendedores.Text.ToLower()),
                fk_compra_venta = compra_venta._list.Last().id_venta
            }; 
        }*/
        #endregion

        #region CARGAR FORMULARIO        
        public void cargarFormulario(int index)
        {
            if (cargarCompraVenta(index))
            {
                cargarPredio();

                /*detalle_pago.amortizacion
                (
                    Convert.ToDateTime
                    (
                        compra_venta._list[_indexCompraVenta].fecha_compra
                    )
                );*/

                /*if (cargarDetallePago())
                {
                    cargarDgvHistorial();

                    cargarInteres();

                    cargarTotal();
                }
                else
                {
                    mostarFormularioPago(true);

                     cargarInteres();
                    cargarTotal();

                    dtpProximoPago.Value = dtpFechaVenta.Value;
                    txtPagoActual.Text = "1";
                    Mensaje.responseMessage("Registro sin historial de pagos", "Detella de venta", false);
                }*/
            }
            else
            {
                MessageBox.Show("No se encontraron ventas para este cliente");
            }
        }

        //COMPRA VENTA
        public bool cargarCompraVenta(int index)
        {
            /*Recupera la poscición del objeto compra venta 
             * de una lista de id de clientes que coiciden 
             * con los indices del coltrol combobox nombre*/
            _indexCompraVenta = compra_venta._list.IndexOf
                (
                    compra_venta._listaCompraVentaCliente[index]
                );

            if (_indexCompraVenta >= 0)
            {
                //cargar formulario con los datos de compra venta
                cbEstatus.Text = compra_venta._list[_indexCompraVenta].status_venta;
                txtMensualidad.Text = compra_venta._list[_indexCompraVenta].Mensualidad;
                dtpFechaVenta.Value = Convert.ToDateTime(compra_venta._list[_indexCompraVenta].fecha_compra);
                cbVendedores.Text = user.nombreUsuario(compra_venta._list[_indexCompraVenta].fk_user);
                //**termina carga de datos de la instancia compra venta///

                return true;
            }
            return false;
        }

        //PREDIO
        public void cargarPredio()
        {
            //recuperar la posicion del objeto lote
            _indexLote = lote._list.IndexOf
                (
                    lote._list.Find
                    (
                        x => x.id_lote == compra_venta._list[_indexCompraVenta].pk_lote
                    )
                );

            //cargar formulario con los datos predio, manzana y lote de lote._list
            cbPredio.Text = predio.nombrePredio(lote._list[_indexLote].pk_predio);
            cbManzana.Text = manzana.numeroManzana(lote._list[_indexLote].pk_manzana);
            cbVendedores.Text = user.nombreUsuario(compra_venta._list[_indexCompraVenta].fk_user).ToUpper();
        }
        /* //DETALLE PAGO
         public bool cargarDetallePago()
         {
             //Consulta detalle pago de la compra venta
             detalle_pago.query
                 (
                     compra_venta._list[_indexCompraVenta].id_venta.ToString(),
                     $"WHERE `idVenta` = ? ORDER BY `idDetallePago` DESC"
                 );

             if (Mensaje.result_bool)
             {
                 detalle_pago.loadList();

                 //instancia del detalle de pago
                 //var _loadDetallePago = detalle_pago._list[0];
                 //cargar formulario con los datos del detalle de venta
                 txtPagoActual.Text = detalle_pago._numeroProximoPago.ToString();//numeroProximoPago(detalle_pago._listDetallePago).ToString();//_loadDetallePago.pagoActual;
                 dtpProximoPago.Value = Convert.ToDateTime(detalle_pago.fechaProximoPago);//fechaProximoPagoMetodo(detalle_pago.amortizacion(Convert.ToDateTime(compra_venta._list[_indexCompraVenta].fecha_compra)));//Convert.ToDateTime(_loadDetallePago.fechaPago);
                 dtpProximoPago.CustomFormat = "MMMM - yyyy";
                 return true;
             }

             dtpProximoPago.Value = dtpFechaVenta.Value;//Convert.ToDateTime(_loadDetallePago.fechaPago);
             dtpProximoPago.CustomFormat = "MMMM - yyyy";

             return false;
         }
         //HISTORIAL DATA GRID VIEW
         public void cargarDgvHistorial()
         {
             //carga el dataGridView con el historial del cliente
             dgv = new GestionDGV();

             dgv.loadDgvPagos(detalle_pago._listDetallePago, dgvHistorial);
             dgvHistorial.Columns["dgvId"].Visible = false;
             mostarFormularioPago(true);
         }
         //INTERES 
         public void cargarInteres()
         {
             if (
                     detalle_pago.pagoVencido
                     (
                         detalle_pago.numeroUltimoPago
                             (
                                 detalle_pago._listDetallePago
                             ),
                             detalle_pago.numeroPagoMesEnCurso
                             (
                                 detalle_pago.amortizacion
                                     (
                                         Convert.ToDateTime(compra_venta.fecha_compra)
                                     )
                             ),
                             detalle_pago.fechaPagoMesEnCurso
                                 (
                                     detalle_pago.amortizacion(Convert.ToDateTime(compra_venta.fecha_compra)), 
                                     detalle_pago.numeroPagoMesEnCurso(detalle_pago.amortizacion(Convert.ToDateTime(compra_venta.fecha_compra)))
                                 )
                     )
                 )
             {
                 txtInteres.Text = string.Format("{0:N2}", interes.mensual
                     (
                         detalle_pago._mensualidadMenosAbono,
                         0.06M,
                         detalle_pago.mesesVencidos
                         (
                             detalle_pago.numeroUltimoPago(detalle_pago._listDetallePago),
                             detalle_pago.numeroPagoMesEnCurso
                             (
                                 detalle_pago.amortizacion
                                 (
                                     Convert.ToDateTime(compra_venta.fecha_compra)
                                 )
                             ),
                             detalle_pago.mesActualVencido
                             (
                                 detalle_pago.fechaPagoMesEnCurso
                                     (
                                         detalle_pago.amortizacion(Convert.ToDateTime(compra_venta.fecha_compra)),
                                         detalle_pago.numeroPagoMesEnCurso
                                         (
                                             detalle_pago.amortizacion(Convert.ToDateTime(compra_venta.fecha_compra))
                                         )
                                     )
                              )
                          )
                     ));
             }
         }
         //TOTAL
         public void cargarTotal()
         {
             decimal interes = Convert.ToDecimal(txtInteres.Text);
             decimal mensualidad = Convert.ToDecimal(txtMensualidad.Text);
             decimal resultado = interes + mensualidad;

             txtTotal.Text = string.Format("{0:N2}", resultado); 
         }*/
        #endregion

        #region MODIFICAR PROPIEDADES
        //OCULTAR BOTONES VENTA
        public void ocultarBotones()
        {
            //btnCancelar.Visible = false;
            //btnTraspasar.Visible = false;
            //btnReestructurar.Visible = false;
        }

        //OCULTAR BOTONES VENTA
        public void mostrarBotonesInicio()
        {
            switch (user.tipo)
            {
                case NivelAcceso.CAJERO:
                    tsBtnPagar.Visible = true;
                    break;
                case NivelAcceso.ADMINISTRADOR:
                    tsBtnCliente.Visible = true;
                    tsBtnPredio.Visible = true;
                    tsBtnUsuario.Visible = true;
                    tsBtnConsulta.Visible = true;
                    tsBtnGenerarVenta.Visible = true;
                    toolStripSeparator1.Visible = true;
                    break;
                case NivelAcceso.ADMINISTRADORMM:
                    tsBtnCliente.Visible = true;
                    tsBtnPredio.Visible = true;
                    tsBtnUsuario.Visible = true;
                    tsBtnConsulta.Visible = true;
                    tsBtnGenerarVenta.Visible = true;
                    toolStripSeparator1.Visible = true;
                    break;
                case NivelAcceso.SUPERVISOR:
                    tsBtnCliente.Visible = true;
                    tsBtnPredio.Visible = true;
                    tsBtnUsuario.Visible = true;
                    tsBtnConsulta.Visible = true;
                    tsBtnGenerarVenta.Visible = true;
                    toolStripSeparator1.Visible = true;
                    break;
                case NivelAcceso.COBRANZA:
                    break;
                case NivelAcceso.SISTEMAS:
                    break;
                default:
                    break;
            }
            //btnCancelar.Visible = true;
            //btnTraspasar.Visible = true;
            //btnReestructurar.Visible = true;
        }

        public void mostrarBotonesGestionVenta()
        {
            switch (user.tipo)
            {
                case NivelAcceso.ADMINISTRADOR:
                    //tsBtnCancelar.Visible = true;
                    //tsBtnReestructurar.Visible = true;
                    tsBtnPagar.Visible = true;
                    break;
                case NivelAcceso.SUPERVISOR:
                    //tsBtnCancelar.Visible = true;
                    //tsBtnReestructurar.Visible = true;
                    tsBtnPagar.Visible = true;
                    break;
                case NivelAcceso.COBRANZA:
                    break;
                case NivelAcceso.SISTEMAS:
                    break;
                default:
                    break;
            }
        }

        //OCULTAR FORMULARIO MODIFICAR VENTA
        public void ocultarFormularioPago()
        {
            //formulario detalle de venta
            txtPagoActual.Visible = false;
            cbEstatus.Visible = false;
            dtpProximoPago.Visible = false;

            lNoPago.Visible = false;
            lEstatusVenta.Visible = false;
            lProximoPago.Visible = false;

            //formulario de pago
            lInteres.Visible = false;
            lTotal.Visible = false;
            lMonto.Visible = false;

            txtInteres.Visible = false;
            txtTotal.Visible = false;
            txtMonto.Visible = false;
        }

        //OCULTAR FORMULARIO MODIFICAR VENTA
        public void mostarFormularioPago(bool _soloFormularioPago)
        {
            if (_soloFormularioPago)
            {
                //Detalle venta
                txtPagoActual.Visible = true;
                cbEstatus.Visible = true;
                dtpProximoPago.Visible = true;

                lNoPago.Visible = true;
                lEstatusVenta.Visible = true;
                lProximoPago.Visible = true;

                //formulario pago
                lInteres.Visible = true;
                lTotal.Visible = true;
                lMonto.Visible = true;

                txtInteres.Visible = true;
                txtTotal.Visible = true;
                txtMonto.Visible = true;
            }
            else
            {
                //formulario pago
                lInteres.Visible = true;
                lTotal.Visible = true;
                lMonto.Visible = true;

                txtInteres.Visible = true;
                txtTotal.Visible = true;
                txtMonto.Visible = true;
            }
        }
        #endregion
        #endregion

        #region BOTONES TOOLSTRIP
        //CONSULTA
        private void pagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsulta consulta = new frmConsulta();
            consulta.llenaComboClientes(comprador);
            consulta.Show();
        }

        //CORTE_CAJA
        private void reporteDiarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCorteCaja corte_caja = new FrmCorteCaja(user.nombre, NivelAcceso.ADMINISTRADOR.ToString(), user.id_user, user.administracion);
            corte_caja.Show();
        }

        //MOROSOS
        private void atrasadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConsultaMorosos morosos = new FrmConsultaMorosos();
            morosos.Show();
        }

        //HISTORIAL DE PAGOS
        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmHistorial historial = new FrmHistorial();

            if (!historial.Created)
            {
                historial.Show();
            }
        }

        //COMISIONES
        private void comisionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmComision comision = new FrmComision(user, compra_venta);
            comision.Show();
        }

        //CLIENTE
        private void tsBtnCliente_Click(object sender, EventArgs e)
        {
            FrmComprador frm_comprador = new FrmComprador(ref comprador, ref cbNombre);
            frm_comprador.Show();
        }

        //PREDIO
        private void tsBtnPredio_Click(object sender, EventArgs e)
        {
            FrmAltaPredio predio = new FrmAltaPredio();
            predio.Show();
        }

        //USUARIO
        private void tsBtnUsuario_Click(object sender, EventArgs e)
        {
            FrmUsuario usuario = new FrmUsuario(ref user, ref cbVendedores);
            usuario.Show();
        }

        //PAGO
        private void tsBtnPago_Click(object sender, EventArgs e)
        {
            FrmPuntoVenta _puntoDeVenta = new FrmPuntoVenta(user);
            _puntoDeVenta.Show();
            //if (txtMonto.Text != "0" & txtMonto.Text != "")
            //{
            //    //List<DetallePago> _listDetallePago = new List<DetallePago>();

            //    //decimal restoMonto = Convert.ToDecimal(txtMonto.Text);

            //    //for (restoMonto = Convert.ToDecimal(txtMonto.Text); restoMonto > 0; )
            //    //{

            //    //    if(restoMonto >= Convert.ToDecimal(txtMensualidad.Text))
            //    //    {
            //    //        restoMonto -= Convert.ToDecimal(txtMensualidad.Text);
            //    //    }
            //    //    else
            //    //    {
            //    //        txtMensualidad.Text= Convert.ToString( Convert.ToDecimal(txtMensualidad.Text) - restoMonto);
            //    //    }
            //    //}

            //    detalle_pago.tipoPago(txtInteres.Text, txtMonto.Text, txtMensualidad.Text);
            //    inicializarDetallePago();
            //    detalle_pago.insert();



            //    if (Mensaje.result_bool)
            //    {
            //        Mensaje.responseMessage("Pago realizado", "Pago", true);
            //        inicializarEnCero();
            //        cargarFormulario(index);
            //        recibo.create(cbPredio.Text, cbManzana.Text, cbLotes.Text, cbNombre.Text, this.detalle_pago);
            //            //(
            //            //    txtPagoActual.Text,
            //            //    "100",
            //            //    cbLotes.Text,
            //            //    cbManzana.Text,
            //            //    cbPredio.Text,
            //            //    cbNombre.Text,
            //            //    txtMensualidad.Text,
            //            //    Convert.ToDouble(txtMonto.Text),
            //            //    0.0,
            //            //    dtpProximoPago.Value.ToString(),
            //            //    dtpProximoPago.Value.AddMonths(1).ToString(),
            //            //    borrar,
            //            //    borrarDouble,
            //            //    0,
            //            //    "ADMINISTRADOR"
            //            //);
            //    }
            //    else
            //    {
            //        Mensaje.responseMessage("", "Fallo el registro", true);
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("No se ha ingresado el monto");
            //}
        }

        //PAGO BORRAR
        private void tsBtnPago_Click_1(object sender, EventArgs e)
        {
            FrmPuntoVenta _puntoDeVenta = new FrmPuntoVenta(user);
            _puntoDeVenta.Show();
        }

        #endregion

        private void tsMenuReporteDiario_Click(object sender, EventArgs e)
        {
            FrmCorteCaja corte_caja = new FrmCorteCaja(user.nombre, user.tipo.ToString(), user.id_user, user.administracion);
            corte_caja.Show();
        }

        private void tsMenuAtrasados_Click(object sender, EventArgs e)
        {
            FrmConsultaMorosos morosos = new FrmConsultaMorosos();
            morosos.Show();
        }

        private void tsMenuHistorial_Click(object sender, EventArgs e)
        {
            FrmHistorial historial = new FrmHistorial();

            if (!historial.Created)
            {
                historial.Show();
            }
        }

        private void tsMenuComisiones_Click(object sender, EventArgs e)
        {
            FrmComision comision = new FrmComision(user, compra_venta);
            comision.Show();
        }

        private void direccionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (BanderaVideo == true)
            {


                FrmDireccion direccion = new FrmDireccion();
                direccion.Show();



            }
        }

        private void tsBtnConsulta_MouseHover(object sender, EventArgs e)
        {
            if (BanderaVideo == false)
            {

                DialogResult dr = MessageBox.Show("Nueva Actualizacion ,VER VIDEO", "Actualizacion", MessageBoxButtons.YesNo,
                MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    FrmPlayer video = new FrmPlayer();
                    video.Show();
                    BanderaVideo = true;
                }

                if (dr == DialogResult.No)
                {
                    BanderaVideo = true;
                }

            }
        }
    }
}
 