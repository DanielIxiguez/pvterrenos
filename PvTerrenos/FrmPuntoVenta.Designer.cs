﻿namespace PvTerrenos
{
    partial class FrmPuntoVenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPuntoVenta));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbConcepto = new System.Windows.Forms.Label();
            this.cmdBuscarCliente = new System.Windows.Forms.Button();
            this.txtPagoActual = new System.Windows.Forms.TextBox();
            this.txtManzana = new System.Windows.Forms.TextBox();
            this.txtPredio = new System.Windows.Forms.TextBox();
            this.cbConcepto = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbComprador = new System.Windows.Forms.ComboBox();
            this.txtProximoPago = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbLote = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDiaCorte = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpProximoPago = new System.Windows.Forms.DateTimePicker();
            this.txtPagoFinal = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkNoGeneraraInteres = new System.Windows.Forms.CheckBox();
            this.txtMonto = new System.Windows.Forms.TextBox();
            this.lMonto = new System.Windows.Forms.Label();
            this.txtTotalPagar = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMensualidadMesActual = new System.Windows.Forms.TextBox();
            this.lMensualidad = new System.Windows.Forms.Label();
            this.txtInteresMesActual = new System.Windows.Forms.TextBox();
            this.lTotalInteres = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbactual = new System.Windows.Forms.TextBox();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.cbMesesMora = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cmdPagoMensualidad = new System.Windows.Forms.Button();
            this.dgvPagos = new System.Windows.Forms.DataGridView();
            this.rowCmdEliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.rowIdDetallePago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowPagoActual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowMontoPagoMes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowMotoAbonoMes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowMontoPagoMora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowMontoAbonoMora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowTotalPagado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowMesPagado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowFechaPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gbDesdeHasta = new System.Windows.Forms.GroupBox();
            this.txtDesdeHastaTotal = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtDesdeHastaMensualidad = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtDesdeHastaInteres = new System.Windows.Forms.TextBox();
            this.gbMensual = new System.Windows.Forms.GroupBox();
            this.txtTotalMesSeleccionado = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtMensualidadMesSeleccionado = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtInteresMesSeleccionado = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.pbPagos = new System.Windows.Forms.ProgressBar();
            this.lCargar = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsBtnBuscarComprador = new System.Windows.Forms.ToolStripButton();
            this.tsCmdCorteCaja = new System.Windows.Forms.ToolStripButton();
            this.tsCmdReestructuracion = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsBtnPagar = new System.Windows.Forms.ToolStripButton();
            this.tsBtnConcepto = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Eliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.idpagoconceptoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaconceptopagoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidadtotalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidadpagarDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conceptoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultconceptospagosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.resultQueryCPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.resultQueryCPCorteCajaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPagos)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.gbDesdeHasta.SuspendLayout();
            this.gbMensual.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultconceptospagosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultQueryCPBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultQueryCPCorteCajaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbConcepto);
            this.groupBox1.Controls.Add(this.cmdBuscarCliente);
            this.groupBox1.Controls.Add(this.txtPagoActual);
            this.groupBox1.Controls.Add(this.txtManzana);
            this.groupBox1.Controls.Add(this.txtPredio);
            this.groupBox1.Controls.Add(this.cbConcepto);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.cbComprador);
            this.groupBox1.Controls.Add(this.txtProximoPago);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.cbLote);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtDiaCorte);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(11, 119);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(879, 263);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Comprador";
            // 
            // lbConcepto
            // 
            this.lbConcepto.AutoSize = true;
            this.lbConcepto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lbConcepto.Location = new System.Drawing.Point(549, 46);
            this.lbConcepto.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbConcepto.Name = "lbConcepto";
            this.lbConcepto.Size = new System.Drawing.Size(72, 17);
            this.lbConcepto.TabIndex = 54;
            this.lbConcepto.Text = "Concepto:";
            // 
            // cmdBuscarCliente
            // 
            this.cmdBuscarCliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdBuscarCliente.BackgroundImage")));
            this.cmdBuscarCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cmdBuscarCliente.Location = new System.Drawing.Point(467, 22);
            this.cmdBuscarCliente.Margin = new System.Windows.Forms.Padding(4);
            this.cmdBuscarCliente.Name = "cmdBuscarCliente";
            this.cmdBuscarCliente.Size = new System.Drawing.Size(39, 28);
            this.cmdBuscarCliente.TabIndex = 6;
            this.cmdBuscarCliente.UseVisualStyleBackColor = true;
            this.cmdBuscarCliente.Visible = false;
            this.cmdBuscarCliente.Click += new System.EventHandler(this.cmdBuscarCliente_Click);
            // 
            // txtPagoActual
            // 
            this.txtPagoActual.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtPagoActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPagoActual.Location = new System.Drawing.Point(12, 207);
            this.txtPagoActual.Margin = new System.Windows.Forms.Padding(4);
            this.txtPagoActual.Name = "txtPagoActual";
            this.txtPagoActual.ReadOnly = true;
            this.txtPagoActual.Size = new System.Drawing.Size(107, 37);
            this.txtPagoActual.TabIndex = 40;
            this.txtPagoActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtManzana
            // 
            this.txtManzana.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtManzana.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtManzana.Location = new System.Drawing.Point(381, 127);
            this.txtManzana.Margin = new System.Windows.Forms.Padding(4);
            this.txtManzana.Name = "txtManzana";
            this.txtManzana.ReadOnly = true;
            this.txtManzana.Size = new System.Drawing.Size(53, 34);
            this.txtManzana.TabIndex = 45;
            this.txtManzana.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPredio
            // 
            this.txtPredio.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtPredio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPredio.Location = new System.Drawing.Point(9, 127);
            this.txtPredio.Margin = new System.Windows.Forms.Padding(4);
            this.txtPredio.Name = "txtPredio";
            this.txtPredio.ReadOnly = true;
            this.txtPredio.Size = new System.Drawing.Size(363, 34);
            this.txtPredio.TabIndex = 45;
            // 
            // cbConcepto
            // 
            this.cbConcepto.DisplayMember = "Conceptos";
            this.cbConcepto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConcepto.FormattingEnabled = true;
            this.cbConcepto.Items.AddRange(new object[] {
            "Traspaso",
            "Subdivision",
            "Cambio de titular",
            "escrituracion"});
            this.cbConcepto.Location = new System.Drawing.Point(667, 42);
            this.cbConcepto.Margin = new System.Windows.Forms.Padding(4);
            this.cbConcepto.Name = "cbConcepto";
            this.cbConcepto.Size = new System.Drawing.Size(160, 21);
            this.cbConcepto.TabIndex = 53;
            this.cbConcepto.SelectedIndexChanged += new System.EventHandler(this.cbConcepto_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 185);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(99, 20);
            this.label14.TabIndex = 34;
            this.label14.Text = "Pago Actual";
            // 
            // cbComprador
            // 
            this.cbComprador.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbComprador.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbComprador.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbComprador.FormattingEnabled = true;
            this.cbComprador.Location = new System.Drawing.Point(8, 54);
            this.cbComprador.Margin = new System.Windows.Forms.Padding(4);
            this.cbComprador.Name = "cbComprador";
            this.cbComprador.Size = new System.Drawing.Size(496, 33);
            this.cbComprador.TabIndex = 46;
            this.cbComprador.SelectedIndexChanged += new System.EventHandler(this.cbComprador_SelectedIndexChanged);
            // 
            // txtProximoPago
            // 
            this.txtProximoPago.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtProximoPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProximoPago.Location = new System.Drawing.Point(283, 209);
            this.txtProximoPago.Margin = new System.Windows.Forms.Padding(4);
            this.txtProximoPago.Name = "txtProximoPago";
            this.txtProximoPago.ReadOnly = true;
            this.txtProximoPago.Size = new System.Drawing.Size(220, 30);
            this.txtProximoPago.TabIndex = 45;
            this.txtProximoPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(280, 185);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(201, 20);
            this.label12.TabIndex = 44;
            this.label12.Text = "Mes que se esta pagando";
            // 
            // cbLote
            // 
            this.cbLote.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLote.FormattingEnabled = true;
            this.cbLote.Location = new System.Drawing.Point(444, 127);
            this.cbLote.Margin = new System.Windows.Forms.Padding(4);
            this.cbLote.Name = "cbLote";
            this.cbLote.Size = new System.Drawing.Size(60, 33);
            this.cbLote.TabIndex = 33;
            this.cbLote.SelectedIndexChanged += new System.EventHandler(this.cbLote_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(377, 106);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 20);
            this.label13.TabIndex = 32;
            this.label13.Text = "M°";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 103);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 20);
            this.label8.TabIndex = 29;
            this.label8.Text = "Predio";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(440, 106);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "L°";
            // 
            // txtDiaCorte
            // 
            this.txtDiaCorte.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtDiaCorte.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaCorte.Location = new System.Drawing.Point(137, 207);
            this.txtDiaCorte.Margin = new System.Windows.Forms.Padding(4);
            this.txtDiaCorte.Name = "txtDiaCorte";
            this.txtDiaCorte.ReadOnly = true;
            this.txtDiaCorte.Size = new System.Drawing.Size(127, 37);
            this.txtDiaCorte.TabIndex = 39;
            this.txtDiaCorte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(137, 185);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 20);
            this.label11.TabIndex = 38;
            this.label11.Text = "Fecha de corte";
            // 
            // dtpProximoPago
            // 
            this.dtpProximoPago.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpProximoPago.CustomFormat = "mmmm-yyyy";
            this.dtpProximoPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpProximoPago.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpProximoPago.Location = new System.Drawing.Point(291, 98);
            this.dtpProximoPago.Margin = new System.Windows.Forms.Padding(4);
            this.dtpProximoPago.Name = "dtpProximoPago";
            this.dtpProximoPago.Size = new System.Drawing.Size(247, 34);
            this.dtpProximoPago.TabIndex = 47;
            this.dtpProximoPago.Visible = false;
            // 
            // txtPagoFinal
            // 
            this.txtPagoFinal.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtPagoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPagoFinal.Location = new System.Drawing.Point(993, 36);
            this.txtPagoFinal.Margin = new System.Windows.Forms.Padding(4);
            this.txtPagoFinal.Name = "txtPagoFinal";
            this.txtPagoFinal.ReadOnly = true;
            this.txtPagoFinal.Size = new System.Drawing.Size(68, 37);
            this.txtPagoFinal.TabIndex = 37;
            this.txtPagoFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPagoFinal.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(947, 44);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 25);
            this.label15.TabIndex = 36;
            this.label15.Text = "de";
            this.label15.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(863, 36);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 17);
            this.label5.TabIndex = 40;
            this.label5.Text = "de cada mes";
            this.label5.Visible = false;
            // 
            // txtId
            // 
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtId.Location = new System.Drawing.Point(789, 57);
            this.txtId.Margin = new System.Windows.Forms.Padding(4);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(124, 23);
            this.txtId.TabIndex = 15;
            this.txtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtId.Visible = false;
            this.txtId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtId_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(521, 36);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "ID:";
            this.label7.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkNoGeneraraInteres);
            this.groupBox3.Controls.Add(this.txtMonto);
            this.groupBox3.Controls.Add(this.lMonto);
            this.groupBox3.Controls.Add(this.txtTotalPagar);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txtMensualidadMesActual);
            this.groupBox3.Controls.Add(this.lMensualidad);
            this.groupBox3.Controls.Add(this.txtInteresMesActual);
            this.groupBox3.Controls.Add(this.lTotalInteres);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.tbactual);
            this.groupBox3.Controls.Add(this.txttotal);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(891, 119);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(283, 263);
            this.groupBox3.TabIndex = 36;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Resumen  de pago";
            // 
            // chkNoGeneraraInteres
            // 
            this.chkNoGeneraraInteres.AutoSize = true;
            this.chkNoGeneraraInteres.Location = new System.Drawing.Point(11, 102);
            this.chkNoGeneraraInteres.Margin = new System.Windows.Forms.Padding(4);
            this.chkNoGeneraraInteres.Name = "chkNoGeneraraInteres";
            this.chkNoGeneraraInteres.Size = new System.Drawing.Size(51, 21);
            this.chkNoGeneraraInteres.TabIndex = 53;
            this.chkNoGeneraraInteres.Text = "S  /";
            this.chkNoGeneraraInteres.UseVisualStyleBackColor = true;
            // 
            // txtMonto
            // 
            this.txtMonto.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtMonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonto.Location = new System.Drawing.Point(147, 214);
            this.txtMonto.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonto.Name = "txtMonto";
            this.txtMonto.Size = new System.Drawing.Size(124, 34);
            this.txtMonto.TabIndex = 48;
            this.txtMonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lMonto
            // 
            this.lMonto.AutoSize = true;
            this.lMonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lMonto.Location = new System.Drawing.Point(71, 224);
            this.lMonto.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lMonto.Name = "lMonto";
            this.lMonto.Size = new System.Drawing.Size(61, 18);
            this.lMonto.TabIndex = 47;
            this.lMonto.Text = "Monto:";
            this.lMonto.Click += new System.EventHandler(this.lMonto_Click);
            // 
            // txtTotalPagar
            // 
            this.txtTotalPagar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtTotalPagar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPagar.Location = new System.Drawing.Point(147, 140);
            this.txtTotalPagar.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalPagar.Name = "txtTotalPagar";
            this.txtTotalPagar.ReadOnly = true;
            this.txtTotalPagar.Size = new System.Drawing.Size(124, 34);
            this.txtTotalPagar.TabIndex = 46;
            this.txtTotalPagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 150);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 18);
            this.label4.TabIndex = 45;
            this.label4.Text = "Total a pagar:";
            // 
            // txtMensualidadMesActual
            // 
            this.txtMensualidadMesActual.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtMensualidadMesActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMensualidadMesActual.Location = new System.Drawing.Point(147, 43);
            this.txtMensualidadMesActual.Margin = new System.Windows.Forms.Padding(4);
            this.txtMensualidadMesActual.Name = "txtMensualidadMesActual";
            this.txtMensualidadMesActual.ReadOnly = true;
            this.txtMensualidadMesActual.Size = new System.Drawing.Size(124, 34);
            this.txtMensualidadMesActual.TabIndex = 42;
            this.txtMensualidadMesActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMensualidadMesActual.TextChanged += new System.EventHandler(this.txtMensualidadMesActual_TextChanged);
            // 
            // lMensualidad
            // 
            this.lMensualidad.AutoSize = true;
            this.lMensualidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lMensualidad.Location = new System.Drawing.Point(13, 54);
            this.lMensualidad.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lMensualidad.Name = "lMensualidad";
            this.lMensualidad.Size = new System.Drawing.Size(107, 18);
            this.lMensualidad.TabIndex = 41;
            this.lMensualidad.Text = "Mensualidad:";
            // 
            // txtInteresMesActual
            // 
            this.txtInteresMesActual.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtInteresMesActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInteresMesActual.Location = new System.Drawing.Point(147, 90);
            this.txtInteresMesActual.Margin = new System.Windows.Forms.Padding(4);
            this.txtInteresMesActual.Name = "txtInteresMesActual";
            this.txtInteresMesActual.ReadOnly = true;
            this.txtInteresMesActual.Size = new System.Drawing.Size(124, 34);
            this.txtInteresMesActual.TabIndex = 44;
            this.txtInteresMesActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lTotalInteres
            // 
            this.lTotalInteres.AutoSize = true;
            this.lTotalInteres.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTotalInteres.Location = new System.Drawing.Point(65, 100);
            this.lTotalInteres.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lTotalInteres.Name = "lTotalInteres";
            this.lTotalInteres.Size = new System.Drawing.Size(64, 18);
            this.lTotalInteres.TabIndex = 43;
            this.lTotalInteres.Text = "Interes:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(39, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 18);
            this.label2.TabIndex = 54;
            this.label2.Text = "Total:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(39, 151);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 18);
            this.label3.TabIndex = 55;
            this.label3.Text = "Actual:";
            // 
            // tbactual
            // 
            this.tbactual.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.tbactual.Location = new System.Drawing.Point(147, 140);
            this.tbactual.Margin = new System.Windows.Forms.Padding(4);
            this.tbactual.Name = "tbactual";
            this.tbactual.ReadOnly = true;
            this.tbactual.Size = new System.Drawing.Size(124, 34);
            this.tbactual.TabIndex = 54;
            // 
            // txttotal
            // 
            this.txttotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txttotal.Location = new System.Drawing.Point(147, 43);
            this.txttotal.Margin = new System.Windows.Forms.Padding(4);
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new System.Drawing.Size(124, 34);
            this.txttotal.TabIndex = 53;
            // 
            // cbMesesMora
            // 
            this.cbMesesMora.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMesesMora.FormattingEnabled = true;
            this.cbMesesMora.Location = new System.Drawing.Point(139, 26);
            this.cbMesesMora.Margin = new System.Windows.Forms.Padding(4);
            this.cbMesesMora.Name = "cbMesesMora";
            this.cbMesesMora.Size = new System.Drawing.Size(160, 25);
            this.cbMesesMora.TabIndex = 1;
            this.cbMesesMora.SelectedIndexChanged += new System.EventHandler(this.cbMesesMora_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(443, 58);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 17);
            this.label16.TabIndex = 48;
            this.label16.Text = "Fecha actual:";
            this.label16.Visible = false;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(543, 53);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(265, 22);
            this.dateTimePicker1.TabIndex = 47;
            this.dateTimePicker1.Visible = false;
            // 
            // cmdPagoMensualidad
            // 
            this.cmdPagoMensualidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdPagoMensualidad.Image = ((System.Drawing.Image)(resources.GetObject("cmdPagoMensualidad.Image")));
            this.cmdPagoMensualidad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdPagoMensualidad.Location = new System.Drawing.Point(1044, 693);
            this.cmdPagoMensualidad.Margin = new System.Windows.Forms.Padding(4);
            this.cmdPagoMensualidad.Name = "cmdPagoMensualidad";
            this.cmdPagoMensualidad.Size = new System.Drawing.Size(125, 46);
            this.cmdPagoMensualidad.TabIndex = 26;
            this.cmdPagoMensualidad.Text = "Pagar";
            this.cmdPagoMensualidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdPagoMensualidad.UseVisualStyleBackColor = true;
            this.cmdPagoMensualidad.Visible = false;
            this.cmdPagoMensualidad.Click += new System.EventHandler(this.cmdPagoMensualidad_Click);
            // 
            // dgvPagos
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvPagos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPagos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPagos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPagos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPagos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rowCmdEliminar,
            this.rowIdDetallePago,
            this.rowPagoActual,
            this.rowMontoPagoMes,
            this.rowMotoAbonoMes,
            this.rowMontoPagoMora,
            this.rowMontoAbonoMora,
            this.rowTotalPagado,
            this.rowMesPagado,
            this.rowFechaPago});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPagos.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPagos.Location = new System.Drawing.Point(10, 379);
            this.dgvPagos.Margin = new System.Windows.Forms.Padding(4);
            this.dgvPagos.Name = "dgvPagos";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPagos.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvPagos.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvPagos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPagos.Size = new System.Drawing.Size(1159, 295);
            this.dgvPagos.TabIndex = 29;
            this.dgvPagos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPagos_CellContentClick);
            this.dgvPagos.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPagos_CellEndEdit);
            this.dgvPagos.DoubleClick += new System.EventHandler(this.dgvPagos_DoubleClick);
            // 
            // rowCmdEliminar
            // 
            this.rowCmdEliminar.FillWeight = 50F;
            this.rowCmdEliminar.HeaderText = "Eliminar";
            this.rowCmdEliminar.MinimumWidth = 50;
            this.rowCmdEliminar.Name = "rowCmdEliminar";
            this.rowCmdEliminar.Text = "Eliminar";
            // 
            // rowIdDetallePago
            // 
            this.rowIdDetallePago.FillWeight = 50F;
            this.rowIdDetallePago.HeaderText = "id";
            this.rowIdDetallePago.MinimumWidth = 50;
            this.rowIdDetallePago.Name = "rowIdDetallePago";
            this.rowIdDetallePago.ReadOnly = true;
            // 
            // rowPagoActual
            // 
            this.rowPagoActual.FillWeight = 40F;
            this.rowPagoActual.HeaderText = "No pago";
            this.rowPagoActual.MinimumWidth = 40;
            this.rowPagoActual.Name = "rowPagoActual";
            // 
            // rowMontoPagoMes
            // 
            this.rowMontoPagoMes.HeaderText = "Mensualidad";
            this.rowMontoPagoMes.Name = "rowMontoPagoMes";
            // 
            // rowMotoAbonoMes
            // 
            this.rowMotoAbonoMes.HeaderText = "Abono Mes";
            this.rowMotoAbonoMes.Name = "rowMotoAbonoMes";
            // 
            // rowMontoPagoMora
            // 
            this.rowMontoPagoMora.HeaderText = "Interes";
            this.rowMontoPagoMora.Name = "rowMontoPagoMora";
            // 
            // rowMontoAbonoMora
            // 
            this.rowMontoAbonoMora.HeaderText = "Abono Interes";
            this.rowMontoAbonoMora.Name = "rowMontoAbonoMora";
            // 
            // rowTotalPagado
            // 
            this.rowTotalPagado.HeaderText = "Total";
            this.rowTotalPagado.Name = "rowTotalPagado";
            this.rowTotalPagado.ReadOnly = true;
            // 
            // rowMesPagado
            // 
            this.rowMesPagado.FillWeight = 130F;
            this.rowMesPagado.HeaderText = "Mes Pagado";
            this.rowMesPagado.MinimumWidth = 130;
            this.rowMesPagado.Name = "rowMesPagado";
            // 
            // rowFechaPago
            // 
            this.rowFechaPago.FillWeight = 130F;
            this.rowFechaPago.HeaderText = "Fecha pago";
            this.rowFechaPago.MinimumWidth = 130;
            this.rowFechaPago.Name = "rowFechaPago";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gbDesdeHasta);
            this.groupBox2.Controls.Add(this.gbMensual);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.cbMesesMora);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(524, 140);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(365, 243);
            this.groupBox2.TabIndex = 49;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Resumen Intereses";
            // 
            // gbDesdeHasta
            // 
            this.gbDesdeHasta.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.gbDesdeHasta.Controls.Add(this.txtDesdeHastaTotal);
            this.gbDesdeHasta.Controls.Add(this.label21);
            this.gbDesdeHasta.Controls.Add(this.txtDesdeHastaMensualidad);
            this.gbDesdeHasta.Controls.Add(this.label22);
            this.gbDesdeHasta.Controls.Add(this.label24);
            this.gbDesdeHasta.Controls.Add(this.txtDesdeHastaInteres);
            this.gbDesdeHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDesdeHasta.Location = new System.Drawing.Point(8, 162);
            this.gbDesdeHasta.Margin = new System.Windows.Forms.Padding(4);
            this.gbDesdeHasta.Name = "gbDesdeHasta";
            this.gbDesdeHasta.Padding = new System.Windows.Forms.Padding(4);
            this.gbDesdeHasta.Size = new System.Drawing.Size(351, 90);
            this.gbDesdeHasta.TabIndex = 6;
            this.gbDesdeHasta.TabStop = false;
            this.gbDesdeHasta.Text = "Desde / Hasta";
            // 
            // txtDesdeHastaTotal
            // 
            this.txtDesdeHastaTotal.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtDesdeHastaTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesdeHastaTotal.Location = new System.Drawing.Point(229, 32);
            this.txtDesdeHastaTotal.Margin = new System.Windows.Forms.Padding(4);
            this.txtDesdeHastaTotal.Name = "txtDesdeHastaTotal";
            this.txtDesdeHastaTotal.ReadOnly = true;
            this.txtDesdeHastaTotal.Size = new System.Drawing.Size(107, 29);
            this.txtDesdeHastaTotal.TabIndex = 5;
            this.txtDesdeHastaTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(201, 38);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(19, 20);
            this.label21.TabIndex = 4;
            this.label21.Text = "=";
            // 
            // txtDesdeHastaMensualidad
            // 
            this.txtDesdeHastaMensualidad.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtDesdeHastaMensualidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesdeHastaMensualidad.Location = new System.Drawing.Point(96, 53);
            this.txtDesdeHastaMensualidad.Margin = new System.Windows.Forms.Padding(4);
            this.txtDesdeHastaMensualidad.Name = "txtDesdeHastaMensualidad";
            this.txtDesdeHastaMensualidad.ReadOnly = true;
            this.txtDesdeHastaMensualidad.Size = new System.Drawing.Size(96, 24);
            this.txtDesdeHastaMensualidad.TabIndex = 3;
            this.txtDesdeHastaMensualidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(39, 26);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 15);
            this.label22.TabIndex = 2;
            this.label22.Text = "Interes:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(7, 58);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 15);
            this.label24.TabIndex = 1;
            this.label24.Text = "Mensualidad:";
            // 
            // txtDesdeHastaInteres
            // 
            this.txtDesdeHastaInteres.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtDesdeHastaInteres.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesdeHastaInteres.Location = new System.Drawing.Point(96, 21);
            this.txtDesdeHastaInteres.Margin = new System.Windows.Forms.Padding(4);
            this.txtDesdeHastaInteres.Name = "txtDesdeHastaInteres";
            this.txtDesdeHastaInteres.ReadOnly = true;
            this.txtDesdeHastaInteres.Size = new System.Drawing.Size(96, 24);
            this.txtDesdeHastaInteres.TabIndex = 0;
            this.txtDesdeHastaInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gbMensual
            // 
            this.gbMensual.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.gbMensual.Controls.Add(this.txtTotalMesSeleccionado);
            this.gbMensual.Controls.Add(this.label20);
            this.gbMensual.Controls.Add(this.txtMensualidadMesSeleccionado);
            this.gbMensual.Controls.Add(this.label19);
            this.gbMensual.Controls.Add(this.label18);
            this.gbMensual.Controls.Add(this.txtInteresMesSeleccionado);
            this.gbMensual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbMensual.Location = new System.Drawing.Point(8, 69);
            this.gbMensual.Margin = new System.Windows.Forms.Padding(4);
            this.gbMensual.Name = "gbMensual";
            this.gbMensual.Padding = new System.Windows.Forms.Padding(4);
            this.gbMensual.Size = new System.Drawing.Size(351, 85);
            this.gbMensual.TabIndex = 2;
            this.gbMensual.TabStop = false;
            this.gbMensual.Text = "Mensual";
            // 
            // txtTotalMesSeleccionado
            // 
            this.txtTotalMesSeleccionado.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtTotalMesSeleccionado.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalMesSeleccionado.Location = new System.Drawing.Point(229, 31);
            this.txtTotalMesSeleccionado.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalMesSeleccionado.Name = "txtTotalMesSeleccionado";
            this.txtTotalMesSeleccionado.ReadOnly = true;
            this.txtTotalMesSeleccionado.Size = new System.Drawing.Size(107, 29);
            this.txtTotalMesSeleccionado.TabIndex = 5;
            this.txtTotalMesSeleccionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(201, 37);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 20);
            this.label20.TabIndex = 4;
            this.label20.Text = "=";
            // 
            // txtMensualidadMesSeleccionado
            // 
            this.txtMensualidadMesSeleccionado.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtMensualidadMesSeleccionado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMensualidadMesSeleccionado.Location = new System.Drawing.Point(96, 52);
            this.txtMensualidadMesSeleccionado.Margin = new System.Windows.Forms.Padding(4);
            this.txtMensualidadMesSeleccionado.Name = "txtMensualidadMesSeleccionado";
            this.txtMensualidadMesSeleccionado.ReadOnly = true;
            this.txtMensualidadMesSeleccionado.Size = new System.Drawing.Size(96, 24);
            this.txtMensualidadMesSeleccionado.TabIndex = 3;
            this.txtMensualidadMesSeleccionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(39, 25);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 15);
            this.label19.TabIndex = 2;
            this.label19.Text = "Interes:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(7, 57);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 15);
            this.label18.TabIndex = 1;
            this.label18.Text = "Mensualidad:";
            // 
            // txtInteresMesSeleccionado
            // 
            this.txtInteresMesSeleccionado.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtInteresMesSeleccionado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInteresMesSeleccionado.Location = new System.Drawing.Point(96, 20);
            this.txtInteresMesSeleccionado.Margin = new System.Windows.Forms.Padding(4);
            this.txtInteresMesSeleccionado.Name = "txtInteresMesSeleccionado";
            this.txtInteresMesSeleccionado.ReadOnly = true;
            this.txtInteresMesSeleccionado.Size = new System.Drawing.Size(96, 24);
            this.txtInteresMesSeleccionado.TabIndex = 0;
            this.txtInteresMesSeleccionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(15, 30);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(94, 17);
            this.label23.TabIndex = 0;
            this.label23.Text = "Mes en mora:";
            // 
            // pbPagos
            // 
            this.pbPagos.Location = new System.Drawing.Point(11, 698);
            this.pbPagos.Margin = new System.Windows.Forms.Padding(4);
            this.pbPagos.Name = "pbPagos";
            this.pbPagos.Size = new System.Drawing.Size(943, 31);
            this.pbPagos.TabIndex = 51;
            // 
            // lCargar
            // 
            this.lCargar.AutoSize = true;
            this.lCargar.BackColor = System.Drawing.Color.Transparent;
            this.lCargar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lCargar.ForeColor = System.Drawing.Color.Red;
            this.lCargar.Location = new System.Drawing.Point(957, 709);
            this.lCargar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lCargar.Name = "lCargar";
            this.lCargar.Size = new System.Drawing.Size(56, 17);
            this.lCargar.TabIndex = 50;
            this.lCargar.Text = "............";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel3,
            this.toolStripLabel1,
            this.tsBtnBuscarComprador,
            this.tsCmdCorteCaja,
            this.tsCmdReestructuracion,
            this.toolStripSeparator1,
            this.tsBtnPagar,
            this.tsBtnConcepto,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1185, 92);
            this.toolStrip1.TabIndex = 52;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(0, 89);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(0, 89);
            // 
            // tsBtnBuscarComprador
            // 
            this.tsBtnBuscarComprador.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnBuscarComprador.Image")));
            this.tsBtnBuscarComprador.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnBuscarComprador.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnBuscarComprador.Name = "tsBtnBuscarComprador";
            this.tsBtnBuscarComprador.Size = new System.Drawing.Size(134, 89);
            this.tsBtnBuscarComprador.Text = "Buscar comprador";
            this.tsBtnBuscarComprador.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnBuscarComprador.Click += new System.EventHandler(this.tsBtnBuscarComprador_Click);
            // 
            // tsCmdCorteCaja
            // 
            this.tsCmdCorteCaja.Image = ((System.Drawing.Image)(resources.GetObject("tsCmdCorteCaja.Image")));
            this.tsCmdCorteCaja.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsCmdCorteCaja.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsCmdCorteCaja.Name = "tsCmdCorteCaja";
            this.tsCmdCorteCaja.Size = new System.Drawing.Size(80, 89);
            this.tsCmdCorteCaja.Text = "Corte caja";
            this.tsCmdCorteCaja.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsCmdCorteCaja.Click += new System.EventHandler(this.tsCmdCorteCaja_Click);
            // 
            // tsCmdReestructuracion
            // 
            this.tsCmdReestructuracion.BackColor = System.Drawing.SystemColors.Control;
            this.tsCmdReestructuracion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tsCmdReestructuracion.Image = ((System.Drawing.Image)(resources.GetObject("tsCmdReestructuracion.Image")));
            this.tsCmdReestructuracion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsCmdReestructuracion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsCmdReestructuracion.Name = "tsCmdReestructuracion";
            this.tsCmdReestructuracion.Size = new System.Drawing.Size(130, 89);
            this.tsCmdReestructuracion.Text = "Reestruccturacion";
            this.tsCmdReestructuracion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsCmdReestructuracion.Click += new System.EventHandler(this.tsCmdReestructuracion_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 92);
            // 
            // tsBtnPagar
            // 
            this.tsBtnPagar.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnPagar.Image")));
            this.tsBtnPagar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnPagar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnPagar.Name = "tsBtnPagar";
            this.tsBtnPagar.Size = new System.Drawing.Size(68, 89);
            this.tsBtnPagar.Text = "Pagar";
            this.tsBtnPagar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnPagar.Click += new System.EventHandler(this.tsBtnPagar_Click);
            // 
            // tsBtnConcepto
            // 
            this.tsBtnConcepto.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnConcepto.Image")));
            this.tsBtnConcepto.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnConcepto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnConcepto.Name = "tsBtnConcepto";
            this.tsBtnConcepto.Size = new System.Drawing.Size(83, 89);
            this.tsBtnConcepto.Text = "Conceptos";
            this.tsBtnConcepto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnConcepto.Click += new System.EventHandler(this.tsBtnConcepto_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(68, 89);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Visible = false;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "view.png");
            this.imageList1.Images.SetKeyName(1, "hide.png");
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Eliminar,
            this.idpagoconceptoDataGridViewTextBoxColumn,
            this.fechaconceptopagoDataGridViewTextBoxColumn,
            this.cantidadtotalDataGridViewTextBoxColumn,
            this.cantidadpagarDataGridViewTextBoxColumn,
            this.conceptoDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.resultconceptospagosBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(10, 379);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1159, 295);
            this.dataGridView1.TabIndex = 53;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick_1);
            // 
            // Eliminar
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.NullValue = "Eliminar";
            this.Eliminar.DefaultCellStyle = dataGridViewCellStyle6;
            this.Eliminar.HeaderText = "Eliminar";
            this.Eliminar.Name = "Eliminar";
            // 
            // idpagoconceptoDataGridViewTextBoxColumn
            // 
            this.idpagoconceptoDataGridViewTextBoxColumn.DataPropertyName = "id_pago_concepto";
            this.idpagoconceptoDataGridViewTextBoxColumn.HeaderText = "id";
            this.idpagoconceptoDataGridViewTextBoxColumn.Name = "idpagoconceptoDataGridViewTextBoxColumn";
            this.idpagoconceptoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fechaconceptopagoDataGridViewTextBoxColumn
            // 
            this.fechaconceptopagoDataGridViewTextBoxColumn.DataPropertyName = "fecha_concepto_pago";
            this.fechaconceptopagoDataGridViewTextBoxColumn.HeaderText = "fecha de pago";
            this.fechaconceptopagoDataGridViewTextBoxColumn.Name = "fechaconceptopagoDataGridViewTextBoxColumn";
            this.fechaconceptopagoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cantidadtotalDataGridViewTextBoxColumn
            // 
            this.cantidadtotalDataGridViewTextBoxColumn.DataPropertyName = "cantidad_total";
            this.cantidadtotalDataGridViewTextBoxColumn.HeaderText = "cantidad total";
            this.cantidadtotalDataGridViewTextBoxColumn.Name = "cantidadtotalDataGridViewTextBoxColumn";
            this.cantidadtotalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cantidadpagarDataGridViewTextBoxColumn
            // 
            this.cantidadpagarDataGridViewTextBoxColumn.DataPropertyName = "cantidad_pagar";
            this.cantidadpagarDataGridViewTextBoxColumn.HeaderText = "cantidad pagada";
            this.cantidadpagarDataGridViewTextBoxColumn.Name = "cantidadpagarDataGridViewTextBoxColumn";
            this.cantidadpagarDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // conceptoDataGridViewTextBoxColumn
            // 
            this.conceptoDataGridViewTextBoxColumn.DataPropertyName = "concepto";
            this.conceptoDataGridViewTextBoxColumn.HeaderText = "concepto";
            this.conceptoDataGridViewTextBoxColumn.Name = "conceptoDataGridViewTextBoxColumn";
            this.conceptoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            this.statusDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // resultconceptospagosBindingSource
            // 
            this.resultconceptospagosBindingSource.DataSource = typeof(PvTerrenos.resultconceptospagos);
            // 
            // resultQueryCPBindingSource
            // 
            this.resultQueryCPBindingSource.DataSource = typeof(PvTerrenos.ResultQueryCP);
            // 
            // resultQueryCPCorteCajaBindingSource
            // 
            this.resultQueryCPCorteCajaBindingSource.DataSource = typeof(PvTerrenos.ResultQueryCPCorteCaja);
            // 
            // FrmPuntoVenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1185, 746);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.lCargar);
            this.Controls.Add(this.dtpProximoPago);
            this.Controls.Add(this.pbPagos);
            this.Controls.Add(this.txtPagoFinal);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cmdPagoMensualidad);
            this.Controls.Add(this.dgvPagos);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.toolStrip1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmPuntoVenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pago";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPago_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPagos)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbDesdeHasta.ResumeLayout(false);
            this.gbDesdeHasta.PerformLayout();
            this.gbMensual.ResumeLayout(false);
            this.gbMensual.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultconceptospagosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultQueryCPBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultQueryCPCorteCajaBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button cmdPagoMensualidad;
        private System.Windows.Forms.ComboBox cbLote;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgvPagos;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtInteresMesActual;
        private System.Windows.Forms.Label lTotalInteres;
        private System.Windows.Forms.TextBox txtMensualidadMesActual;
        private System.Windows.Forms.Label lMensualidad;
        private System.Windows.Forms.ComboBox cbMesesMora;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbComprador;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox txtPagoActual;
        private System.Windows.Forms.TextBox txtPagoFinal;
        private System.Windows.Forms.TextBox txtDiaCorte;
        private System.Windows.Forms.TextBox txtProximoPago;
        private System.Windows.Forms.TextBox txtManzana;
        private System.Windows.Forms.TextBox txtPredio;
        private System.Windows.Forms.TextBox txtTotalPagar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMonto;
        private System.Windows.Forms.Label lMonto;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox gbDesdeHasta;
        private System.Windows.Forms.TextBox txtDesdeHastaTotal;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtDesdeHastaMensualidad;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtDesdeHastaInteres;
        private System.Windows.Forms.GroupBox gbMensual;
        private System.Windows.Forms.TextBox txtTotalMesSeleccionado;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtMensualidadMesSeleccionado;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtInteresMesSeleccionado;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button cmdBuscarCliente;
        private System.Windows.Forms.ProgressBar pbPagos;
        private System.Windows.Forms.Label lCargar;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsCmdReestructuracion;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.CheckBox chkNoGeneraraInteres;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton tsCmdCorteCaja;
        private System.Windows.Forms.DateTimePicker dtpProximoPago;
        private System.Windows.Forms.ToolStripButton tsBtnBuscarComprador;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsBtnPagar;
        private System.Windows.Forms.DataGridViewButtonColumn rowCmdEliminar;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowIdDetallePago;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowPagoActual;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowMontoPagoMes;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowMotoAbonoMes;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowMontoPagoMora;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowMontoAbonoMora;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowTotalPagado;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowMesPagado;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowFechaPago;
        private System.Windows.Forms.ToolStripButton tsBtnConcepto;
        private System.Windows.Forms.ComboBox cbConcepto;
        private System.Windows.Forms.Label lbConcepto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbactual;
        private System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.BindingSource resultQueryCPCorteCajaBindingSource;
        private System.Windows.Forms.BindingSource resultQueryCPBindingSource;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource resultconceptospagosBindingSource;
        private System.Windows.Forms.DataGridViewButtonColumn Eliminar;
        private System.Windows.Forms.DataGridViewTextBoxColumn idpagoconceptoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaconceptopagoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidadtotalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidadpagarDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn conceptoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
    }
}