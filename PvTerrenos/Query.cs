﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PvTerrenos
{
    public class Query
    {
        public List<string> values_list = new List<string>();
        public action action;

        #region INSTRUCCIONES SQL
        #region TABLAS

        public string APPOINTMENTS = "APPOINTMENTS";
        public string MANZANA = "Manzana";
        public string COMPRADOR = "Comprador";
        public string EMPLOYEES = "EMPLOYEES";
        public string PAYMENTS = "PAYMENTS";
        public string LOTE = "Lote";
        public string PPREDIO = "Predio";
        public string COMPRAVENTA = "CompraVenta";
        public string SESSIONS = "SESSIONS";
        public string USUARIO = "Usuario";
        public string COMISION = "Comision";
        public string PROXIMOPAGO = "ProximoPago";
        public string DETALLEPAGO = "DetallePago";
        public string CONCEPTOPAGO = "ConceptoPago";
        public string na = "na";
        public string ACTUALIZACIONES = "Actualizacion";
        public string VERCIONACTUALIZACION = "2.7.6.5";
        #endregion

        #region PALABRAS RESERVADAS SQL
        public string INSERT_INTO = "INSERT INTO";
        public string SELECT = "SELECT";
        public string FROM = "FROM";
        public string VALUES = "VALUES";
        public string UPDATE = "UPDATE";
        public string SET = "SET";
        public string igual = " = ";
        public string AND = "AND";
        public string ORDEN_BY = "ORDER BY";
        public string ASC = "ASC";
        public string WHERE = "WHERE";
        public string MAX_ID = "MAX";
        public string LIKE = "LIKE";
        public string CURRENT_TIMESTAMP = "DATE_FORMAT(CURRENT_TIMESTAMP(), '%d-%m-%Y %T')";
        public string DATE_CAST = "dd/MM/yyyy HH:mm:ss";
        public string DATE_TIME = "DATE_TIME";

        public string abrir_parentesis = "(";
        public string cerrar_parentesis = ")";
        public string c = "'";
        public string cc = ",";
        public string ccc = "','";

        #endregion
        #endregion

        #region ATRIBUTOS

        #region APPOINTMENTS

        public string appointment_attributes_insert = "(`date_appointment`, `hour_appointment`, `observations`, `status`, `created_at`, `fk_sale_datail`, `fk_user`)";
        public string appointment_attributes_select = "`id_appointment`, `date_appointment`, `hour_appointment`, `observations`, `status`, `created_at`, `fk_sale_datail`, `fk_user`";

        public string id_appointment_a = "id_appointment";
        public string date_appointment_a = "date_appointment";
        public string hour_appointment_a = "hour_appointment";
        public string observations_a = "observations";
        public string status_ = "status";
        public string created_at_a = "created_at";
        #endregion

        #region BLOCKS
        public string block_attributes_insert = "(`block_number`, `pk_predio`, `fk_property`)";
        public string block_attributes_select = "`id_block`, `block_number`, `pk_predio`, `fk_property`";

        //public string id_lote_a = "id_lote";
        public string block_number_a = "block_number";
        public string fk_property = "fk_property";
        #endregion

        #region COMPRADOR
        public string id_comprador_a = "id_comprador";
        public string nombre_a = "nombre";
        //public string last_name_a = "last_name";
        public string direccion_a = "direccion";
        public string beneficiario_a = "beneficiario";
        public string originario_a = "originario";
        public string recidencia_a = "residencia";
        public string ocupacion_a = "ocupacion";
        public string estado_civil_a = "estado_civil";
        public string tel1_a = "tel1";
        public string tel2_a = "tel2";
        //public string alternative_number_phone_a = "alternative_number_phone";
        //public string created_at_a = "created_at";
        #endregion

        #region USERS
        public string id_user_a = "id_user";
        public string username_a = "nombre";
        public string password_a = "password";
        public string nivel_acceso_a = "tipo";
        public string administracion_a = "administracion";
        public string fk_empleado_a = "fk_empleado";
        //public string WHERE_ONE = $"{WHERE} {username_a} {igual} ? {AND} {password_a} {igual} ?";
        #endregion

        #region COMPRAVENTA
        public string id_venta_a = "id_venta";
        public string mensualidad_a = "mensualidad";
        public string fecha_corte_a = "fecha_corte";
        public string status_venta_a = "status_venta";
        public string fecha_compra_a = "fecha_compra";
        public string monto_a = "monto";
        public string pk_comprador_a = "pk_comprador";
        public string pk_lote_a = "pk_lote";
        public string ACTIVO = "ACTIVO";

        public string whereVentasActivas = "WHERE ? ORDER BY `id_venta` ASC";//"WHERE `status_venta` = ? ORDER BY `id_venta` ASC";
        //public string whereActualizarCompraVenta = "WHERE `id_venta` = ?";
        //public string fk_user_a = "fk_user";
        //public string fecha_pago_comision_a = "fecha_pago_comision";
        #endregion

        #region COMISION
        public string id_comision_a = "id_comision";
        public string status_a = "status";
        public string fecha_pago_a = "fecha_pago";
        public string fk_compra_venta_a = "fk_compra_venta";

        public string query_comisiones = "SELECT " +
            "C.nombre, " +
            "max(DP.pagoActual) AS UltimoPago, " +
            "P.nombre_predio AS predio, " +
            "M.n_manzana AS manzana, " +
            "L.n_lote AS lote, " +
            "CV.fk_user, " +
            "CV.fecha_pago_comision, " +
            "CV.id_venta " + 
            "FROM " +
            "DetallePago AS DP " +
            "INNER JOIN " +
            "(" +
            "CompraVenta AS CV, " +
            "Comprador AS C, " +
            "Lote AS L, " +
            "Manzana AS M, " +
            "Predio AS P" +
            ") " +
            "ON DP.idVenta = CV.id_venta " +
            "AND CV.pk_comprador = C.id_comprador " +
            "AND CV.pk_lote = L.id_lote " +
            "AND L.pk_manzana = M.id_manzana " +
            "AND M.pk_predio = P.id_predio " +
            "WHERE DP.pagoActual >= 3 AND CV.status_venta = 'ACTIVO' AND `fk_user` != 0 " +
            "GROUP BY  CV.pk_comprador ORDER BY DP.idDetallePago DESC";//"SELECT Predio.nombre_predio AS predio, Manzana.n_manzana AS manzana, Lote.n_lote AS lote,CompraVenta.fecha_pago_comision, CompraVenta.fk_user FROM CompraVenta INNER JOIN (Lote, Manzana, Predio) ON CompraVenta.pk_lote = Lote.id_lote AND Lote.pk_manzana = Manzana.id_manzana AND Manzana.pk_predio = Predio.id_predio";// WHERE `fecha_pago_comision` != ''";
        #endregion

        #region COMBRANZA
        public string query_morosos = "SELECT " +
            "C.nombre, " +
            "max(DP.pagoActual) AS UltimoPago, " +
            "P.nombre_predio AS predio, " +
            "M.n_manzana AS manzana, " +
            "L.n_lote AS lote, " +
            "CV.fk_user, " +
            "CV.fecha_pago_comision, " +
            "CV.id_venta " + 
            "FROM " +
            "DetallePago AS DP " +
            "INNER JOIN " +
            "(" +
            "CompraVenta AS CV, " +
            "Comprador AS C, " +
            "Lote AS L, " +
            "Manzana AS M, " +
            "Predio AS P" +
            ") " +
            "ON DP.idVenta = CV.id_venta " +
            "AND CV.pk_comprador = C.id_comprador " +
            "AND CV.pk_lote = L.id_lote " +
            "AND L.pk_manzana = M.id_manzana " +
            "AND M.pk_predio = P.id_predio " +
            "WHERE DP.pagoActual >= 3 AND CV.status_venta = 'ACTIVO' AND `fk_user` != 0 " +
            "GROUP BY  CV.pk_comprador ORDER BY DP.idDetallePago DESC";//"SELECT Predio.nombre_predio AS predio, Manzana.n_manzana AS manzana, Lote.n_lote AS lote,CompraVenta.fecha_pago_comision, CompraVenta.fk_user FROM CompraVenta INNER JOIN (Lote, Manzana, Predio) ON CompraVenta.pk_lote = Lote.id_lote AND Lote.pk_manzana = Manzana.id_manzana AND Manzana.pk_predio = Predio.id_predio";// WHERE `fecha_pago_comision` != ''";
        #endregion

        #region PROXIMOPAGO
        public string id_proximo_pago_a = "id_proximo_pago";
        //public string monto_a = "monto";
        public string proximo_pago_a = "proximo_pago";
        public string pago_actual_a = "pago_actual";
        public string status_mora_a = "status_mora";
        public string pk_venta = "pk_venta";
        #endregion

        #region PREDIO  
        public string id_predio_a = "id_predio";
        public string id_predio_literal_a = "id_predio_literal";
        public string nombre_predio_a = "nombre_predio";
        public string colonia_a = "colonia";
        public string municipio_a = "municipio";
        //public string administracion_a = "administracion";
        #endregion

        #region MANZANA  
        public string id_manzana_a = "id_manzana";
        public string n_manzana_a = "n_manzana";
        public string pk_predio_a = "pk_predio";
        //public string administracion_a = "administracion";
        #endregion

        #region LOTE  
        public string id_lote_a = "id_lote";
        public string n_lote_a = "n_lote";
        //public string pk_predio_a = "pk_predio";
        public string pk_manzana_a = "pk_manzana";
        public string status_lote_a = "status_lote";
        public string medidaNorte_a = "medidaNorte";
        public string medidaSur_a = "medidaSur";
        public string medidaEste_a = "medidaEste";
        public string medidaOeste_a = "medidaOeste";
        //public string administracion_a = "administracion";
        #endregion

        #endregion

        #region METODOS
        public string implode(List<string> valores)
        {
            int items = valores.Count();
            string cadena = "";

            for (int i = 0; i < items; i++)
            {
                cadena += valores[i];

                if(i < (items-1))
                {
                    cadena += ",";
                }
            }
            return cadena;
        }

        public string queryUpdate(string atributo, string value)
        {
            return $"{WHERE} {atributo} {igual} {value}";
        }

        public string queryInsertar(string tabla, string atributos, string valores)
        {
            return $"{INSERT_INTO} {tabla} {atributos} {VALUES} {valores}";
        }
        
        public string queryConsulta(string tabla, string atributo, string condicion)
        {
            return $"{SELECT} {atributo} {FROM} {tabla} {condicion}";
        }

        public string queryAll(string atributo, bool order_by, bool asc)
        {
            string asc_desc = "DESC";
            if (!order_by)
            {
                return "WHERE ?";
            }
            else
            {
                if (asc)
                {
                    asc_desc = "ASC";
                }
                return $"WHERE ? ORDER BY {atributo} {ASC}";
            }
        }

        public string queryActualizar(string tabla, string valoresActualizar, string condicion)
        {
            return $"{UPDATE} {tabla} {SET} {valoresActualizar} {condicion}";
        }

        public string maxId(string atributo)
        {
            return MAX_ID + abrir_parentesis + atributo + cerrar_parentesis;
        }
        #endregion
    }
}
