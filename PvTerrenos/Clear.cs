﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvTerrenos
{
    public static class Clear
    {
        public static void clearForm(Form form)
        {
            foreach (Control control in form.Controls)
            {
                if (control is TextBox | control is ComboBox)
                {
                    control.Text = "";
                }
                else if(control is GroupBox)
                {
                    foreach (Control control_in in control.Controls)
                    {
                        if (control_in is TextBox | control_in is ComboBox)
                        {
                            control_in.Text = "";
                        }
                    }
                }
            }
        }
    }
}
