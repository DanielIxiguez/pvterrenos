﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvTerrenos
{
    public class Interes
    {  
        decimal amount { get; set; }
        decimal porcentage { get; set; }

        /// <summary>
        /// Calcualte the interest per month
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="porcentage"></param>
        /// <returns></returns>
        public decimal mensual(decimal _amount, decimal _porcentage)
        {
            return _amount * _porcentage;
        }

        /// <summary>
        /// Calculate the interest for each month expired
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="porcentage"></param>
        /// <param name="expiration"></param>
        /// <returns></returns>
        public decimal mensual(decimal _amount, decimal _porcentage, int _monthsExpiration)
        {
            decimal resultado = (_amount * _porcentage) * _monthsExpiration;

            return Math.Round(resultado);
        }
    }
}
