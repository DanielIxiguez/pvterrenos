﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvTerrenos
{
    class ResultQueryCP
    {
            public int cantidad_total { get; set; }
            public int sumcantidad_pagar { get; set; } 
    }

    public class ResultQueryCPCorteCaja
    {
      
      
        public string comprador { get; set; }
        public int cantidad_pagar { get; set; }
        public string lote { get; set; }
        public string manzana { get; set; }
        public string concepto { get; set; }
        public string fecha_concepto_pago { get; set; }
        public string predio { get; set; }
    }

    public class orderdgvlist
    {
        public string comprador { get; set; }
        public string cantidad_pagar { get; set; }
        public string lote { get; set; }
        public string manzana { get; set; }
        public string concepto { get; set; }
        public DateTime fecha_concepto_pago { get; set;}
        public string predio { get; set; }
    }

    public class resultconceptospagos
    {
        private string _date;
        private string _cantitotal;
        private string _cantipagar;
        private string _conceptomayus;

        public string id_pago_concepto { get; set; }
        public string fecha_concepto_pago { get; set; }
        public string cantidad_total { get { return string.Format("{0:N2}", _cantitotal); } set { this._cantitotal = value; } }
        public string cantidad_pagar { get { return string.Format("{0:N2}", _cantipagar); } set { this._cantipagar = value; } }
        public string concepto { get { return _conceptomayus.ToUpper(); } set { this._conceptomayus = value; } }
        public string status { get; set; }
    }

}
