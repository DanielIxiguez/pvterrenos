﻿namespace PvTerrenos
{
    partial class FrmComision
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmComision));
            this.cbVendedor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvComisiones = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsBtnPagarComision = new System.Windows.Forms.ToolStripButton();
            this.cEstatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cUltimoPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPredio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cManzana = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cLote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFechaPagoComision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComisiones)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbVendedor
            // 
            this.cbVendedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVendedor.FormattingEnabled = true;
            this.cbVendedor.Location = new System.Drawing.Point(97, 98);
            this.cbVendedor.Name = "cbVendedor";
            this.cbVendedor.Size = new System.Drawing.Size(324, 32);
            this.cbVendedor.TabIndex = 6;
            this.cbVendedor.SelectedIndexChanged += new System.EventHandler(this.cbVendedor_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Vendedor";
            // 
            // dgvComisiones
            // 
            this.dgvComisiones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvComisiones.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvComisiones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvComisiones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cEstatus,
            this.cCliente,
            this.cUltimoPago,
            this.cPredio,
            this.cManzana,
            this.cLote,
            this.cFechaPagoComision});
            this.dgvComisiones.Location = new System.Drawing.Point(16, 136);
            this.dgvComisiones.Name = "dgvComisiones";
            this.dgvComisiones.Size = new System.Drawing.Size(867, 310);
            this.dgvComisiones.TabIndex = 8;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnPagarComision});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(895, 87);
            this.toolStrip1.TabIndex = 9;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsBtnPagarComision
            // 
            this.tsBtnPagarComision.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnPagarComision.Image")));
            this.tsBtnPagarComision.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnPagarComision.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnPagarComision.Name = "tsBtnPagarComision";
            this.tsBtnPagarComision.Size = new System.Drawing.Size(93, 84);
            this.tsBtnPagarComision.Text = "Pagar comisión";
            this.tsBtnPagarComision.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnPagarComision.Click += new System.EventHandler(this.tsBtnPagarComision_Click);
            // 
            // cEstatus
            // 
            this.cEstatus.HeaderText = "Id venta";
            this.cEstatus.Name = "cEstatus";
            // 
            // cCliente
            // 
            this.cCliente.HeaderText = "Cliente";
            this.cCliente.Name = "cCliente";
            // 
            // cUltimoPago
            // 
            this.cUltimoPago.HeaderText = "Ultimo pago";
            this.cUltimoPago.Name = "cUltimoPago";
            // 
            // cPredio
            // 
            this.cPredio.HeaderText = "Predio";
            this.cPredio.Name = "cPredio";
            // 
            // cManzana
            // 
            this.cManzana.HeaderText = "Manzana";
            this.cManzana.Name = "cManzana";
            // 
            // cLote
            // 
            this.cLote.HeaderText = "Lote";
            this.cLote.Name = "cLote";
            // 
            // cFechaPagoComision
            // 
            this.cFechaPagoComision.HeaderText = "Fecha de pago comisión";
            this.cFechaPagoComision.Name = "cFechaPagoComision";
            // 
            // FrmComision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 454);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dgvComisiones);
            this.Controls.Add(this.cbVendedor);
            this.Controls.Add(this.label1);
            this.Name = "FrmComision";
            this.Text = "Comisiones";
            ((System.ComponentModel.ISupportInitialize)(this.dgvComisiones)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbVendedor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvComisiones;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsBtnPagarComision;
        private System.Windows.Forms.DataGridViewTextBoxColumn cEstatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn cUltimoPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPredio;
        private System.Windows.Forms.DataGridViewTextBoxColumn cManzana;
        private System.Windows.Forms.DataGridViewTextBoxColumn cLote;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFechaPagoComision;
    }
}