﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvTerrenos
{
    public partial class FrmDireccion : Form
    {

        DireccionClientes direccionClientes = new DireccionClientes();
        static List<DireccionClientes> _ListresultQueryDireccionClientes = new List<DireccionClientes>();

        DireccionEscrituras direccionEscrituras = new DireccionEscrituras();
        static List<DireccionEscrituras> _ListQueryDireccionEscrituras = new List<DireccionEscrituras>();

        public FrmDireccion()
        {
            InitializeComponent();

          

        }

     

        private void ClientesActivos_Click(object sender, EventArgs e)
        {
            //hiden all dwv after show this dwv
           // dGVstatusClientes.Visible = true;
            //sent dgv name an load data 
            GetData();

        }
        //get  param dgv 
        private void GetData()
        {
            direccionClientes.queryFree("", $"SELECT Comprador.nombre,CompraVenta.status_venta,CompraVenta.fecha_compra " +
                $"FROM `CompraVenta` INNER JOIN `Comprador` " +
                $"WHERE status_venta LIKE 'activo' and CompraVenta.pk_comprador = Comprador.id_comprador  ORDER BY `pk_lote` DESC");

            _ListresultQueryDireccionClientes = direccionClientes.list<DireccionClientes>();

            LoadDgvClientes();

        }

        private void LoadDgvClientes()
        {
            dGVEscrituras.Visible = false;
            dGVClientesActivos.Visible = true;
            dGVClientesActivos.DataSource = null;
            dGVClientesActivos.DataSource = _ListresultQueryDireccionClientes;
            
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("funcion esta en desarrollo");

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {

            //Ocoltar todas menos esta 

            DireccionPagados();

           

       

        }

        public void DireccionPagados()
        {
            direccionEscrituras.queryFree("", $" SELECT Lote.id_lote, Comprador.nombre, Predio.nombre_predio,Manzana.n_manzana,Lote.n_lote,CompraVenta.status_venta " +
               $"FROM CompraVenta " +
               $"INNER JOIN Comprador on Comprador.id_comprador = CompraVenta.pk_comprador " +
               $"INNER JOIN Lote ON Lote.id_lote = CompraVenta.pk_lote " +
               $"INNER JOIN Predio ON Lote.pk_predio = Predio.id_predio " +
               $"INNER JOIN Manzana on Manzana.id_manzana = Lote.pk_manzana " +
               $"where status_venta = 'pagado' or status_venta = 'escriturado' or status_venta = 'para escriturar' " +
               "ORDER BY pk_lote");

            _ListQueryDireccionEscrituras = direccionEscrituras.list<DireccionEscrituras>();

           LoadDgvEscrituras();
        }

        private void LoadDgvEscrituras()
        {
          //var count = _ListQueryDireccionEscrituras;

           // var results = _ListQueryDireccionEscrituras.GroupBy(x => Int32.Parse(x.id_lote)).Select(g => g.OrderBy(y => Int32.Parse(y.id_lote)).First());

            dGVEscrituras.Visible = true;
            dGVClientesActivos.Visible = false;
            dGVEscrituras.DataSource = null;

            dGVEscrituras.DataSource = _ListQueryDireccionEscrituras;
        }

        private void dGVEscrituras_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 0:
                    //order lista and clean
                    var listOfObjects0 = _ListQueryDireccionEscrituras.OrderBy(x => x.nombre).ToList();
                    loadFor(listOfObjects0);
                    break;
                case 1:
                    //order lista and clean
                  var listOfObjects1 = _ListQueryDireccionEscrituras.OrderBy(x => x.nombre_predio).ToList();
                    loadFor(listOfObjects1);
                    break;
                case 2:
                     
                    break;
                case 3:

                    
                    break;
                case 4:
                    var listOfObjects4 = _ListQueryDireccionEscrituras.OrderBy(x => x.status_venta).ToList();
                    loadFor(listOfObjects4);
                    
                    break;
                    //default:
                    //    Console.WriteLine("Default case");
                    //    break;
            }

        }

        private void loadFor(List<DireccionEscrituras> listOfObjects1)
        {
            dGVEscrituras.Visible = true;
            dGVClientesActivos.Visible = false;
            dGVEscrituras.DataSource = null;
            dGVEscrituras.DataSource = listOfObjects1;
        }

      
    }
}
