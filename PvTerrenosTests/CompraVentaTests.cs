﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PvTerrenos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvTerrenos.Tests
{
    [TestClass()]
    public class CompraVentaTests
    {
        //[TestMethod()]
        //public void insertLastIdTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void insertarDetallePago()
        {
            //averenge
            decimal monto = 1300.0M;
            decimal mensualidadActual = 1000;
            decimal mensualidad = 2000M;
            decimal mensualidadPago = 0.0M;
            decimal mensualidadAbono = 0.0M;
            decimal interes = 300.0M;
            decimal InteresPago = 0.0M;
            decimal interesAbono = 0.0M;
            int mesesVencidos = 3;
            int entro = 0;
            int resultado = 3;

            //action

            do
            {
                if (interes != 0)
                {
                    if(monto >= interes)
                    {
                        InteresPago = interes;
                        monto -= interes;
                        interes = 0.0M;
                    }
                    else
                    {
                        interesAbono = interes;
                        monto = 0.0M;
                        interes = 0.0M;
                    }
                }
                if (monto >= mensualidadActual)
                {
                    mensualidadPago = mensualidadActual;
                    monto -= mensualidadActual;

                    if (mesesVencidos > 0)
                    {
                        mesesVencidos--;

                        if (mensualidadActual <  mensualidad)
                        {
                            mensualidadActual = mensualidad;
                        }

                        interes = (mensualidadActual * 0.06M) * mesesVencidos;

                        entro++;
                    }
                    else
                    {
                        entro++;
                    }
                }
                else
                {
                    mensualidadAbono = monto;
                    monto = 0.0M;
                }

            } while (monto > 0);

            //assert
            Assert.AreEqual(resultado, entro);
        }
    }
} 