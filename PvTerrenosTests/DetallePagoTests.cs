﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PvTerrenos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvTerrenos.Tests
{
    [TestClass()]
    public class DetallePagoTests
    {
        [TestMethod()]
        public void numeroPagoMesEnCursoTest()
        {
            Assert.Fail();
        }

        [TestMethod]
        public void mesesVencidos()
        {
            //avarege
            int UltimoPago = 2;
            DateTime FechaUltimoPago = new DateTime(2018, 02, 11);

            int PagoActual = 4;
            DateTime FechaaPagoActual = new DateTime(2018, 04, 11);
            int ProximoPago = 3;

            DateTime FechaProximoPago = new DateTime(2018, 03, 11);
            int MesesVencidos = 0;
            int resultado = 2;
            bool mesActualVencido = true;

            //action

            MesesVencidos = PagoActual - UltimoPago;

            if (!mesActualVencido)
            {
                MesesVencidos--;
            }

            //asserts
             Assert.AreEqual(resultado, MesesVencidos);
        }

        [TestMethod]
        public void calcularInteres()
        {
            //avarege
            int NumeroMensualidadesVencidasPagadas = 0;

            decimal MensualidadActual = 25;
            decimal PagoParcialMensualidad = 0;
            decimal pagoParcialInteres = 0;


            decimal Interes = 0;
            decimal InteresActual = 0;

            int NumeroMensuliadadesVencidad = 8;

            decimal[ , ] tabla = { { 25.0M , 0.0M} , { 0.0M, 3.0M } , { 30.0M , 0.0M } , { 20 , 30} };

            //action
            PagoParcialMensualidad += MensualidadActual;

            for (int i = 0; i <= 3; i++)
            {
                for (int j = 0; j < 1 ; j++)
                {
                    PagoParcialMensualidad += tabla[i, j];
                    pagoParcialInteres = tabla[i, j + 1];

                    Interes = PagoParcialMensualidad * 0.06M;

                    NumeroMensualidadesVencidasPagadas += Convert.ToInt32(pagoParcialInteres / Interes);
                }
            }

            NumeroMensuliadadesVencidad -= NumeroMensualidadesVencidasPagadas;

            InteresActual = (MensualidadActual * 0.06M) * NumeroMensuliadadesVencidad;

            //asserts
            Assert.AreEqual(InteresActual , 3.0M);

        }
    }
}